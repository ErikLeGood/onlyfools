


Welcome to Tutorialsville - population 5 (plus you).

Use WASD to move the camera around. You can use Z and X to zoom in and out.

To rotate the camera around, you can hold CTRL and use WASD, or you can hold the middle-mouse button.

You can click on citizens, police, zombies, players and buildings to get information about them.

You can also view the entire map (by pressing M), and you can focus on your player avatar with R (try it).

If the player avatar dies, the game is over. It can be killed by Police, Zombies and (occasionally) angry Citizens.

To Move, right click somewhere on the level. Try it out - go for a stroll!

This town has no defences - there are no clinics to cure infected citizens, and no Police to fight the Zombies.

In short, Tutorialsville is doomed.

However, you still need to actually infect some citizens. Try watching them for a while - note where they tend to cluster.

Patience and observation are key!

When you're ready to start the apocalypse, press 1 to enter aim mode - then aim the mouse and use Left Click to fire.

Citizens in the blast radius will panic for a while, but will eventually calm down and carry on with their day.

However, over time, the contagion will drain their health - if they don't get treated, they will die and become a Zombie.

Citizens are frightened of Zombies - a brave few may try to fight them, but most will run away, and with good reason.

A single bite from a Zombie will infect a Citizen, and the Zombies will destroy buildings to get at Citizens inside.

When an infected Citizen dies, they also drop a tissue sample - collecting enough of these grants extra design points.

However, you must gather them carefully - the Zombies will chase you if you get too close to them.

You have some powers to help direct the zombies - you can use your zombie whistle to get their attention...

...And you can throw some zombie bait to send them toward another target (at least for a brief moment).

With these powers, you should be able to get around the Zombies and gather samples for the next level.