﻿Shader "UI/Background Based Color Shift"
{
    Properties
    {
        _Color ("MainColor", Color) = (1,1,1,1)
		_LColor ("LightColor", Color) = (0,0,0,0)
		_Layer ("Layer", Float) = 0
		_MainTex ("Leave Blank", 2D) = "white" {}
    }

    SubShader
    {
        LOD 200

        Tags
        {
            "Queue" = "Overlay+1"
            "IgnoreProjector" = "True"
            "RenderType" = "Overlay"
            "PreviewType"="Plane"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        ZTest Always
        Offset -1, -1
        Fog { Mode Off }
        Blend SrcAlpha OneMinusSrcAlpha

		GrabPass
        {
            "_BackgroundTexture"
        }

        Pass
        {
            CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
                #include "UnityCG.cginc"

                struct appdata_t
                {
                    float4 vertex : POSITION;
                    half4 color : COLOR;
                    float2 texcoord : TEXCOORD0;
                };

                struct v2f
                {
                    float4 vertex : POSITION;
                    half4 color : COLOR;
                    float2 texcoord : TEXCOORD0;
					float4 grabpos : TEXCOORD1;
                };

				sampler2D _BackgroundTexture;
                sampler2D _MainTex;
                float4 _MainTex_ST;
                fixed4 _Color;
				fixed4 _LColor;

                v2f vert (appdata_t v)
                {
                    v2f o;
                    o.vertex = UnityObjectToClipPos(v.vertex);
                    o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
                    o.color = v.color;
					#ifdef UNITY_HALF_TEXEL_OFFSET
                    o.vertex.xy += (_ScreenParams.zw-1.0)*float2(-1,1);
					#endif
					o.grabpos = ComputeGrabScreenPos(o.vertex);
                    return o;
                }

                half4 frag (v2f i) : COLOR
                {
					
                    half4 col = i.color;
                    col.a *= tex2D(_MainTex, i.texcoord).a;
                    col = col * _Color;
                    clip (col.a - 0.01);
					half4 bgcol = tex2Dproj(_BackgroundTexture, i.grabpos);
					//float shade = (bgcol.r + bgcol.g + bgcol.b) / 3;
					half4 output = col - bgcol;
					float d = distance(bgcol, _Color);
					float d2 = distance(bgcol, _LColor);

					if (d < d2)
					{
						output = _LColor;
					}
					else
					{
						output = _Color;
					}
					output.a = col.a;
                    return output;
                }
            ENDCG
        }
    }
}