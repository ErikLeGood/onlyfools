﻿using UnityEngine;
using System.Collections;

public delegate void EventHandler_Capsule(GameObject Source, Avatar_Movement_Control launcher);

public class Capsule_Script : MonoBehaviour 
{
    public Avatar_Movement_Control Owning_Player;
    public GameObject Explosion_Effect;

    public static event EventHandler_Capsule Explosion_Event;

	// Use this for initialization
	void Start()
    {
	    for (int i = 0; i < Game_Manager.All_Citizens.Count; i++)
        {
            Explosion_Event += new EventHandler_Capsule(Game_Manager.All_Citizens[i].Capsule_Event);
        }
        for (int i = 0; i < Game_Manager.All_Police.Count; i++)
        {
            Explosion_Event += new EventHandler_Capsule(Game_Manager.All_Police[i].CapsuleEvent);
        }
	}

    public static void Remove_Subscriber(NPC_Citizen citizen)
    {
        Explosion_Event -= citizen.Capsule_Event;
    }
    public static void Remove_Subscriber(NPC_Police police)
    {
        Explosion_Event -= police.CapsuleEvent;
    }
	
	// Update is called once per frame
	void Update () 
    {
	
	}
    void OnCollisionEnter(Collision c)
    {
        Explosion_Event.Invoke((GameObject)GameObject.Instantiate(Explosion_Effect, transform.position, Quaternion.identity), Owning_Player);
        GameObject.Destroy(this.gameObject);
    }
}