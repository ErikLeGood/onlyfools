﻿using UnityEngine;
using System.Collections;

public class GlobalSpeedModifier : MonoBehaviour
{
    public float Speed = 1.0f;
	
	void Update()
    {
        Time.timeScale = Speed;
	}
}