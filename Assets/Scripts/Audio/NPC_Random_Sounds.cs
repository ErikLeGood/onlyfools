﻿using UnityEngine;
using System.Collections;

public class NPC_Random_Sounds : MonoBehaviour
{
    public AudioClip[] clips;
    public AudioSource source;

    public float Min_Interval = 1f;
    public float Max_Interval = 60f;
    float timer = 0f;
    float interval = 0f;

    AudioClip GetRandomClip()
    {
        return clips[Random.Range(0, clips.Length)];
    }

	void Start ()
    {
        source = GetComponent<AudioSource>();
        interval = Random.Range(Min_Interval, Max_Interval);
	}
	
    //To Do: Make this A) call on a global library of sounds,
    // And B) Account for NPC info (Gender, panic state, infection state, etc.)
	void FixedUpdate()
    {
        if (enabled)
        {
            if (timer > interval)
            {
                timer = 0f;
                source.clip = GetRandomClip();
                source.loop = false;
                source.Play();
                interval = Random.Range(Min_Interval, Max_Interval);
            }
            if (!source.isPlaying)
            {
                timer += Time.deltaTime;
            }
        }
	}
}