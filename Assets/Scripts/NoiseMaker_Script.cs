﻿using UnityEngine;
using System.Collections;

public delegate void EventHandler_RangedTaunt(Vector3 source);
public delegate void EventHandler_RangedTauntBuilding(Vector3 source, Building_Behaviour targ);
public class NoiseMaker_Script : MonoBehaviour
{
    public static event EventHandler_RangedTaunt Ranged_Taunt_Event;
    public static event EventHandler_RangedTauntBuilding BuildingTauntEvent;
    public GameObject ParticlePrefab;
    bool fired = false;

	void Start ()
    {
	    for (int i = 0; i < Game_Manager.All_Zombies.Count; i++)
        {
            Ranged_Taunt_Event += Game_Manager.All_Zombies[i].TauntedBySound;
            BuildingTauntEvent += Game_Manager.All_Zombies[i].TauntedToBulding;
        }
	}

    void OnTriggerEnter(Collider c)
    {
        if (!fired)
        {
            if (c.GetComponent<Building_Behaviour>())
            {
                if (BuildingTauntEvent != null)
                    BuildingTauntEvent.Invoke(transform.position, c.GetComponent<Building_Behaviour>());
            }
            else
            {
                if (Ranged_Taunt_Event != null)
                    Ranged_Taunt_Event.Invoke(transform.position);
            }

            GameObject.Destroy(this.gameObject, 0.5f);
            fired = true;
        }
        //Debug.Log(c.gameObject.name);
        GameObject.Instantiate(ParticlePrefab, transform.position + Vector3.up, Quaternion.identity);
    }
}