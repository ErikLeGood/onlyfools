﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public enum Actor_Type
{
    Player, Citizen, Doctor, Police, Zombie
}

[System.Serializable]
public enum Actor_Action
{
    Idle, Walking, Running, Attacking
}

public class Actor_Data : MonoBehaviour 
{
    public Actor_Type ActorType;
    public Actor_Action CurrentAction = Actor_Action.Idle;

    //[HideInInspector]
    public Building_Behaviour Containing_Building;
    NPC_Citizen citizenScript;
    public string Forename = "";
    public string Surname = "";
    public Gender gender;
    
    [HideInInspector]
    public UnityEngine.AI.NavMeshAgent navAgent;

    public float Walk_Speed = 3.5f;
    public float Run_Speed = 8f;

    float recalc_timer = 0f;

    [HideInInspector]
    public bool Selected = false;

    public GameObject PlayerAvatar_NavMarker;

    public Avatar_Movement_Control optional_Player_Marker;

    public void Stop()
    {
        navAgent.Stop();
    }

    void Awake()
    {
        navAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        citizenScript = GetComponent<NPC_Citizen>();
        if (ActorType != Actor_Type.Player)
            optional_Player_Marker = null;
    }

    public bool Running
    {
        get { return running; }
    }
    private bool running = false;

    public void UpdateSpeed(float newSpeed)
    {
        navAgent.speed = newSpeed;
        Animator[] anims = GetComponentsInChildren<Animator>();
        for (int i = 0; i < anims.Length; i++)
        {
            anims[i].speed = 1f / (Walk_Speed / navAgent.speed);
        }
    }

    void UpdateSpeedBasedOnWeather()
    {
        if (Weather.CurrentWeather == WeatherType.Fog)
        {
            UpdateSpeed(Random.Range(Walk_Speed * 0.5f, Walk_Speed * 0.75f));
        }
        else if (Weather.CurrentWeather == WeatherType.Rain || Weather.CurrentWeather == WeatherType.Storm)
        {
            UpdateSpeed(Random.Range(Walk_Speed * 1.3f, Walk_Speed * 1.5f));
        }
        else
        {
            UpdateSpeed(Random.Range(Walk_Speed * 0.5f, Walk_Speed * 1.5f));
        }
    }

    public void StartMoving(Vector3 destination, bool Is_Running, float? Speed_Override = null)
    {
        if (Is_Running)
        {
            running = true;
            CurrentAction = Actor_Action.Running;
            NavMeshAgent_Helpers.EqualizeLayerCosts(navAgent);
            navAgent.speed = 8f;
        }
        else
        {
            running = false;
            CurrentAction = Actor_Action.Walking;
            NavMeshAgent_Helpers.RevertLayerCosts(navAgent);
            if (ActorType == Actor_Type.Citizen)
            {
                UpdateSpeedBasedOnWeather();
            }
            else
            {
                navAgent.speed = 3.5f;
            }
        }
        if (ActorType == Actor_Type.Player)
        {
            if (Speed_Override != null)
            {
                navAgent.speed = Speed_Override.Value;
            }
            PlayerAvatar_NavMarker.SetActive(true);
            PlayerAvatar_NavMarker.transform.position = destination;
            UnityEngine.AI.NavMeshPath path = new UnityEngine.AI.NavMeshPath();
            navAgent.CalculatePath(destination, path);
            navAgent.path = path;
            navAgent.Resume();
        }
        else if (ActorType == Actor_Type.Police)
        {
            UnityEngine.AI.NavMeshPath path = new UnityEngine.AI.NavMeshPath();
            navAgent.CalculatePath(destination, path);
            navAgent.path = path;
            navAgent.Resume();
        }
        else
        {
            navAgent.updatePosition = false;
            navAgent.SetDestination(destination);
            navAgent.Resume();
            navAgent.updatePosition = true;
        }
    }

	void Start()
    {
	
	}

    void ReturnToIdleChecks()
    {
        if (CurrentAction == Actor_Action.Running || CurrentAction == Actor_Action.Walking)
        {
            if (!citizenScript || (citizenScript && citizenScript.panic_State == NPC_Panic_State.Calm))
            {
                #region Proximity Check
                if (Vector3.Distance(transform.position, navAgent.destination) <= 1f)
                {
                    CurrentAction = Actor_Action.Idle;
                    if (ActorType == Actor_Type.Player)
                    {
                        PlayerAvatar_NavMarker.SetActive(false);
                    }
                    return;
                }
                #endregion

                #region Timer / Larger Proximity Check
                if (Vector3.Distance(transform.position, navAgent.destination) <= navAgent.speed * 2)
                {
                    recalc_timer += Time.deltaTime;
                    if (recalc_timer >= 3f)
                    {
                        recalc_timer = 0f;
                        CurrentAction = Actor_Action.Idle;
                        if (ActorType == Actor_Type.Player)
                        {
                            PlayerAvatar_NavMarker.SetActive(false);
                        }
                        return;
                    }
                }
                else
                {
                    recalc_timer = 0f;
                }
                #endregion
            }
        }
    }
	
	void Update()
    {
        //if (ActorType == Actor_Type.Player)
        //{
        //    if (Selected)
        //    {
        //        PlayerAvatar_NavMarker.SetActive(true);
        //    }
        //    else
        //    {
        //        PlayerAvatar_NavMarker.SetActive(false);
        //    }
        //}
        ReturnToIdleChecks();
    }

    public float Distance_To_Nav_Destination()
    {
        return navAgent.remainingDistance;
    }
}