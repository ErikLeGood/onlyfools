﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public enum NPC_Panic_State
{
    Calm, Fleeing, Hiding, Angry, Hysterical
}
[System.Serializable]
public enum Gender
{
    Female, Male
}

public class NPC_Citizen : MonoBehaviour
{
    public float Health = 5;
    float hp_Timer = 0f;
    float hp_tick = 5f;

    public Actor_Data ActorControl;

    public Building_Behaviour Home;
    public Building_Behaviour Target_Building;
    public NPC_Zombie Bitten_By;

    public NPC_Panic_State panic_State = NPC_Panic_State.Calm;

    public Face face;
    Texture outfitTexture;
    public GameObject Male_Bod;
    public GameObject Female_Bod;
    public Renderer[] SkinRenderers;
    public Renderer[] ClothesRenderers;
    public Renderer[] FaceRenderers;
    public Renderer[] HairRenderers_Male;
    public Renderer[] HairRenderers_Female;

    CitizenThoughtTextManager ThoughtTextManager;

    public GameObject InfectionMarker;

    GameObject Panic_Source;

    public Actor_Data Attack_Target;

    [HideInInspector]
    public bool Chased_By_Zombie = false;
    public Avatar_Movement_Control InfectionPointer;
    public bool Infected = false;
    public bool Aware_Of_Infection = false;
    float awarenessTimer = 0f;
    float awarenessTick = 10f;
    int Number_Of_Checks = 0;
    public float flee_BaseTimer = 5f;
    float flee_Duration = 0f;
    float fleeTimer = 0f;
    float panic_Timer = 0f;
    float panic_BaseLength = 20f;
    float panic_Duration = 0f;
    Color initialColor;

    public int KillCount = 0;

    bool immune = false;
    float immunityTimer = 0f;
    float immunityDuration = 10f;

    public int Base_Bravery = 0;
    public int Base_Responsibility = 0;
    public int PreviousCombatSuccessFactor = 0;

    float attack_Damage = 2f;
    float attack_timer = 0f;
    float attack_speed = 1f;
    float Attack_Range = 2f;

    float drug_Resist = 1f;

    float RandomTextTimer = 0f;
    float RandomTextInterval = 30f;

    public void GoHome(bool RunThere)
    {
        if (Home)
        {
            Target_Building = Home;
            ActorControl.StartMoving(Target_Building.DoorPoint.position, RunThere);
        }
    }

    #region Infection Functions
    public void Infect(Avatar_Movement_Control contagion, bool Injury)
    {
        if (!Infected && !immune)
        {
            InfectionMarker.SetActive(true);
            TurnSkinInfected();
            
            GetComponent<SphereCollider>().enabled = true;
            Number_Of_Checks = 0;
            InfectionPointer = contagion;
            Infected = true;
            if (panic_State != NPC_Panic_State.Calm)
            {
                Self_Infected_Check(true);
            }
            if (Injury)
            {
                Stats.Infections_Biting++;
            }

            Game_Manager.Record_Infection();
        }
    }

    public void Cure()
    {
        if (Infected)
        {
            InfectionMarker.SetActive(false);
            RevertSkin();
            Number_Of_Checks = 0;
            InfectionPointer = null;
            Infected = false;
            Aware_Of_Infection = false;
            ActorControl.CurrentAction = Actor_Action.Idle;
            panic_State = NPC_Panic_State.Calm;

            Game_Manager.Record_Curing();
            immune = true;
        }
    }
    #endregion

    #region Cosmetic / Appearance Functions
    void UpdateFace()
    {
        if (panic_State == NPC_Panic_State.Calm)
        {
            ApplyCalmFace();
        }
        else
        {
            ApplyPanicFace();
        }
    }
    void ApplyCalmFace()
    {
        switch(ActorControl.gender)
        {
            case Gender.Male:
                FaceRenderers[0].material.SetTexture("_MainTex", face.Calm);
                break;
            case Gender.Female:
                FaceRenderers[1].material.SetTexture("_MainTex", face.Calm);
                break;
        }
    }
    void ApplyPanicFace()
    {
        switch (ActorControl.gender)
        {
            case Gender.Male:
                FaceRenderers[0].material.SetTexture("_MainTex", face.Panicked);
                break;
            case Gender.Female:
                FaceRenderers[1].material.SetTexture("_MainTex", face.Panicked);
                break;
        }
    }
    public void RandomizeAppearance()
    {
        if (Random.Range(0, 2) == 0)
        {
            ActorControl.gender = Gender.Male;
            face = Face_Catalogue.Get_Random_Face(Gender.Male);
            Male_Bod.SetActive(true);
            Female_Bod.SetActive(false);
            ActorControl.Forename = Citizen_Name_Palette.GetRandomMaleForename();
        }
        else
        {
            ActorControl.gender = Gender.Female;
            face = Face_Catalogue.Get_Random_Face(Gender.Female);
            Male_Bod.SetActive(false);
            Female_Bod.SetActive(true);
            ActorControl.Forename = Citizen_Name_Palette.GetRandomFemaleForename();
        }
        ActorControl.Surname = Citizen_Name_Palette.GetRandomSurname();

        if (ActorControl.Forename == "Gary" && ActorControl.Surname == "Oak")
        {
            Debug.Log("Gary Oak detected! Beware!");
        }

        outfitTexture = Face_Catalogue.GetRandomCitizenOutfit(ActorControl.gender);
        ApplyOutfit();
        RandomizeHair();
    }
    void ApplyOutfit()
    {
        if (ActorControl.gender == Gender.Male)
        {
            SkinRenderers[0].materials[1].SetTexture("_MainTex", outfitTexture);
            //SkinRenderers[0].materials[1].SetColor("_EmissionColor", initialColor);
        }
        else
        {
            SkinRenderers[1].materials[0].SetTexture("_MainTex", outfitTexture);
            //SkinRenderers[1].materials[1].SetColor("_EmissionColor", initialColor);
        }
    }
    public void RandomizeSkin(bool set_Initial)
    {
        Color col = SkinRenderers[0].material.GetColor("_Color");
        float mod = Random.Range(0.1f, 1.2f);
        col.r *= mod;
        col.g *= mod;
        col.b *= mod;
        col.a = 255f;

        SkinRenderers[0].materials[0].SetColor("_Color", col);
        SkinRenderers[0].materials[0].SetColor("_EmissionColor", col);
        SkinRenderers[1].materials[1].SetColor("_Color", col);
        SkinRenderers[1].materials[1].SetColor("_EmissionColor", col);

        if (set_Initial)
            initialColor = col;
    }
    public void RevertSkin()
    {
        if (ActorControl.gender == Gender.Male)
        {
            SkinRenderers[0].materials[0].SetColor("_Color", initialColor);
            SkinRenderers[0].materials[0].SetColor("_EmissionColor", initialColor);
        }
        else
        {
            SkinRenderers[1].materials[1].SetColor("_Color", initialColor);
            SkinRenderers[1].materials[1].SetColor("_EmissionColor", initialColor);
        }
    }
    public void TurnSkinInfected()
    {
        Color col = new Color(0, 1.4f, 0f, 1f);

        if (ActorControl.gender == Gender.Male)
        {
            SkinRenderers[0].materials[0].SetColor("_Color", col);
            SkinRenderers[0].materials[0].SetColor("_EmissionColor", col);
        }
        else
        {
            SkinRenderers[1].materials[1].SetColor("_Color", col);
            SkinRenderers[1].materials[1].SetColor("_EmissionColor", col);
        }
    }
    public void RandomizeHair()
    {
        for (int i = 0; i < HairRenderers_Male.Length; i++)
        {
            HairRenderers_Male[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < HairRenderers_Female.Length; i++)
        {
            HairRenderers_Female[i].gameObject.SetActive(false);
        }

        int selection = -1;
        if (ActorControl.gender == Gender.Male)
        {
            selection = Random.Range(-1, HairRenderers_Male.Length);
            if (selection >= 0)
            {
                HairRenderers_Male[selection].gameObject.SetActive(true);
                HairRenderers_Male[selection].material.SetColor("_Color", new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f)));
            }
        }
        else
        {
            selection = Random.Range(-1, HairRenderers_Female.Length);
            if (selection >= 0)
            {
                HairRenderers_Female[selection].gameObject.SetActive(true);
                HairRenderers_Male[selection].material.SetColor("_Color", new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f)));
            }
        }
    }
    #endregion

    void Awake()
    {
        RandomTextInterval = Random.Range(5f, 60f);
        ThoughtTextManager = GetComponentInChildren<CitizenThoughtTextManager>();
        ActorControl = GetComponent<Actor_Data>();
        InfectionMarker.SetActive(false);
        RandomizeAppearance();
        RandomizeSkin(true);
        ApplyCalmFace();

        Base_Responsibility = Random.Range(0, 5);
        Base_Bravery = Random.Range(0, 5);

        NPC_Police.Fired_A_Shot_Event += Heard_Gunshot_Event;
        Weather.WeatherChangeEvent += ReactToWeather;
    }

    void OnDestroy()
    {
        NPC_Police.Fired_A_Shot_Event -= Heard_Gunshot_Event;
        Weather.WeatherChangeEvent -= ReactToWeather;
    }

	void Start()
    {
        
	}

    void RandomTextUpdate()
    {
        if (ThoughtTextManager)
        {
            RandomTextTimer += Time.deltaTime;
            if (RandomTextTimer >= RandomTextInterval)
            {
                RandomTextInterval = Random.Range(5f, 60f);
                ThoughtTextManager.ShowText(panic_State);
                RandomTextTimer = 0f;
            }
        }
    }

    #region Behaviour State Updating
    void Idle_State_Update()
    {
        bool do_Movement = true;
        if (Target_Building != null)
        {
            if (Vector3.Distance(transform.position, Target_Building.DoorPoint.position) <= 2f)
            {
                if (Target_Building.Take_In_Actor(ActorControl))
                {
                    do_Movement = false;
                }
            }
        }

        if (do_Movement)
        {
            if (Aware_Of_Infection && Infected && Game_Manager.Buildings_By_Type[BuildingType.Clinic].Count > 0)
            {
                Target_Building = Game_Manager.ClosestBuildingOfTypeTo(BuildingType.Clinic, transform.position);
                if (Target_Building != null)
                {
                    ActorControl.StartMoving(Target_Building.DoorPoint.position, false);
                }
            }
            else
            {
                Self_Infected_Check(false);

                Building_Behaviour targ = Game_Manager.GetRandomBuildingOfType(BuildingType.Cinema);
                if (targ != null && !targ.Locked && targ.HasSpace && Random.Range(0,10) >= 4)
                {
                    Target_Building = targ;
                }
                else if (Game_Manager.Buildings_By_Type[BuildingType.Shop].Count > 1 && Random.Range(0, 10) >= 3)
                {
                    Target_Building = Game_Manager.GetRandomBuildingOfType(BuildingType.Shop);
                }
                else
                {
                    Target_Building = Game_Manager.GetRandomBuilding();
                }
            
                ActorControl.StartMoving(Target_Building.DoorPoint.position, false);
                
            }
        }
    }

    void Attack_State_Update()
    {
        if (Attack_Target != null)
        {
            transform.LookAt(Attack_Target.transform);
            transform.rotation = Quaternion.Euler(0f, transform.rotation.eulerAngles.y, 0f);
            ActorControl.StartMoving(Attack_Target.transform.position, true);
            if (Vector3.Distance(transform.position, Attack_Target.transform.position) <= Attack_Range)
            {
                attack_timer += Time.deltaTime;
                if (attack_timer >= attack_speed)
                {
                    attack_timer = 0f;
                    switch (Attack_Target.ActorType)
                    {
                        case Actor_Type.Zombie:
                            if (Attack_Target.GetComponent<NPC_Zombie>().Take_Damage(ActorControl, attack_Damage))
                            {
                                Health += 1f;
                                attack_Damage += 0.5f;
                                Attack_Target = null;
                                PreviousCombatSuccessFactor++;
                                Stats.Zombies_Killed_By_Civs++;
                                KillCount++;
                            }
                            break;
                        case Actor_Type.Player:
                            Attack_Target.optional_Player_Marker.TakeDamage(attack_Damage);
                            break;
                    }
                }
            }
            else
            {
                attack_timer = 0f;
            }
        }
        else
        {
            Check_For_Zombies_And_Attack();
        }
    }

    void Hide_State_Update()
    {
        if (Game_Manager.All_Buildings.Count == 0)
        {
            Enter_Panic_State();
        }
        bool do_Movement = true;
        if (Target_Building != null)
        {
            if (Vector3.Distance(transform.position, Target_Building.DoorPoint.position) <= 2f)
            {
                if (Target_Building.Take_In_Actor(ActorControl))
                {
                    Target_Building = null;
                    do_Movement = false;
                    panic_Duration = panic_BaseLength * Random.Range(0.5f, 2f);
                    panic_Timer = 0f;
                }
            }
        }
        else
        {
            if (ActorControl.Containing_Building != null)
            {
                if (!Chased_By_Zombie)
                {
                    panic_Timer += Time.deltaTime;
                    if (panic_Timer >= panic_Duration)
                    {
                        panic_Timer = 0f;
                        panic_State = NPC_Panic_State.Calm;
                    }
                }
            }
        }
        if (do_Movement)
        {
            if (Game_Manager.Buildings_By_Type[BuildingType.House].Contains(Home))
            {
                Target_Building = Home;
            }
            else
            {
                Target_Building = Game_Manager.ClosestBuildingTo(transform.position);
            }
            ActorControl.StartMoving(Target_Building.DoorPoint.position, true);
        }
    }

    void Flee_State_Update()
    {
        if (Panic_Source != null)
        {
            fleeTimer += Time.deltaTime;
            if (fleeTimer >= flee_Duration)
            {
                fleeTimer = 0f;
                flee_Duration = 0f;
                Enter_Panic_State();
            }
            Vector3 awayVec = -(Panic_Source.transform.position - transform.position).normalized;
            awayVec *= 10f;
            ActorControl.StartMoving(transform.position + awayVec, true);
        }
        else
        {
            Enter_Panic_State();
        }
    }

    void Enter_Panic_State()
    {
        panic_State = NPC_Panic_State.Hysterical;
        panic_Duration = panic_BaseLength + Random.Range(-15f, 15f);
        panic_Timer = 0f;
    }

    void Panic_State_Update()
    {
        if (fleeTimer > flee_Duration || ActorControl.Distance_To_Nav_Destination() < 1f)
        {
            flee_Duration = Random.Range(1, 5f);
            fleeTimer = 0f;
            ActorControl.StartMoving(transform.position + new Vector3(Random.Range(-5f, 5f), 0f, Random.Range(-5f, 5f)), true);
        }
        else
        {
            fleeTimer += Time.deltaTime;
        }
        panic_Timer += Time.deltaTime;
        if (panic_Timer >= panic_Duration)
        {
            panic_Timer = 0f;
            panic_State = NPC_Panic_State.Calm;
            ActorControl.CurrentAction = Actor_Action.Idle;
        }
    }
    #endregion

    #region Zombie Checks
    void Check_For_Zombies_And_Attack()
    {
        for (int i = 0; i < Game_Manager.All_Zombies.Count; i++)
        {
            if (Vector3.Distance(transform.position, Game_Manager.All_Zombies[i].transform.position) <= perception_Range)
            {
                Attack_Target = Game_Manager.All_Zombies[i].GetComponent<Actor_Data>();
            }
        }
    }
    void Check_For_Zombies_And_Roll_Panic()
    {
        if (panic_State != NPC_Panic_State.Fleeing && panic_State != NPC_Panic_State.Angry)
        {
            for (int i = 0; i < Game_Manager.All_Zombies.Count; i++)
            {
                NPC_Zombie zombie = Game_Manager.All_Zombies[i];
                if (zombie.gameObject.activeInHierarchy)
                {
                    if (Vector3.Distance(zombie.transform.position, transform.position) <= perception_Range * 0.5f)
                    {
                        Panic_Source = zombie.gameObject;
                        Chased_By_Zombie = true;
                        Roll_Panic(zombie.gameObject, 1); //add scariness modifier property to zombie class
                        break;
                    }
                }
            }
        }
    }
    #endregion

    public void Health_Tick()
    {
        if (panic_State == NPC_Panic_State.Angry)
        {
            drug_Resist = 2f;
        }
        else
        {
            drug_Resist = 1f;
        }
        hp_Timer += Time.deltaTime;
        if (hp_Timer >= hp_tick)
        {
            hp_Timer = 0f;
            Health -= Random.Range(InfectionPointer.C_Data.Lethality * 0.1f, InfectionPointer.C_Data.Lethality * 1.1f) / drug_Resist;
        }
        
    }

    public void Roll_Panic(GameObject source, int Scariness = 0, Avatar_Movement_Control launcher = null)
    {
        float ScareFactor = 0f;
        float HealthDiff = 5 / Health;
        float distance = Vector3.Distance(source.transform.position, transform.position);

        ScareFactor = 1 + Scariness + (1 / distance);
        if (source.GetComponent<Actor_Data>())
        {
            if (source.GetComponent<Actor_Data>().Surname == ActorControl.Surname)
            {
                Scariness *= 2;
            }
        }
        float roll = ((Base_Bravery + Base_Responsibility + PreviousCombatSuccessFactor) - Scariness) * HealthDiff;

        if (ActorControl.Forename == "Gary" && ActorControl.Surname == "Oak")
        {
            if (launcher != null)
            {
                Attack_Target = launcher.actorControl;
                panic_State = NPC_Panic_State.Angry;
            }
        }

        else if (roll <= 0.5f)
        {
            panic_State = NPC_Panic_State.Hysterical;
        }
        else if (roll > 0.5f && roll <= 5)
        {
            Enter_Flee_State(source);
        }
        else if (roll > 5 && roll <= 10 && Game_Manager.All_Buildings.Count > 0)
        {
            panic_State = NPC_Panic_State.Hiding;
        }
        else if (roll > 10)
        {
            if (launcher != null)
            {
                Attack_Target = launcher.actorControl;
                panic_State = NPC_Panic_State.Angry;
            }
            else if (source.GetComponent<NPC_Zombie>() || source.GetComponent<NPC_Police>())
            {
                Attack_Target = source.GetComponent<Actor_Data>();
                panic_State = NPC_Panic_State.Angry;
            }
        }
    }

    #region Event Recipients
    public void Capsule_Event(GameObject source, Avatar_Movement_Control launcher)
    {
        if (transform)
        {
            if (source)
            {
                if (Vector3.Distance(source.transform.position, transform.position) <= perception_Range)
                {
                    Panic_Source = source;
                    Roll_Panic(source, 2, launcher);
                    if (!Infected)
                    {
                        if (Vector3.Distance(source.transform.position, transform.position) <= 10f)
                        {
                            Infect(launcher, false);
                        }
                        Stats.Infections_Capsule++;
                    }
                    //int roll = Random.Range(0, 1000);
                    //if (roll > 0 && roll <= 400)
                    //{
                    //    panic_State = NPC_Panic_State.Hiding;
                    //}
                    //else if (roll > 400 && roll <= 700)
                    //{
                    //    panic_State = NPC_Panic_State.Fleeing;
                    //}
                    //else if (roll > 700 && roll < 800)
                    //{
                    //    Self_Infected_Check(true);
                    //}
                    //else if (roll == 0 && !launcher.Invisible)
                    //{
                    //    panic_State = NPC_Panic_State.Attacking;
                    //    Attack_Target = launcher.actorControl;
                    //}
                    //else
                    //{
                    //    panic_State = NPC_Panic_State.Hysterical;
                    //}

                    
                }
            }
        }
        Capsule_Script.Explosion_Event -= Capsule_Event;
    }
    public void Heard_Gunshot_Event(NPC_Police firer, Actor_Type TargetType)
    {
        if (Vector3.Distance(firer.transform.position, transform.position) <= perception_Range)
        {
            if (TargetType != Actor_Type.Citizen)
            {
                Roll_Panic(firer.gameObject, 5, null);
            }
            else
            {
                Roll_Panic(firer.gameObject, -5, null);
            }
        }
    }
    void ReactToWeather(WeatherType weather)
    {
        if (weather == WeatherType.Rain || weather == WeatherType.Storm)
        {
            ActorControl.UpdateSpeed(Random.Range(ActorControl.Walk_Speed * 1.5f, ActorControl.Walk_Speed * 1.9f));
        }
        else if (weather == WeatherType.Fog)
        {
            ActorControl.UpdateSpeed(Random.Range(ActorControl.Walk_Speed * 0.25f, ActorControl.Walk_Speed * 0.75f));
        }
        else
        {
            ActorControl.UpdateSpeed(Random.Range(ActorControl.Walk_Speed * 0.5f, ActorControl.Walk_Speed * 1.5f));
        }
    }
    #endregion

    void Enter_Flee_State(GameObject source)
    {
        if (Vector3.Distance(source.transform.position, transform.position) <= perception_Range)
        {
            Panic_Source = source;
            panic_State = NPC_Panic_State.Fleeing;
            fleeTimer = 0f;
            flee_Duration = flee_BaseTimer + Random.Range(-2f, 2f);
        }
    }

    public void Clear_Building_Target()
    {
        Target_Building = null;
    }

    #region Self-Assessment

    void Self_Infected_Check(bool Run_To_Hospital)
    {
        if (panic_State == NPC_Panic_State.Calm)
        {
            if (Infected && !Aware_Of_Infection)
            {
                if (Number_Of_Checks == 0)
                {
                    float roll = Random.Range(0f, 10f);
                    if (roll > InfectionPointer.C_Data.Subtlety)
                    {
                        Aware_Of_Infection = true;
                        Building_Behaviour bb = Game_Manager.ClosestBuildingOfTypeTo(BuildingType.Clinic, transform.position);
                        if (bb)
                        {
                            ActorControl.StartMoving(bb.DoorPoint.position, Run_To_Hospital);
                        }
                    }
                }
            }
        }
    }

    public bool Is_Dead_Check(bool overrideB = false)
    {
        if (Health <= 0)
        {
            //if (Infected)
            //Game_Manager.Spawn_Zombie(transform.position, InfectionPointer);

            Game_Manager.Kill_Citizen(this, overrideB);
            return true;
        }
        return false;
    }

    #endregion

    #region Damage Reception
    /// <summary>
    /// Applies Damage. Returns True if the damage was fatal, otherwise false.
    /// </summary>
    /// <param name="attacker"></param>
    /// <param name="Amount"></param>
    /// <returns></returns>
    public bool TakeDamage(NPC_Zombie attacker, float Amount)
    {
        Health -= Amount;
        Enter_Flee_State(attacker.gameObject);
        Chased_By_Zombie = true;

        if (Health <= 0)
        {
            return true;
        }
        return false;
    }
    public void TakeDamage(NPC_Police attacker, float Amount)
    {
        Health -= Amount;
        Roll_Panic(attacker.gameObject, 2, null);
        //Chased_By_Zombie = true;
    }
    #endregion

    void Update()
    {
        if (Game_Manager.State != GameState.Paused)
        {
            if (Is_Dead_Check())
                return;

            if (gameObject.activeInHierarchy)
            {
                UpdatePerceptionRange();
                Check_For_Zombies_And_Roll_Panic();

                RandomTextUpdate();

                UpdateFace();

                if (immune)
                {
                    immunityTimer += Time.deltaTime;
                    if (immunityTimer >= immunityDuration)
                    {
                        immune = false;
                    }
                }

                if (Infected && InfectionPointer != null)
                {
                    //GetComponent<SphereCollider>().radius = InfectionPointer.C_Data.Range;
                    HandleCrossInfection();

                    Health_Tick();

                    if (!Aware_Of_Infection && panic_State == NPC_Panic_State.Calm)
                    {
                        awarenessTimer += Time.deltaTime;
                        if (awarenessTimer >= awarenessTick)
                        {
                            awarenessTimer = 0f;
                            Self_Infected_Check(false);
                        }
                    }
                }

                #region Calm Updating
                if (panic_State == NPC_Panic_State.Calm)
                {
                    switch (ActorControl.CurrentAction)
                    {
                        case Actor_Action.Idle:
                            Idle_State_Update();
                            break;
                        case Actor_Action.Walking:
                            break;
                        case Actor_Action.Running:
                            break;
                    }
                }
                #endregion

                #region Panic Updating
                else
                {
                    UpdateFamilyUnit();
                    switch (panic_State)
                    {
                        case NPC_Panic_State.Hiding:
                            Hide_State_Update();
                            break;
                        case NPC_Panic_State.Fleeing:
                            Flee_State_Update();
                            break;
                        case NPC_Panic_State.Hysterical:
                            Panic_State_Update();
                            break;
                        case NPC_Panic_State.Angry:
                            Attack_State_Update();
                            break;
                    }
                }
                #endregion
            }
        }
    }

    void UpdatePerceptionRange()
    {
        perception_Range = Base_Perception_Range;
        if (Weather.CurrentWeather == WeatherType.Fog)
        {
            perception_Range *= 0.5f;
        }
    }

    void HandleCrossInfection()
    {
        if (transmissions < 2)
        {
            for (int i = 0; i < Game_Manager.All_Citizens.Count; i++)
            {
                if (Game_Manager.All_Citizens[i].gameObject != this.gameObject)
                {
                    if (Vector3.Distance(transform.position, Game_Manager.All_Citizens[i].transform.position) <= InfectionPointer.C_Data.Range)
                    {
                        NPC_Citizen npc = Game_Manager.All_Citizens[i].gameObject.GetComponent<NPC_Citizen>();
                        if (npc)
                        {
                            if (!npc.Infected)
                            {
                                float roll = Random.Range(0f, 10f);
                                if (roll < InfectionPointer.C_Data.Infectivity)
                                {
                                    npc.Infect(InfectionPointer, false);
                                    Stats.Infections_Hygiene++;
                                    transmissions++;
                                    if (transmissions == 2)
                                    {
                                        GetComponent<SphereCollider>().enabled = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    void UpdateFamilyUnit()
    {
        int braveryValue = Base_Bravery;
        NPC_Citizen citi, citiHold = null;
        if (Game_Manager.Actors_By_Surname.ContainsKey(ActorControl.Surname))
        {
            for (int i = 0; i < Game_Manager.Actors_By_Surname[ActorControl.Surname].Count; i++)
            {
                if (Game_Manager.Actors_By_Surname[ActorControl.Surname][i])
                {
                    citi = Game_Manager.Actors_By_Surname[ActorControl.Surname][i].GetComponent<NPC_Citizen>();
                    if (citi)
                    {
                        if (braveryValue < citi.Base_Bravery)
                        {
                            if (Vector3.Distance(transform.position, citi.transform.position) <= perception_Range)
                            {
                                braveryValue = citi.Base_Bravery;
                                citiHold = citi;
                            }
                        }
                    }
                }
            }
            if (citiHold != null)
            {
                panic_State = citiHold.panic_State;
                if (citiHold.Target_Building != null)
                {
                    Target_Building = citiHold.Target_Building;
                }
                if (Attack_Target != null)
                {
                    Attack_Target = citiHold.Attack_Target;
                }
            }
        }
    }

    int transmissions = 0;
    void OnTriggerEnter(Collider other)
    {
        if (Infected && InfectionPointer != null)
        {
            if (transmissions < 2)
            {
                if (other.gameObject != this.gameObject)
                {
                    NPC_Citizen npc = other.gameObject.GetComponent<NPC_Citizen>(); 
                    if (npc)
                    {
                        if (!npc.Infected)
                        {
                            float roll = Random.Range(0f, 10f);
                            if (roll < InfectionPointer.C_Data.Infectivity)
                            {
                                npc.Infect(InfectionPointer, false);
                                Stats.Infections_Hygiene++;
                                transmissions++;
                                if (transmissions == 2)
                                {
                                    GetComponent<SphereCollider>().enabled = false;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    float perception_Range = 30f;
    public float Base_Perception_Range = 30f;
    public float perception_Angle = 90f;
}