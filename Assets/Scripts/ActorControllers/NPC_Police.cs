﻿using UnityEngine;
using System.Collections;

public delegate void EventHandler_ZombieSighting(NPC_Police CallingObject);
public delegate void EventHandler_GunShot(NPC_Police Firer, Actor_Type Target_Type);

public class NPC_Police : MonoBehaviour
{
    public int Personal_Panic_Level = 0;
    public int Panic_Level_Mid_Threshold = 5;
    public int Panic_Level_High_Threshold = 20;

    public int KillCount = 0;

    public static bool ShowRanges = false;
    public bool ShowRangeLocal = false;
    public float Health = 10f;
    public static event EventHandler_ZombieSighting sawZombie_Event;
    public static event EventHandler_GunShot Fired_A_Shot_Event;
    public GameObject Range_Marker;
    public float Attack_Range = 15f;
    public float BASE_Perception_Range = 30f;
    float Perception_Range = 0f;

    float Attack_Rate = 1f;
    float Attack_Timer = 0f;
    float Attack_Damage = 2.5f;
    public static float Damage = 2.5f;

    public bool Infected = false;
    public Avatar_Movement_Control Infection_Pointer;
    //float infect_Timer = 0f;
    //float infect_Tick = 5f;
    
    Ray ray;
    LayerMask lMaskZ;
    LayerMask lMaskC;

    [HideInInspector]
    public Actor_Data ActorControl;

    public Actor_Data Target;

    Vector3? Destination = null;

    public GameObject Bullet_Prefab;

    public Renderer SkinRenderer;
    public Renderer FaceRenderer;
    public Renderer[] HairRenderers;

    public Face face;

    public void RandomizeAppearance()
    {
        Color col = SkinRenderer.materials[0].GetColor("_Color");
        float mod = Random.Range(0.1f, 1.2f);
        col.r *= mod;
        col.g *= mod;
        col.b *= mod;
        col.a = 255f;
        SkinRenderer.materials[0].SetColor("_Color", col);
        ActorControl.gender = Random.Range(0, 2) == 0 ? Gender.Male : Gender.Female;
        face = Face_Catalogue.Get_Random_Face(ActorControl.gender);
        ApplyCalmFace();
    }

    void ApplyCalmFace()
    {
        FaceRenderer.material.SetTexture("_MainTex", face.Calm);
    }
    void ApplyPanicFace()
    {
        FaceRenderer.material.SetTexture("_MainTex", face.Panicked);
    }
    void UpdateFace()
    {
        
    }

    public void Infect(Avatar_Movement_Control infector)
    {
        if (!Infected)
        {
            Infected = true;
            Infection_Pointer = infector;
        }
    }
    public void Cure()
    {
        if (Infected)
        {
            Infected = false;
            Infection_Pointer = null;
        }
    }


    void UpdatePerceptionRange()
    {
        Perception_Range = BASE_Perception_Range;
        if (Weather.CurrentWeather == WeatherType.Fog)
        {
            Perception_Range *= 0.5f;
        }
    }
    bool Targetted_LOS_Check()
    {
        if (Target)
        {
            if (Target.ActorType == Actor_Type.Zombie)
            {
                return LOS_Check_For_Zombie(Target.GetComponent<NPC_Zombie>());
            }
            else if (Target.ActorType == Actor_Type.Citizen)
            {
                return LOS_Check_For_Citizen(Target.GetComponent<NPC_Citizen>());
            }
            else if (Target.ActorType == Actor_Type.Player)
            {
                return LOS_Check_For_Player(Target.optional_Player_Marker);
            }
        }
        return false;
    }
    bool LOS_Check_For_Zombie(NPC_Zombie targ)
    {
        bool output = false;
        if (targ != null)
        {
            RaycastHit hit;
            ray.origin = transform.position;
            ray.direction = (targ.transform.position - transform.position).normalized;
            float range = Vector3.Distance(transform.position, targ.transform.position);
            if (!Physics.Raycast(ray, out hit, range, lMaskZ.value))
            {
                output = true;
            }
            else if (hit.collider && hit.collider.GetComponent<NPC_Zombie>())
            {
                output = true;
            }
        }

        return output;
    }
    bool LOS_Check_For_Citizen(NPC_Citizen targ)
    {
        bool output = false;
        RaycastHit hit;
        ray.origin = transform.position;
        ray.direction = (targ.transform.position - transform.position).normalized;
        float range = Vector3.Distance(transform.position, targ.transform.position);
        if (!Physics.Raycast(ray, out hit, range, lMaskC.value))
        {
            output = true;
        }
        else if (hit.collider && hit.collider.GetComponent<NPC_Zombie>())
        {
            output = true;
        }

        return output;
    }
    bool LOS_Check_For_Player(Avatar_Movement_Control targ)
    {
        bool output = false;
        RaycastHit hit;
        ray.origin = transform.position;
        ray.direction = (targ.transform.position - transform.position).normalized;
        float range = Vector3.Distance(transform.position, targ.transform.position);
        if (!Physics.Raycast(ray, out hit, range, lMaskC.value))
        {
            output = true;
        }
        else if (hit.collider && hit.collider.GetComponent<Actor_Data>() && hit.collider.GetComponent<Actor_Data>().ActorType == Actor_Type.Player)
        {
            output = true;
        }
        return output;
    }

    void Awake()
    {
        Damage = Attack_Damage;
        ActorControl = GetComponent<Actor_Data>();
        lMaskZ = (1 << 10 | 1 << 11);
        lMaskC = (1 << 10);

        ActorControl.Forename = "Officer";
        ActorControl.Surname = "Law";

        RandomizeAppearance();
        sawZombie_Event += Got_Radio_Call;
    }

	void Start()
    {
	
	}

    void Wander_Update()
    {
        if (Destination == null || Vector3.Distance(transform.position, Destination.Value) <= 4f)
        {
            Destination = Game_Manager.GetRandomBuilding().DoorPoint.position;
            ActorControl.StartMoving(Destination.Value, false);
        }
        else if (Destination != null)
        {
            ActorControl.StartMoving(Destination.Value, false);
        }
    }

    public void CapsuleEvent(GameObject source, Avatar_Movement_Control launcher)
    {
        if (!launcher.Invisible)
        {
            if (Vector3.Distance(launcher.actorControl.transform.position, transform.position) <= Perception_Range * 0.5f)
            {
                RaycastHit hit;
                ray.origin = transform.position;
                ray.direction = (launcher.actorControl.transform.position - transform.position).normalized;

                //float range = Vector3.Distance(transform.position, launcher.transform.position);

                Adjust_Panic_Level(1, true);
                if (!Physics.Raycast(ray, out hit, Perception_Range, lMaskC.value))
                {
                    Target = launcher.actorControl;
                }
                
            }
            Capsule_Script.Explosion_Event -= CapsuleEvent;
        }
    }

    void AttackLoop()
    {
        ActorControl.Stop();
        Attack_Timer += Time.deltaTime;
        if (Attack_Timer >= Attack_Rate)
        {
            Personal_Panic_Level++;
            Attack_Timer = 0f;
            Bullet_Behaviour.Spawn(Bullet_Prefab.GetComponent<Bullet_Behaviour>(), transform.position + transform.forward.normalized, transform.rotation, Target.transform.position);
            Fired_A_Shot_Event.Invoke(this, Target.ActorType);

            if (Target.ActorType == Actor_Type.Zombie)
            {
                if (Target.GetComponent<NPC_Zombie>().Take_Damage(ActorControl, Attack_Damage))
                {
                    KillCount++;
                    Stats.Zombies_Killed_By_Cops++;
                }
            }

            if (Target.ActorType == Actor_Type.Player)
                Target.optional_Player_Marker.TakeDamage(Attack_Damage);

            if (Target.ActorType == Actor_Type.Citizen)
            {
                Target.GetComponent<NPC_Citizen>().TakeDamage(this, Attack_Damage);
            } 
        }
    }

    //TODO: This is where the AI For guard combat is done. It's shit. Improve it.
    void Pursue_Target_And_Attack()
    {
        if (!Target || Target.Containing_Building)
        {
            Target = null;
            return;
        }

        #region If Target is too close, Navigate Away
        if (Vector3.Distance(Target.transform.position, transform.position) <= Attack_Range * 0.5f)
        {
            if (Targetted_LOS_Check())
            {
                Attack_Timer = 0f;
                //Debug.Log("RETREAT!");
                ActorControl.StartMoving(transform.position + (-(Target.transform.position - transform.position).normalized * 10f), true);
                Adjust_Panic_Level(1, false);
            }
        }
        #endregion

        #region If Target Is Far Away, Navigate Closer
        else if (Vector3.Distance(Target.transform.position, transform.position) >= Attack_Range)
        {
            Attack_Timer = 0f;
            ActorControl.StartMoving(Target.transform.position, true);
        }
        #endregion

        #region Else If Target is within Range proceed to raycast check
        else
        {
            transform.LookAt(Target.transform.position);
            transform.rotation = Quaternion.Euler(0f, transform.rotation.eulerAngles.y, 0f);

            //RaycastHit hit;
            ray.origin = transform.position;
            ray.direction = (Target.transform.position - transform.position).normalized;
            float range = Vector3.Distance(transform.position, Target.transform.position);

            if (Targetted_LOS_Check())
                AttackLoop();
            
        }
        #endregion
    }

    void Scan_For_Targets()
    {
        int target_Index = -1;
        float Distance = 100000f;
        if (Target != null)
            Distance = Vector3.Distance(transform.position, Target.transform.position);

        #region Zombie Scan
        for (int i = 0; i < Game_Manager.All_Zombies.Count; i++)
        {
            float tempdist = Vector3.Distance(transform.position, Game_Manager.All_Zombies[i].transform.position);
            if (tempdist <= Perception_Range && tempdist <= Distance)
            {
                if (LOS_Check_For_Zombie(Game_Manager.All_Zombies[i]))
                {
                    Distance = tempdist;
                    target_Index = i;
                }
            }
        }
        if (target_Index != -1)
        {
            Target = Game_Manager.All_Zombies[target_Index].GetComponent<Actor_Data>();
            Adjust_Panic_Level(2, true);
            sawZombie_Event.Invoke(this);
        }
        #endregion
        else
        {
            if (Personal_Panic_Level >= Panic_Level_Mid_Threshold)
            {
                for (int i = 0; i < Game_Manager.All_Citizens.Count; i++)
                {
                    if (!Game_Manager.All_Citizens[i].ActorControl.Containing_Building)
                    {
                        if (Game_Manager.All_Citizens[i].Infected || Personal_Panic_Level >= Panic_Level_High_Threshold)
                        {
                            float tempdist = Vector3.Distance(transform.position, Game_Manager.All_Citizens[i].transform.position);
                            if (tempdist <= Perception_Range && tempdist <= Distance)
                            {
                                if (LOS_Check_For_Citizen(Game_Manager.All_Citizens[i]))
                                {
                                    Distance = tempdist;
                                    target_Index = i;
                                }
                            }
                        }
                    }
                }
                if (target_Index != -1)
                {
                    Target = Game_Manager.All_Citizens[target_Index].GetComponent<Actor_Data>();
                    //sawZombie_Event.Invoke(this);
                }
            }
        }
    }

    public void Got_Radio_Call(NPC_Police Caller)
    {
        if (Game_Manager.Buildings_By_Type[BuildingType.PoliceStation].Count > 0)
        {
            if (Target == null)
            {
                if (Caller && Caller != this)
                {
                    if (Vector3.Distance(Caller.transform.position, transform.position) <= Perception_Range * 2f)
                    {
                        //Add Police Station exists Check here
                        Target = Caller.Target;
                        Adjust_Panic_Level(1, true);
                    }
                }
            }
        }
    }

    public bool TakeDamage(Actor_Data attacker, float amount)
    {
        Health -= amount;

        if (Health < 0)
            Health = 0;

        if (Health == 0)
            return true;
        return false;
    }

    void OnDestroy()
    {
        NPC_Police.sawZombie_Event -= Got_Radio_Call;
    }
	
    public void Infected_Health_Tick()
    {
        
    }

    public string Return_Mood_Description()
    {
        string output = "";

        if (Personal_Panic_Level == 0)
        {
            output += "Calm";
        }
        else if (Personal_Panic_Level > 0 && Personal_Panic_Level < Panic_Level_Mid_Threshold)
        {
            output += "Agitated";
        }
        else if (Personal_Panic_Level >= Panic_Level_Mid_Threshold && Personal_Panic_Level < Panic_Level_High_Threshold)
        {
            output += "Alarmed!";
        }
        else if (Personal_Panic_Level >= Panic_Level_High_Threshold)
        {
            output += "Unbalanced!!";
        }
        return output;
    }

    public void Adjust_Panic_Level(int Amount, bool StopAtMidThreshold)
    {
        if (!StopAtMidThreshold && Personal_Panic_Level < Panic_Level_Mid_Threshold)
        {
            Personal_Panic_Level += Amount;
            //if (Game_Manager.Buildings_By_Type[BuildingType.PoliceStation].Count == 0)
            //{
            //    Personal_Panic_Level+= Amount;
            //}
        }
    }

    public void Health_Check_And_Death()
    {
        if (Health <= 0)
        {
            Game_Manager.Kill_Police(this);
        }
    }
    void RangeMarkerUpdate()
    {
        ShowRangeLocal = false;
        if (InformationSelection.selection is Actor_Data)
        {
            if (InformationSelection.selection as Actor_Data == ActorControl)
            {
                ShowRangeLocal = true;
            }
        }

        if (ShowRanges || ShowRangeLocal)
        {
            Range_Marker.SetActive(true);
            Range_Marker.transform.localScale = new Vector3(Perception_Range, Perception_Range, 5f);
        }
        else
        {
            Range_Marker.SetActive(false);
        }
    }
	void Update()
    {
        UpdatePerceptionRange();
        RangeMarkerUpdate();
        if (Game_Manager.State != GameState.Paused)
        {
            Scan_For_Targets();
            Infected_Health_Tick();
            if (Target == null)
            {
                Wander_Update();
            }
            else
            {
                Pursue_Target_And_Attack();
            }
            Health_Check_And_Death();
        }
	}
}