﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class ToolTip_Menus : MonoBehaviour
{
    public Text TitlePointer;
    public Text BodyPointer;

    public Transform Tooltip_Marker;

    bool show = false;

    int Current_Index = -1;

    List<string> Titles = new List<string>();
    List<string> Bodies = new List<string>();

    void Awake()
    {
        Configure_Lists();
    }

    void Configure_Lists()
    {
        // 0 (ZERO) = Points
        Titles.Add("Points");
        Bodies.Add("Points left to spend.");

        // 1 (ONE) = Items
        Titles.Add("Items");
        Bodies.Add("Things you use.");

        //2 (TWO) - Contagion
        Titles.Add("Contagion");
        Bodies.Add("Plague Attributes.");

        //3 (THREE) - Zombies
        Titles.Add("Zombies");
        Bodies.Add("Zombie Attributes.");

        //4 (FOUR) = Extras
        Titles.Add("Extras");
        Bodies.Add("Coming Soon...ish...");
        
        //5 (FIVE) = Capsule Count
        Titles.Add("Ammo");
        Bodies.Add("Capsules containing your Contagion. \nThrown by player.");

        //6 (SIX) - Remote Capsules
        Titles.Add("Remote Capsules");
        Bodies.Add("Carries contagion. \nRemote detonated.");

        //7 (SEVEN) - Lethality
        Titles.Add("Lethality");
        Bodies.Add("How quickly the infected become zombies.");

        //8 (EIGHT) - Contagiousness
        Titles.Add("Contagiousness");
        Bodies.Add("How easily your pathogen will spread between citizens.");

        //9 (NINE) - Subtlety
        Titles.Add("Subtlety");
        Bodies.Add("How unlikely it is that infect will visit hospitals.");

        //10 (TEN) - Damage
        Titles.Add("Damage");
        Bodies.Add("How hard the zombies hit stuff.");

        //11 (ELEVEN) - Speed
        Titles.Add("Speed");
        Bodies.Add("How quickly the zombies move.");

        //12 (TWELVE) - Toughness
        Titles.Add("Toughness");
        Bodies.Add("How much it takes to kill a zombie.");
    }

	void Start()
    {
	
	}
	
	void Update()
    {
        Tooltip_Marker.gameObject.SetActive(show);

        if (show)
        {
            Tooltip_Marker.position = Input.mousePosition;
            if (Current_Index >= 0)
            {
                TitlePointer.text = Titles[Current_Index];
                BodyPointer.text = Bodies[Current_Index];
            }
        }
	}

    public void ActivateTooltip(int index)
    {
        if (!show && index != Current_Index)
        {
            Current_Index = index;
            show = true;
        }
    }
    public void Clear()
    {
        if (show)
        {
            Current_Index = -1;
            show = false;
        }
    }
}