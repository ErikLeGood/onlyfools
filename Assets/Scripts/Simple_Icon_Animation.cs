﻿using UnityEngine;
using System.Collections;

public class Simple_Icon_Animation : MonoBehaviour 
{
    Vector3 initial_Pos = new Vector3();
    public float Max_Height = 15f;
    public float RotationSpeed = 3f;
    public float BobbingSpeed = 3f;
    public float diff = 0f;
    bool Ascending = true;
	void Start()
    {
        initial_Pos = transform.position;
	}

    void FixedUpdate()
    {
        if (gameObject.activeInHierarchy)
        {
            if (Game_Manager.State != GameState.Paused)
            {
                diff = transform.position.y - initial_Pos.y;
                if (Ascending)
                {
                    if (transform.position.y - initial_Pos.y < Max_Height)
                        transform.position += Vector3.up * BobbingSpeed * Time.deltaTime;
                    else
                        Ascending = false;
                }
                else
                {
                    if (transform.position.y - initial_Pos.y > -Max_Height)
                        transform.position -= Vector3.up * BobbingSpeed * Time.deltaTime;
                    else
                    {
                        Ascending = true;
                    }
                }
                transform.Rotate(Vector3.up, RotationSpeed * Time.deltaTime, Space.Self);
            }
        }
    }
}
