﻿using UnityEngine;
using System.Collections;

public class AutoRotate : MonoBehaviour 
{
    public float Speed = 0.5f;
    public Vector3 Axis = Vector3.up;
	
	void FixedUpdate()
    {
        transform.Rotate(Axis * Speed * Time.deltaTime);
	}
}