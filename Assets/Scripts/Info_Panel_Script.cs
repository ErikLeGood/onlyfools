﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class Info_Panel_Script : MonoBehaviour
{
    public GameObject ControlsWindow;
    Camera cam;
    public GameObject PlayerAvatar_Pointer;
    public GameObject Building_Info;
    public GameObject Actor_Info;
    
    public Text BuildingTypeText;
    public Text CapactiyText;
    public Text HP_Text;
    public Text CapsulePlantedText;

    public GameObject CapsulePanel;
    public GameObject CapsulePanel_Placement_Set;
    public GameObject CapsulePanel_Detonation_Set;
    public Button AddCapsuleButton;
    public Button RemoveCapsuleButton;
    public Button StartButton;
    public Button DetonateButton;

    public Text ActorTypeText;
    public Text ActorMoodText;
    public Text ActorActionText;
    public Text ActorInfectedText;
    public Text ActorAwareOfInfectionText;
    public Text ActorGenerationText;
    
    LayerMask mask;

    public Transform selected_Actor_Marker;
    public Transform selected_Building_Marker;

    [HideInInspector]
    public Actor_Data Actor_Selection;
    [HideInInspector]
    public Building_Behaviour Building_Selection;

    void Awake()
    {
        //DetonateButton.onClick.RemoveAllListeners();
    }

	void Start()
    {
        cam = GetComponent<Camera>();
        mask = (1 << 10 | 1 << 11 | 1 << 17);
	}

    void Update()
    {
        if (Game_Manager.State == GameState.Setup)
        {
            StartButton.gameObject.SetActive(true);
            ControlsWindow.SetActive(false);
            if (GetComponent<Avatar_Movement_Control>().C_Data.Remote_Capsule_Ammo_Remaining == 0)
            {
                StartButton.gameObject.SetActive(true);
            }
            else
                StartButton.gameObject.SetActive(false);
        }
        if (Game_Manager.State == GameState.Playing)
        {
            ControlsWindow.SetActive(true);
            StartButton.gameObject.SetActive(false);
        }

        if (Game_Manager.State == GameState.Setup)
        {
            #region Selecting / Unselecting
            if (EventSystem.current.currentSelectedGameObject == null)
            {
                Ray ray = cam.ScreenPointToRay(Input.mousePosition);
                RaycastHit rayHit;

                if (Physics.Raycast(ray, out rayHit, 1500f, mask.value))
                {
                    //if (rayHit.collider.GetComponentInParent<Building_Behaviour>())
                    //{
                    //    if (Input.GetMouseButtonDown(0))
                    //    {
                    //        Building_Selection = rayHit.collider.GetComponentInParent<Building_Behaviour>();
                    //    }
                    //}
                    //else 
                    if (rayHit.collider.GetComponentInChildren<Actor_Data>() && Game_Manager.State != GameState.Setup)
                    {
                        if (Input.GetMouseButtonDown(0))
                        {
                            Actor_Selection = rayHit.collider.GetComponentInChildren<Actor_Data>();
                            Actor_Selection.Selected = true;
                        }
                        Actor_Data AD = rayHit.collider.GetComponentInChildren<Actor_Data>();
                        ActorTypeText.text = AD.ActorType.ToString();
                        ActorActionText.text = AD.CurrentAction.ToString();
                    }
                    else if (Game_Manager.State != GameState.Setup)
                    {
                        if (Input.GetMouseButtonDown(0))
                        {
                            if (PlayerAvatar_Pointer.GetComponent<Avatar_Movement_Control>())
                            {
                                if (PlayerAvatar_Pointer.GetComponent<Avatar_Movement_Control>().Control_Mode == Avatar_Control_Mode.Move)
                                {
                                    Actor_Selection.Selected = false;
                                    Actor_Selection = null;
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        if (Actor_Selection != null)
                        {
                            Actor_Selection.Selected = false;
                            Actor_Selection = null;
                        }
                        if (Building_Selection != null)
                        {
                            //DetonateButton.onClick.RemoveListener(Building_Selection.Detonate_Remote);
                            Building_Selection = null;
                        }
                    }
                }
            }
            #endregion

            #region Actor Selection Updating
            if (Actor_Selection != null)
            {
                selected_Actor_Marker.gameObject.SetActive(true);
                selected_Actor_Marker.parent = Actor_Selection.transform;
                selected_Actor_Marker.position = Actor_Selection.transform.position;

                ActorTypeText.text = Actor_Selection.ActorType.ToString();
                ActorActionText.text = Actor_Selection.CurrentAction.ToString();
                if (Actor_Selection.ActorType == Actor_Type.Player)
                {
                    ActorMoodText.text = "???";
                    ActorGenerationText.text = "";
                }
                else if (Actor_Selection.ActorType == Actor_Type.Citizen)
                {
                    ActorGenerationText.text = "";
                    if (Actor_Selection.GetComponent<NPC_Citizen>().Infected)
                    {
                        ActorInfectedText.text = "*INFECTED*";
                        if (Actor_Selection.GetComponent<NPC_Citizen>().Aware_Of_Infection)
                        {
                            ActorAwareOfInfectionText.text = "*AWARE OF INFECTION*";
                        }
                        else
                            ActorAwareOfInfectionText.text = "";
                    }
                    else
                    {
                        ActorInfectedText.text = "";
                        ActorAwareOfInfectionText.text = "";
                    }

                    ActorMoodText.text = Actor_Selection.GetComponent<NPC_Citizen>().panic_State.ToString();
                    if (ActorMoodText.text == "_NONE")
                        ActorMoodText.text = "Calm";
                }
                else if (Actor_Selection.ActorType == Actor_Type.Zombie)
                {
                    ActorGenerationText.text = "Generation: " + Actor_Selection.GetComponent<NPC_Zombie>().Generation;
                }
            }
            else
            {
                selected_Actor_Marker.gameObject.SetActive(false);
                selected_Actor_Marker.parent = null;
                ActorTypeText.text = "";
                ActorActionText.text = "";
                ActorMoodText.text = "";
                ActorInfectedText.text = "";
                ActorAwareOfInfectionText.text = "";
                ActorGenerationText.text = "";
            }
            #endregion

            #region Building Selection Updating
            if (Building_Selection != null)
            {
                selected_Building_Marker.gameObject.SetActive(true);
                selected_Building_Marker.parent = Building_Selection.transform;
                selected_Building_Marker.position = Building_Selection.transform.position;

                //Building_Behaviour BB = rayHit.collider.GetComponentInParent<Building_Behaviour>();
                BuildingTypeText.text = "Building: " + Building_Selection.Building_Type.ToString();
                CapactiyText.text = Building_Selection.Population.ToString() + "/" + Building_Selection.Capacity.ToString();
                HP_Text.text = ((int)Building_Selection.Health).ToString();
                //CapsulePlantedText.gameObject.SetActive(Building_Selection.Capsule_Placed);

                if (Building_Selection.Capsule_Placed)
                {
                    CapsulePlantedText.text = "*CAPSULE PLANTED*";
                    AddCapsuleButton.gameObject.SetActive(false);
                    RemoveCapsuleButton.gameObject.SetActive(true);
                }
                else
                {
                    CapsulePlantedText.text = "";
                    AddCapsuleButton.gameObject.SetActive(true);
                    RemoveCapsuleButton.gameObject.SetActive(false);
                }

                CapsulePanel.transform.position = cam.WorldToScreenPoint(Building_Selection.transform.position);
                CapsulePanel.SetActive(true);
                if (Game_Manager.State == GameState.Setup)
                {
                    CapsulePanel_Detonation_Set.SetActive(false);
                    CapsulePanel_Placement_Set.SetActive(true);
                }
                else
                {
                    if (Building_Selection.Capsule_Placed)
                    {
                        CapsulePanel.SetActive(true);
                        CapsulePanel_Detonation_Set.SetActive(true);
                    }
                    else
                    {
                        CapsulePanel.SetActive(false);
                        CapsulePanel_Detonation_Set.SetActive(false);
                    }

                    CapsulePanel_Placement_Set.SetActive(false);
                }
            }
            else
            {
                selected_Building_Marker.gameObject.SetActive(false);
                CapsulePanel.SetActive(false);
                //CapsulePlantedText.gameObject.SetActive(false);
                CapsulePlantedText.text = "";
                selected_Building_Marker.parent = null;
                BuildingTypeText.text = "";
                CapactiyText.text = "";
                HP_Text.text = "";
            }
            #endregion
        }
        else
        {
            Actor_Selection = null;
            selected_Actor_Marker.parent = null;
            selected_Actor_Marker.gameObject.SetActive(false);
            Building_Selection = null;
            selected_Building_Marker.gameObject.SetActive(false);
            selected_Building_Marker.parent = null;
            CapsulePanel.SetActive(false);
            //CapsulePlantedText.gameObject.SetActive(false);
        }
    }

    public void Swap_Panels(int index)
    {
        if (index == -1)
        {
            Building_Info.SetActive(false);
            Actor_Info.SetActive(false);
        }
        if (index == 0)
        {
            Building_Info.SetActive(true);
            Actor_Info.SetActive(false);
        }
        if (index == 1)
        {
            Actor_Info.SetActive(true);
            Building_Info.SetActive(false);
        }
        //Building_Info.SetActive(!Building_Info.activeInHierarchy);
        //Actor_Info.SetActive(!Building_Info.activeInHierarchy);
    }

    public void Select_Player_Avatar()
    {
        //Actor_Selection = PlayerAvatar_Pointer.GetComponent<Actor_Data>();
        //Actor_Selection.Selected = true;
        Camera.main.GetComponentInParent<CamController2>().FocusPlayer();
    }

    public void Add_Capsule_Button()
    {
        if(Building_Selection != null)
        {
            Building_Selection.Assign_Capsule(GetComponent<Avatar_Movement_Control>());
            DetonateButton.onClick.AddListener(Building_Selection.Detonate_Remote);
        }
    }
    public void Remove_Capsule_Button()
    {
        if (Building_Selection != null)
        {
            Building_Selection.Remove_Capsule(GetComponent<Avatar_Movement_Control>());
            DetonateButton.onClick.RemoveListener(Building_Selection.Detonate_Remote);
        }
    }

    public void Placement_Over()
    {
        Game_Manager.State = GameState.Playing;
        Building_Selection = null;
        Actor_Selection = null;
        GetComponent<Avatar_Movement_Control>().C_Data.Remote_Capsule_Ammo_Remaining = GetComponent<Avatar_Movement_Control>().C_Data.Remote_Capsule_Ammo_Initial;
    }
}