﻿using UnityEngine;
using System.Collections;

public class CameraTruck : MonoBehaviour 
{
    Transform targetTransform = null;
    Vector3 TargetPos = new Vector3();
    Vector3 TargetRotEuler = new Vector3();

    public float MoveSpeed = 15f;
    public float RotationSpeed = 15f;

    public Transform DebugTarget;

	void Start()
    {
        if (DebugTarget)
            SetTarget(DebugTarget);
	}

    public void SetTarget(Transform target)
    {
        targetTransform = target;
        TargetPos = target.position;
        TargetRotEuler = target.rotation.eulerAngles;
    }
	
	void FixedUpdate()
    {
        if (targetTransform)
        {
            if (Vector3.Distance(transform.position, TargetPos) >= 0.5f)
            {
                transform.Translate((TargetPos - transform.position) * MoveSpeed * Time.deltaTime, Space.World);
            }
            if (Vector3.Distance(TargetRotEuler, transform.rotation.eulerAngles) > 0.1f)
            {
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(TargetRotEuler), RotationSpeed * Time.deltaTime);
            }
        }
	}
}
