﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class TutorialAutoCam : MonoBehaviour 
{
    static TutorialAutoCam Instance;
    public int Stage = -1;
    public Transform camera;
    public Camera_Control CamController;
    public ColorCorrectionCurves ColorCorrector;
    public Vector3 RotationTarget;
    public Transform playerAvatar;
    public Text Title;
    public Text Title2;
    float timer = 0f;

    public GameObject InGameTutorial;

    public static void Begin()
    {
        Instance.Stage = 0;
        Camera_Control.Lock_All();
        
    }

	void Start ()
    {
        Instance = this;
	}
	
	void Update ()
    {
        if (Stage == 0)
        {
            Game_Manager.Instance.Show_UI = false;
            if (camera.localPosition.y < -30f)
            {
                camera.position += Vector3.up * (1.5f * Time.deltaTime);
            }
            if (camera.rotation.eulerAngles.x > 12f)
            {
                camera.rotation = Quaternion.Euler(camera.rotation.eulerAngles.x - (7f * Time.deltaTime), camera.rotation.eulerAngles.y, camera.rotation.eulerAngles.z);
            }
            else
            {
                timer += Time.deltaTime;
                if (timer > 1f)
                {
                    Title.color += new Color(0f, 0f, 0f, (1f * Time.deltaTime));

                    if (Title.color.a >= 1f)
                    {
                        Stage++;
                    }
                }
                //if (ColorCorrector.saturation < 0.28)
                //{
                //    ColorCorrector.saturation += (0.07f * Time.deltaTime);
                //    if (ColorCorrector.saturation > 0.14)
                //    {
                //        Title.color += new Color(0f, 0f, 0f, (1f * Time.deltaTime));
                        
                //        if (Title.color.a >= 1f)
                //        {
                //            Stage++;
                //        }
                //    }
                //}
                //else
                //{
                //    Stage++;
                //}
            }
            //camera.rotation = Quaternion.RotateTowards(camera.rotation, Quaternion.Euler(RotationTarget), 5f * Time.deltaTime);
        }
        if (Stage == 1)
        {
            if (timer < 5f)
            {
                timer += Time.deltaTime;
                if (timer > 1f)
                    Title2.color += new Color(0f, 0f, 0f, (1f * Time.deltaTime));
            }
            else
            {
                Stage++;
                timer = 0f;
                Title.color = new Color(Title.color.r, Title.color.g, Title.color.b, 1f);
                Title2.color = new Color(Title2.color.r, Title2.color.g, Title2.color.b, 1f);
            }
        }
        if (Stage == 2)
        {
            if (ColorCorrector.saturation < 0.28)
            {
                ColorCorrector.saturation += (0.07f * Time.deltaTime);
            }
            
            Title.color -= new Color(0f, 0f, 0f, (1f * Time.deltaTime));
            Title2.color -= new Color(0f, 0f, 0f, (1f * Time.deltaTime));
            camera.rotation = Quaternion.RotateTowards(camera.rotation, Quaternion.LookRotation((playerAvatar.position - camera.position).normalized), 15f * Time.deltaTime);
            camera.rotation = Quaternion.Euler(camera.rotation.eulerAngles.x, camera.rotation.eulerAngles.y, 0f);
            timer += Time.deltaTime;
            if (timer > 6f)
            {
                //CamController.rotY = camera.rotation.eulerAngles.y;
                //camera.rotation = Quaternion.Euler(camera.rotation.x, 0f, camera.rotation.z);
                //Camera_Control.Locked = false;
                //Game_Manager.State = GameState.Paused;

                CamController.transform.rotation = Quaternion.Euler(CamController.transform.rotation.eulerAngles.x, CamController.transform.GetChild(0).rotation.eulerAngles.y, 0f);
                CamController.transform.GetChild(0).rotation = Quaternion.Euler(CamController.transform.GetChild(0).rotation.eulerAngles.x, CamController.transform.rotation.eulerAngles.y, CamController.transform.GetChild(0).rotation.eulerAngles.z);

                //Game_Manager.Instance.Show_UI = true;
                InGameTutorial.SetActive(true);
                Stage++;
                timer = 0f;
            }
        }
	}
}