﻿using UnityEngine;
using System.Collections;

public class Camera_Control : MonoBehaviour
{
    public static Camera_Control instance;
    public static bool MoveLocked = false;
    public static bool ZoomLocked = false;
    public static bool RotLocked = false;
    public Camera cam;
    Vector3 initialPos = new Vector3();
    float fwd_cmd = 0f;
    float right_cmd = 0f;
    float rot_cmd = 0f;
    bool MapMode = false;
    [HideInInspector]
    public float Height_Target = 0f;

    public float Pan_Speed = 25f;
    public float Rot_Speed = 25f;

    float height_Speed_Modifier = 1f;

    public float Max_Height = 175f;
    public float Min_Height = 5f;

    public float Max_Range_X = 75f;
    public float Max_Range_Z = 75f;

    public float rotX, rotY;
    public float Look_Sensitivity = 25f;

    public Ray DownRay;
    public Ray FwdRay;
    public LayerMask TrackPlaneLayer;

    float storedHeight = 0f;
    Vector3 StoredRot;

	void Start()
    {
        instance = this;
        rotX = cam.transform.rotation.eulerAngles.x;
        rotY = transform.rotation.eulerAngles.y;
        //TrackPlaneLayer = (1 << 10 | 1 << 13);
        initialPos = transform.position;
        Height_Target = transform.position.y;
	}

    Ray downRay;

    void ZoomControl()
    {
        RaycastHit data;
        downRay.origin = transform.position;
        downRay.direction = -Vector3.up;
        Physics.Raycast(downRay, out data);
        
        if (Input.GetAxis("Mouse ScrollWheel") > 0f || Input.GetKey(KeyCode.Z))
        {
            MapMode = false;
            if (data.distance > Min_Height)
            {
                //transform.position -= Vector3.up * 4f;
                Height_Target -= 4f;
            }
            else
            {
                //transform.position = data.point + ((Vector3.up * Min_Height) - (Vector3.up * 0.1f));
                Height_Target = data.point.y + (Min_Height - 0.1f);
            }
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0f || Input.GetKey(KeyCode.X))
        {
            MapMode = false;
            if (data.distance < Max_Height)
            {
                //transform.position += Vector3.up * 4f;
                Height_Target += 4f;
            }
            else
            {
                //transform.position = data.point + ((Vector3.up * Max_Height) + (Vector3.up * 0.1f));
                Height_Target = data.point.y + (Max_Height + 0.1f);
            }
        }
        //height_Speed_Modifier = (data.distance / Min_Height) / 3f;
        //if (height_Speed_Modifier < 0.1f) 
        //{ 
        //    height_Speed_Modifier = 0.1f; 
        //}
        //Debug.Log(height_Speed_Modifier);
    }
    void KeyBoardInputs()
    {
        if (Input.GetKey(KeyCode.W))
        {
            if (KeyboardHelpers.Ctrl_Modifier)
            {
                rotX += 1f;
            }
            else
                fwd_cmd += 1f;
        }
        if (Input.GetKey(KeyCode.S))
        {
            if (KeyboardHelpers.Ctrl_Modifier)
            {
                rotY += 1f;
            }
            else
                fwd_cmd -= 1f;
        }
        if (Input.GetKey(KeyCode.A))
        {
            if (KeyboardHelpers.Ctrl_Modifier)
            {
                rot_cmd -= 1f;
            }
            else
            {
                right_cmd -= 1f;
            }
        }
        if (Input.GetKey(KeyCode.D))
        {
            if (KeyboardHelpers.Ctrl_Modifier)
            {
                rot_cmd += 1f;
            }
            else
            {
                right_cmd += 1f;
            }
        }
        if (Input.GetKeyDown(KeyCode.M))
        {
            if (MapMode)
            {
                MapMode = false;
                Height_Target = storedHeight;
                transform.position = new Vector3(transform.position.x, storedHeight, transform.position.z);
                //rotY = StoredRot.y;
                rotX = StoredRot.x;
            }
            else
            {
                StoredRot.x = rotX;
                //StoredRot.y = rotY;
                storedHeight = Height_Target;
                MapView();
                MapMode = true;
            }
        }
        if (Input.GetKeyDown(KeyCode.R) && Game_Manager.State == GameState.Playing)
        {
            MapMode = false;
            FocusObject(Avatar_Movement_Control.All_Controllers[0].actorControl.transform);
        }
    }
    void MousePanning()
    {
        if (!Input.GetMouseButton(2))
        {
            float normX = (Input.mousePosition.x / (float)Screen.currentResolution.width);
            float normY = (Input.mousePosition.y / (float)Screen.currentResolution.height);
            //Debug.Log(new Vector2(normX, normY));

            if (normX <= 0.01f)
                right_cmd = -1f;
            if (normX >= 0.99f)
                right_cmd = 1f;

            if (normY <= 0.01f)
                fwd_cmd = -1f;
            if (normY >= 0.9f)
                fwd_cmd = 1f;
        }
    }
    void MouseRotation()
    {
        if (Input.GetMouseButton(2))
        {
            float time = Time.deltaTime;

            rotX -= (Input.GetAxis("Mouse Y") * Look_Sensitivity) * time;
            rotY += (Input.GetAxis("Mouse X") * Look_Sensitivity) * time;

            if (rotX < -90)
                rotX = -90;
            else if (rotX > 90)
                rotX = 90;

            if (rotY < 0)
                rotY += 360;
            if (rotY > 360)
                rotY -= 360;

            
        }
    }

    void FixedUpdate()
    {
        fwd_cmd = 0f;
        right_cmd = 0f;
        rot_cmd = 0f;

        if (Game_Manager.State != GameState.Creation && Game_Manager.State != GameState.Paused)
        {
            if (!MoveLocked)
            {
                KeyBoardInputs();
                MousePanning();
            }
            if (!ZoomLocked)
            {
                ZoomControl();
            }
            if (!RotLocked)
            {
                MouseRotation();
            }
        }

        Vector3 dir = new Vector3();
        dir.x = right_cmd;
        dir.z = fwd_cmd;
        dir.y = 0f;

        DownRay.origin = transform.position + (transform.TransformDirection(dir) * Pan_Speed * Time.deltaTime);
        DownRay.direction = -Vector3.up;

        if (!MoveLocked)
        {
            RaycastHit hit;
            if (Physics.Raycast(DownRay, out hit, 1500, TrackPlaneLayer.value))
            {
                transform.Translate(dir * Pan_Speed * height_Speed_Modifier * Time.deltaTime, Space.Self);
            }
        }

        if (!RotLocked)
        {
            rotY += rot_cmd;

            

            transform.rotation = Quaternion.Euler(0, rotY, 0);
            cam.transform.rotation = Quaternion.Euler(rotX, rotY, 0);
        }
        if (!ZoomLocked)
        {
            if (Mathf.Abs(Height_Target - transform.position.y) < Pan_Speed * Time.deltaTime)
            {
                //transform.position = new Vector3(transform.position.x, Height_Target, transform.position.z);
            }
            else if (transform.position.y < Height_Target)
            {
                transform.position += (Vector3.up * Pan_Speed * Time.deltaTime);
            }
            else if (transform.position.y > Height_Target)
            {
                transform.position -= (Vector3.up * Pan_Speed * Time.deltaTime);
            }
        }
    }

    public void MapView()
    {
        transform.position = new Vector3(transform.position.x, 200f, transform.position.z);
        Height_Target = 200f;
        rotX = 90f;
    }

    /// <summary>
    /// Is point in bounds (ignores y parameter)
    /// </summary>
    /// <param name="point"></param>
    /// <param name="bounds_Min"></param>
    /// <param name="bounds_Max"></param>
    bool PointIsInsideBounds(Vector3 point, Vector3 bounds_Min, Vector3 bounds_Max)
    {
        if (point.x < bounds_Max.x && point.x > bounds_Min.x)
            if (point.z < bounds_Max.z && point.z > bounds_Min.z)
                return true;

        return false;
    }

    public void TutorialSetPos()
    {
        transform.position = initialPos;
        transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        transform.GetChild(0).rotation = Quaternion.Euler(Quaternion.LookRotation((Game_Manager.Players[0].actorControl.transform.position - transform.GetChild(0).position).normalized, Vector3.up).eulerAngles.x, 0f, transform.GetChild(0).rotation.eulerAngles.z);
    }

    public static void Lock_All()
    {
        RotLocked = MoveLocked = ZoomLocked = true;
    }
    public static void Unlock_All()
    {
        RotLocked = MoveLocked = ZoomLocked = false;
    }
    public static void Unlock_Move()
    {
        MoveLocked = false;
    }
    public static void Unlock_Zoom()
    {
        ZoomLocked = false;
    }
    public static void Unlock_Rot()
    {
        RotLocked = false;
    }

    public void FocusObject(Transform target)
    {
        transform.position = target.position + (Vector3.up * 55f);
        rotX = 90;
        Height_Target = transform.position.y;
    }
}