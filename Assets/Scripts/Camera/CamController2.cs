﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamController2 : MonoBehaviour 
{
    public static CamController2 instance;
    public Camera cam;
    public Vector3? point;
    public Transform CamOrbitOrigin;
    public LayerMask CameraTrackLayerMask;
    public LayerMask CamCollisionMask;
    
    public float MoveSpeed = 25f;
    public float RotSpeed = 25f;

    public float MinZoom = 1f;
    public float MaxZoom = 150f;
    public bool TrackingMode = false;
    public Transform TrackingTarget;

    void Awake()
    {
        cam = GetComponentInChildren<Camera>();
        instance = this;
    }
	void Start()
    {
		
	}
    void OnDrawGizmos()
    {
        Gizmos.DrawLine(cam.transform.position, cam.transform.position + (cam.transform.forward * 5f));
        Gizmos.DrawLine(transform.position, transform.position + Vector3.up);
        Gizmos.DrawLine(transform.position, transform.position + transform.forward);
    }

    void TrackingMovement()
    {
        if (TrackingMode)
        {
            if (InformationSelection.selection != null)
            {
                if (InformationSelection.selection is Actor_Data)
                {
                    TrackingTarget = (InformationSelection.selection as Actor_Data).transform;
                }
                else if (InformationSelection.selection is Building_Behaviour)
                {
                    TrackingTarget = (InformationSelection.selection as Building_Behaviour).DoorPoint;
                }
                else if (InformationSelection.selection is Pickup_Component)
                {
                    TrackingTarget = (InformationSelection.selection as Pickup_Component).transform;
                }
            }
            else
            {
                TrackingMode = false;
                TrackingTarget = null;
            }
        }
        else
        {
            TrackingTarget = null;
        }

        if (TrackingTarget)
        {
            transform.position = new Vector3(TrackingTarget.position.x, transform.position.y, TrackingTarget.position.z);
        }
        else
        {
            TrackingMode = false;
        }
    }
    void OrbitOriginAnchoring()
    {
        Ray ray = new Ray(transform.position, Vector3.down);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 100f, CamCollisionMask.value))
        {
            if (hit.collider)
            {
                CamOrbitOrigin.transform.position = new Vector3(transform.position.x, hit.point.y, transform.position.z);
            }
        }
    }

	void Update()
    {
        TrackingMovement();
        OrbitOriginAnchoring();

        #region Keyboard Input
        float yRot = 0f;
        if ((KeyboardHelpers.Ctrl_Modifier && Input.GetKey(KeyCode.A)) || Input.GetKey(KeyCode.LeftArrow))
        {
            yRot -= 1f;
        }
        if ((KeyboardHelpers.Ctrl_Modifier && Input.GetKey(KeyCode.D)) || Input.GetKey(KeyCode.RightArrow))
        {
            yRot += 1f;
        }
        float xRot = 0f;
        if ((KeyboardHelpers.Ctrl_Modifier && Input.GetKey(KeyCode.W)) || Input.GetKey(KeyCode.UpArrow))
        {
            xRot += 1f;
        }
        if ((KeyboardHelpers.Ctrl_Modifier && Input.GetKey(KeyCode.S)) || Input.GetKey(KeyCode.DownArrow))
        {
            xRot -= 1f;
        }
        float FwdBackMove = 0f;
        if(!KeyboardHelpers.Ctrl_Modifier && Input.GetKey(KeyCode.W))
        {
            FwdBackMove += 1f;
            TrackingMode = false;
        }
        if (!KeyboardHelpers.Ctrl_Modifier && Input.GetKey(KeyCode.S))
        {
            FwdBackMove -= 1f;
            TrackingMode = false;
        }
        float LRMove = 0f;
        if (!KeyboardHelpers.Ctrl_Modifier && Input.GetKey(KeyCode.D))
        {
            LRMove += 1f;
            TrackingMode = false;
        }
        if (!KeyboardHelpers.Ctrl_Modifier && Input.GetKey(KeyCode.A))
        {
            LRMove -= 1f;
            TrackingMode = false;
        }
        float ZoomMove = 0f;
        if (Input.GetKey(KeyCode.Z))
        {
            ZoomMove += 1f;
        }
        if (Input.GetKey(KeyCode.X))
        {
            ZoomMove -= 1f;
        }

        if (Input.GetKeyDown(KeyCode.M))
        {
            MapMode();
        }

        if (Game_Manager.State == GameState.Playing)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                FocusPlayer();
            }
        }

        if (InformationSelection.selection != null)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                TrackingMode = true;
            }
        }
        #endregion

        cam.transform.LookAt(CamOrbitOrigin);
        float rotX = (xRot * RotSpeed * Time.deltaTime) + CamOrbitOrigin.rotation.eulerAngles.x;
        rotX = Mathf.Clamp(rotX, 3.5f, 89.9f);
        CamOrbitOrigin.localRotation = Quaternion.Euler(rotX, CamOrbitOrigin.localRotation.eulerAngles.y, CamOrbitOrigin.localRotation.eulerAngles.z);
        transform.Rotate(0f, yRot * RotSpeed * Time.deltaTime, 0f, Space.Self);

        transform.Translate(transform.forward * FwdBackMove * MoveSpeed * Time.deltaTime, Space.World);
        transform.Translate(transform.right * LRMove * MoveSpeed * Time.deltaTime, Space.World);

        float d = Vector3.Distance(cam.transform.position, CamOrbitOrigin.transform.position);
        if ((ZoomMove > 0f && d > MinZoom) || (ZoomMove < 0f && d < MaxZoom))
            cam.transform.Translate(cam.transform.forward * ZoomMove * MoveSpeed * Time.deltaTime, Space.World);
    }

    public void FocusPlayer()
    {
        TrackingMode = true;
        InformationSelection.selection = Avatar_Movement_Control.All_Controllers[0].actorControl;
        InformationSelection.selected = true;
        TrackingTarget = Avatar_Movement_Control.All_Controllers[0].actorControl.transform;
        
        //cam.transform.Translate(cam.transform.forward * -1f * MinZoom * 3f, Space.World);
    }

    public void MapMode()
    {
        CamOrbitOrigin.localRotation = Quaternion.Euler(89.9f, 0f, 0f);
        cam.transform.position = CamOrbitOrigin.transform.position + (cam.transform.forward * -1f * MaxZoom);
    }

    public Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 angles)
    {
        return Quaternion.Euler(angles) * (point - pivot) + pivot;
    }
}