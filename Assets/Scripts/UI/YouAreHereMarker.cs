﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YouAreHereMarker : MonoBehaviour 
{
    public Transform Player;
    public Camera camera;
    public float DistanceRequirement = 15f;
    public GameObject Text;
    
	// Use this for initialization
	void Start () 
    {
        if (!camera)
            camera = Camera.main;


	}
	

	void Update()
    {
        if (Game_Manager.Instance.Show_UI && Vector3.Distance(camera.transform.position, Player.transform.position) >= DistanceRequirement)
        {
            Text.SetActive(true);
            transform.position = camera.WorldToScreenPoint(Player.position);
        }
        else
            Text.SetActive(false);
	}
}
