﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreCardScript : MonoBehaviour
{
    public Text ScoreText;
    public Avatar_Movement_Control controller;

	void Start()
    {
		
	}
	
	void Update()
    {
        ScoreText.text = "Samples Collected: " + controller.C_Data.Points_Earned.ToString();
	}
}