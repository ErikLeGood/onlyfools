﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InformationSelection : MonoBehaviour 
{
    public GameObject Root;
    public List<Text> Lines = new List<Text>();
    public Text Textbox;
    public Camera cam;
    public LayerMask RayMask;
    public static Object selection;
    public static bool selected = false;
    public GameObject ActorMarker;
    public GameObject BuildingMarker;

	void Start()
    {
		
	}

    void Update()
    {
        bool failed = true;
        RaycastHit hit;
        if (Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit, 2000f, RayMask.value))
        {
            if (!selected)
            {
                if (hit.collider.GetComponent<Pickup_Component>())
                {
                    selection = hit.collider.GetComponent<Pickup_Component>();
                    failed = false;
                }
                else if (hit.collider.GetComponent<Actor_Data>())
                {
                    selection = hit.collider.GetComponent<Actor_Data>();
                    failed = false;
                }
                else if (hit.collider.GetComponent<Building_Behaviour>())
                {
                    selection = hit.collider.GetComponent<Building_Behaviour>();
                    failed = false;
                }
            }
            if (selection != null && Input.GetMouseButtonDown(0))
            {
                selected = true;
            }
        }
        if (failed)
        {
            if (Input.GetMouseButtonDown(0))
            {
                selected = false;
                selection = null;
            }
            if (!selected)
            {
                Root.SetActive(false);
                selection = null;
            }
        }
        else
        {
            Root.SetActive(true);
        }
        UpdateTexts();

        ActorMarker.SetActive(false);
        BuildingMarker.SetActive(false);
        if (selection != null && selected)
        {
            if (selection is Actor_Data)
            {
                if ((selection as Actor_Data).Containing_Building == null)
                {
                    ActorMarker.SetActive(true);
                    ActorMarker.transform.position = (selection as Actor_Data).transform.position;
                }
                else
                {
                    selection = (selection as Actor_Data).Containing_Building;
                }
            }
            else if (selection is Pickup_Component)
            {
                ActorMarker.SetActive(true);
                ActorMarker.transform.position = (selection as Pickup_Component).transform.position;
            }
            if (selection is Building_Behaviour)
            {
                BuildingMarker.SetActive(true);
                BuildingMarker.transform.position = (selection as Building_Behaviour).GetComponentInChildren<Collider>().bounds.center;
            }
        }
    }

    void ActorCitizenText(Actor_Data ad)
    {
        NPC_Citizen c = ad.GetComponent<NPC_Citizen>();
        Lines[0].text = ad.Forename + " " + ad.Surname;
        Lines[1].text = "Gender: " + ad.gender.ToString();
        Lines[2].text = "Type: " + ad.ActorType.ToString();
        Lines[3].text = "HP: " + c.Health.ToString("0.00");
        Lines[4].text = "Mood: " + c.panic_State.ToString();
        Lines[5].text = "Currently: " + ad.CurrentAction.ToString();

        if (c.Attack_Target != null)
            Lines[6].text = "Target: " + c.Attack_Target.ActorType.ToString();
        else if (c.Target_Building != null)
            Lines[6].text = "Target: " + c.Target_Building.Building_Type.ToString();
        else
            Lines[6].text = "Target: None";

        if (c.Infected)
            Lines[7].text = "*Infected*";
        else
            Lines[7].text = "";

        if (c.Aware_Of_Infection)
            Lines[8].text = "*Aware Of Infection!*";
        else
            Lines[8].text = "";
    }

    void ActorPoliceText(Actor_Data ad)
    {
        NPC_Police po = ad.GetComponent<NPC_Police>();
        Lines[0].text = ad.Forename + " " + ad.Surname;
        Lines[1].text = "Gender: " + ad.gender.ToString();
        Lines[2].text = "Type: " + ad.ActorType.ToString();
        Lines[3].text = "HP: " + po.Health.ToString("0.00");
        Lines[4].text = "Mood: " + po.Return_Mood_Description();
        Lines[5].text = "Currently: " + ad.CurrentAction.ToString();
        if (po.Target)
        {
            Lines[6].text = "Target: " + po.Target.ActorType.ToString();
        }
        else
        {
            Lines[6].text = "Target: None";
        }
        
        Lines[7].text = "Kill Count: " + po.KillCount;
        Lines[8].text = "";
    }
    void ActorZombieText(Actor_Data ad)
    {
        NPC_Zombie z = ad.GetComponent<NPC_Zombie>();
        Lines[0].text = ad.Forename + " " + ad.Surname;
        Lines[1].text = "Gender: " + ad.gender.ToString();
        Lines[2].text = "Type: " + ad.ActorType.ToString();
        Lines[3].text = "HP: " + z.Health.ToString("0.00");
        Lines[4].text = "Mood: Hungry";
        Lines[5].text = "Currently: " + ad.CurrentAction.ToString();

        if (z.BuildingTarget)
            Lines[6].text = "Target: " + z.BuildingTarget.Building_Type.ToString();
        else if (z.Target)
            Lines[6].text = "Target: " + z.Target.ActorType.ToString();
        else
            Lines[6].text = "Target: None";

        Lines[7].text = "Kill Count: " + z.KillCount;
        Lines[8].text = "";

    }
    void ActorPlayerText(Actor_Data ad)
    {
        //ad.optional_Player_Marker
        Lines[0].text = "The Player";
        Lines[1].text = "Gender: Unknown";
        Lines[2].text = "Type: " + ad.ActorType.ToString();
        Lines[3].text = "HP: " + ad.optional_Player_Marker.Health.ToString("0.00");
        Lines[4].text = "Mood: Evil";
        Lines[5].text = "Currently: " + ad.CurrentAction.ToString();

        Lines[7].text = "";
        Lines[8].text = "";
    }

    void UpdateTexts()
    {
        #region Pick-up Infor
        if (selection is Pickup_Component)
        {
            Pickup_Component pc = (selection as Pickup_Component);
            if (pc.Type == PickUp_Type.Equipment)
            {
                Lines[0].text = "Item: Equipment";
                Lines[1].text = Item_Catalogue.Instance.Equipment[pc.Index].PowerUp_SubType.ToString();

            }
            else if (pc.Type == PickUp_Type.Unlock)
            {
                Lines[0].text = "Item: New Unlock!";
                Lines[1].text = Item_Catalogue.Instance.Unlocks[pc.Index].Unlock_SubType.ToString();
            }
            else if (pc.Type == PickUp_Type.PowerUp)
            {
                if(Item_Catalogue.Instance.Power_Ups[pc.Index].PowerUp_SubType == PowerUp_Type.Score)
                {
                    Lines[0].text = "Tissue Sample";
                    Lines[1].text = "";
                    Lines[2].text = "";
                    Lines[3].text = "";
                    Lines[4].text = "";
                    Lines[5].text = "";
                    Lines[6].text = "";
                    Lines[7].text = "";
                    Lines[8].text = "";
                    Textbox.text = "A fresh tissue sample - right click to have your Avatar collect it. Collect enough, and earn more points for later levels!";
                }
                else
                {
                    Lines[0].text = "Item: Power-Up";
                    Lines[1].text = Item_Catalogue.Instance.Power_Ups[pc.Index].PowerUp_SubType.ToString();
                    Lines[2].text = '"' + Item_Catalogue.Instance.Power_Ups[pc.Index].Name + '"';
                }
            }

            
        }
        #endregion
        #region Actor Info
        else if (selection is Actor_Data)
        {
            Actor_Data ad = (selection as Actor_Data);
            

            if (ad.ActorType == Actor_Type.Citizen)
                ActorCitizenText(ad);
            else if (ad.ActorType == Actor_Type.Zombie)
                ActorZombieText(ad);
            else if (ad.ActorType == Actor_Type.Police)
                ActorPoliceText(ad);
            else if (ad.ActorType == Actor_Type.Player)
                ActorPlayerText(ad);

            Textbox.text = NPC_Descriptions.DescriptionForType(ad.ActorType);
        }
        #endregion
        #region Building Info
        else if (selection is Building_Behaviour)
        {
            Building_Behaviour bb = (selection as Building_Behaviour);
            Lines[0].text = bb.Building_Type.ToString();
            Lines[1].text = "Inhabitants: " + bb.Population.ToString() + " / " + bb.Capacity.ToString();
            Lines[2].text = "(" + bb.Contained_Actors.FindAll(s => s.GetComponent<NPC_Citizen>() && s.GetComponent<NPC_Citizen>().Infected).Count.ToString() + " Infected)";
            Lines[3].text = "HP: " + bb.Health.ToString("0.00");
            if (bb.Building_Type == BuildingType.Cinema)
            {
                if (bb.Locked)
                {
                    Lines[4].text = "Closed: " + bb.Lock_Time_Remaining.ToString("F1");
                }
                else if (bb.Population == 0)
                {
                    Lines[4].text = "Waiting for patrons...";
                }
                else
                {
                    Lines[4].text = "Show in progress: " + bb.Boot_Time_Remaining.ToString("F1");
                }
            }
            else
                Lines[4].text = "";

            Lines[5].text = "";
            Lines[6].text = "";
            Lines[7].text = "";
            Lines[8].text = "";
            Textbox.text = BuildingDescriptions.DescriptionForType(bb.Building_Type);
        }
        #endregion
        else
        {
            selection = null;
        }
    }
}