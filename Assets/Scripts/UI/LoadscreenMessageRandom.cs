﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class LoadscreenMessageRandom : MonoBehaviour 
{
    public List<string> Messages = new List<string>();
    public Text text;
    bool shake = false;
    Vector3 initpos;

    public void Select_Random_Message()
    {
        text.text = Messages[Random.Range(0, Messages.Count)];
        initpos = transform.position;

        if (text.text == "Loading Intensifies...")
        {
            shake = true;
        }
        else
        {
            transform.position = initpos;
            shake = false;
        }
    }
    public void SetupMessages()
    {
        Messages.Add("Welcome, Dear Player, to the Screen of Loading");
        Messages.Add("-LOADING-");
        Messages.Add("One Loadscreen Later...");
        Messages.Add("* DRUMROLL *");
        Messages.Add("Loading Intensifies...");
        Messages.Add("Enjoy Nutritious Load Screens!");
        Messages.Add("Meanwhile, in the Loadscreen...");
        Messages.Add("The Load of Load Hall...");
        Messages.Add("All you need is Loadscreens!");
        Messages.Add("I Load, therefore I Hang.");
        Messages.Add("This Level is Loading... but in a way, we are all Loading too.");
        Messages.Add("Preparing Fun...");
        Messages.Add("Breaking Everything...");
        Messages.Add("The Game Is Loading.");
        Messages.Add("Loading...");
        Messages.Add("Suddenly, Loadscreen!");
        Messages.Add("The Game Loads...");
        Messages.Add("Caution: Loadscreen in Progress.");
        Messages.Add("Maximum Loadscreen Achieved.");

        //Messages.Add('"' + "" + '"');
        Messages.Add('"' + "You had me at helload." + '"');
        Messages.Add('"' + "Do you even Load?" + '"');
        Messages.Add('"' + "Is it loaded yet?" + '"');
        Messages.Add('"' + "To Load, or not to Load. That is the question." + '"');
        Messages.Add('"' + "Load Ahoy!" + '"');
        Messages.Add('"' + "Suddenly... Loadscreen." + '"');
        Messages.Add('"' + "Run! It's the Loading Screen!" + '"');
        Messages.Add('"' + "Get a load of this!" + '"');
        Messages.Add('"' + "I thought I heard a loadscreen outside..." + '"');
        Messages.Add('"' + "Now is the Winter of our discontent made glorious Summer by this fancy loadscreen." + '"');
        Messages.Add('"' + "She looked him deep in the eye, and said 'Game Loading'. He could but weep in response." + '"');
        Messages.Add('"' + "Charon, ferryman of the Loadscreen, appeared before me." + '"');
        Messages.Add('"' + "Loadscreens should be consumed as part of a healthy, balanced diet" + '"');
        Messages.Add('"' + "This... is my Loadscreen!" + '"');
        Messages.Add('"' + "Loadscreen - protect your skin from loading!" + '"');
        
    }

	void Start()
    {
        SetupMessages();
        Select_Random_Message();
	}

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F2))
        {
            Select_Random_Message();
        }
        if (shake)
        {
            transform.position = initpos + ((Vector3.up * Random.Range(-1, 1)) + (Vector3.right * Random.Range(-1, 1)));
        }
        
    }
}