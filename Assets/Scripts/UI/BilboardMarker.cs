﻿using UnityEngine;
using System.Collections;

public class BilboardMarker : MonoBehaviour 
{
    public Camera camera;
    public Camera_Control camControl;
    public bool ReScale = true;
    public float Modifier = 1f;

	void Start()
    {
        camera = Camera.main;
        camControl = camera.GetComponentInParent<Camera_Control>();
	}
	
	void Update()
    {
        transform.LookAt(camera.transform);
        if (ReScale)
        {
            float scale = Mathf.Clamp(((camControl.Height_Target - 10) * 0.05f) / (1 / Vector3.Distance(transform.position, camera.transform.position)) * 0.01f, 0f, 7.5f);
            transform.localScale = Vector3.one * Modifier * scale;
        }
	}
}
