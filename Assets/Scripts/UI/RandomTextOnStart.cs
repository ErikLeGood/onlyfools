﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class RandomTextOnStart : MonoBehaviour 
{
    public List<string> Messages = new List<string>();
    Text text;

    public void NewRandomText()
    {
        if (!text)
            text = GetComponent<Text>();

        text.text = Messages[Random.Range(0, Messages.Count)];
    }

	void Start () 
    {
		text = GetComponent<Text>();
        NewRandomText();
	}
}