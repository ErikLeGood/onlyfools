﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public enum TutorialSectionType
{
    PreCreation, PreGameplay, Capsule, Shout, Noise, Items
}

//[System.Serializable]
//public class Tutorial_Activated_GameObject
//{
//    public GameObject ActivatingObject;
    

//}

public class Tutorial_Click_Through : MonoBehaviour 
{
    public int index = 0;
    public TutorialSectionType TutorialSection = TutorialSectionType.PreCreation;
    public GameObject EnableUponCompletion;
    public GameObject ClickThroughText;
    public Camera_Control camControl;

	void Start()
    {
        Camera_Control.Lock_All();
        Avatar_Movement_Control.PowersLocked = true;
        Avatar_Movement_Control.MoveLocked = true;
        CitizenPanelManager.Locked = true;
        Game_Manager.Instance.Show_UI = false;
	}
	
	void Update ()
    {
        if (gameObject.activeInHierarchy)
        {
            if (transform.GetChild(index).gameObject == ClickThroughText)
            {
                index++;
            }
            for (int i = 0; i < transform.childCount; i++)
            {
                if (transform.GetChild(i).gameObject == ClickThroughText)
                    transform.GetChild(i).gameObject.SetActive(true);
                else
                    transform.GetChild(i).gameObject.SetActive(i == index); //Set active if selected, otherwise inactive
            }
            if (Input.GetMouseButtonDown(0) || index > transform.childCount - 1)
            {
                if (index < transform.childCount-1)
                {
                    index++;
                    #region Special Actions
                    if (TutorialSection == TutorialSectionType.PreGameplay)
                    {
                        if (index == 3)
                        {
                            Camera_Control.Unlock_Move();
                        }
                        if (index == 5)
                        {
                            Camera_Control.Unlock_Rot();
                            camControl.rotY = camControl.cam.transform.rotation.eulerAngles.y;
                            camControl.rotX = camControl.cam.transform.rotation.eulerAngles.x;
                        }
                        if (index == 6)
                        {
                            Camera_Control.Unlock_Zoom();
                        }
                        if (index == 8)
                        {
                            Camera_Control.Lock_All();
                            camControl.TutorialSetPos();
                        }
                        if (index == 10)
                        {
                            Avatar_Movement_Control.MoveLocked = false;
                        }
                    }
                    #endregion
                }
                else
                {
                    gameObject.SetActive(false);
                    ClickThroughText.SetActive(false);
                    switch (TutorialSection)
                    {
                        case TutorialSectionType.PreCreation:
                            Game_Manager.State = GameState.Creation;
                            break;
                        case TutorialSectionType.PreGameplay:
                            Camera_Control.Unlock_All();
                            Avatar_Movement_Control.PowersLocked = false;
                            Avatar_Movement_Control.MoveLocked = false;
                            CitizenPanelManager.Locked = false;
                            Game_Manager.Instance.Show_UI = true;
                            break;
                        case TutorialSectionType.Capsule:
                            Camera_Control.Unlock_All();
                            Avatar_Movement_Control.PowersLocked = false;
                            Avatar_Movement_Control.MoveLocked = false;
                            CitizenPanelManager.Locked = false;
                            Game_Manager.Instance.Show_UI = true;
                            break;
                        case TutorialSectionType.Shout:
                            Camera_Control.Unlock_All();
                            Avatar_Movement_Control.PowersLocked = false;
                            Avatar_Movement_Control.MoveLocked = false;
                            CitizenPanelManager.Locked = false;
                            Game_Manager.Instance.Show_UI = true;
                            break;
                        case TutorialSectionType.Noise:
                            Camera_Control.Unlock_All();
                            Avatar_Movement_Control.PowersLocked = false;
                            Avatar_Movement_Control.MoveLocked = false;
                            CitizenPanelManager.Locked = false;
                            Game_Manager.Instance.Show_UI = true;
                            break;
                        case TutorialSectionType.Items:
                            Camera_Control.Unlock_All();
                            Avatar_Movement_Control.PowersLocked = false;
                            Avatar_Movement_Control.MoveLocked = false;
                            CitizenPanelManager.Locked = false;
                            Game_Manager.Instance.Show_UI = true;
                            break;
                    }

                    if (EnableUponCompletion)
                    {
                        EnableUponCompletion.SetActive(true);
                    }
                }
            }
        }
	}
}