﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class BuildingPopupManager : MonoBehaviour
{
    
    public Avatar_Movement_Control Owner;

    public GameObject Selection_Marker;

    public Transform WindowRoot;
    public GameObject RemoteWindow;
    public GameObject ConfirmRemoteWindow;
    
    public Camera cam;

    public Text TypeText;
    public Text PopulationText;
    public Text HealthText;
    public Text ExtraText;
    public Text DescriptionText;
    public Button CloseButton;
    public LayerMask mask;

    public Text Remote_Placed_Text;
    public GameObject AddRemoteButton;
    public GameObject RemoveRemoteButton;
    public GameObject DetonateRemoteButton;

    public Building_Behaviour Selection = null;
    public Vector3 position = new Vector3();

    int Remotes_Placed = 0;

	void Start()
    {
        if (!cam)
            cam = GetComponent<Camera>();

        mask = (1 << 10);
        Remotes_Placed = 0;
	}

    void UpdateTexts(Building_Behaviour building)
    {
        #region Title / Desc
        ExtraText.text = string.Empty;
        switch (building.Building_Type)
        {
            case BuildingType.Bank:
                TypeText.text = "Bank";
                DescriptionText.text = BuildingDescriptions.BankDesc;
                break;
            case BuildingType.Butcher:
                TypeText.text = "Butcher's Shop";
                DescriptionText.text = BuildingDescriptions.ButcherDesc;
                break;
            case BuildingType.Cinema:
                TypeText.text = "Cinema";
                DescriptionText.text = BuildingDescriptions.CinemaDesc;
                if (building.Locked)
                {
                    ExtraText.text = "Closed: " + building.Lock_Time_Remaining.ToString("F1");
                }
                else
                {
                    ExtraText.text = "Show In Progress: " + building.Boot_Time_Remaining.ToString("F1");
                }
                break;
            case BuildingType.Clinic:
                TypeText.text = "Clinic";
                DescriptionText.text = BuildingDescriptions.HospitalDesc;
                break;
            case BuildingType.House:
                TypeText.text = "House";
                DescriptionText.text = BuildingDescriptions.HouseDesc;
                break;
            case BuildingType.PoliceStation:
                TypeText.text = "Police Station";
                DescriptionText.text = BuildingDescriptions.PoliceStationDesc;
                break;
            case BuildingType.Powerplant:
                TypeText.text = "Power Substation";
                DescriptionText.text = BuildingDescriptions.PowerStationDesc;
                break;
            case BuildingType.Shop:
                TypeText.text = "Shop";
                DescriptionText.text = BuildingDescriptions.ShopDesc;
                break;
        }
        #endregion

        HealthText.text = "Integrity: " + building.Health.ToString();
        PopulationText.text = "Population: " + building.Population.ToString() + "(" + building.Contained_Actors.FindAll(s => s.GetComponent<NPC_Citizen>() && s.GetComponent<NPC_Citizen>().Infected).Count.ToString() + " Infected) / " + building.Capacity.ToString();
    }

    public void Add_Remote()
    {
        if (Selection)
        {
            if (Remotes_Placed < Owner.C_Data.Remote_Capsule_Ammo_Initial)
            {
                if (!Selection.Capsule_Placed)
                {
                    Selection.Assign_Capsule(Owner);
                    Remotes_Placed += 1;
                }
            }
        }
    }
    public void Remove_Remote()
    {
        if (Selection)
        {
            if (Remotes_Placed > 0)
            {
                if (Selection.Capsule_Placed)
                {
                    Selection.Remove_Capsule(Owner);
                    Remotes_Placed -= 1;
                }
            }
        }
    }

    public void CloseButtonClick()
    {
        Selection = null;
    }

    public void DetonateButtonClick()
    {
        Selection.Detonate_Remote();
        Selection = null;
    }

    public void Confirm_Capsule_Placements()
    {
        ConfirmRemoteWindow.SetActive(false);
        Game_Manager.State = GameState.Playing;
        Selection = null;
        Owner.Configure_Ammo_Counts();
        Game_Manager.Begin_Game(false);
    }
    	
	void Update()
    {
        if (Game_Manager.State == GameState.Playing || Game_Manager.State == GameState.Setup)
        {
            if (!Avatar_Movement_Control.PowersLocked)
            {
                #region No Selection
                bool failed = true;
                RaycastHit hit;
                if (Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit, 2000f, mask.value))
                {
                    Building_Behaviour bb = hit.collider.GetComponent<Building_Behaviour>();
                    if (bb)
                    {
                        failed = false;
                        if (Selection == null)
                        {
                            WindowRoot.gameObject.SetActive(true);
                            CloseButton.gameObject.SetActive(false);
                            WindowRoot.position = Input.mousePosition;
                            UpdateTexts(bb);
                            if (bb.Capsule_Placed)
                            {
                                RemoteWindow.SetActive(true);
                            }
                            else
                            {
                                RemoteWindow.SetActive(false);
                            }
                        }

                        if (Input.GetMouseButtonDown(0))
                        {
                            if (Avatar_Movement_Control.All_Controllers[0].Control_Mode != Avatar_Control_Mode.Ranged_Taunt && Avatar_Movement_Control.All_Controllers[0].Control_Mode != Avatar_Control_Mode.Launch)
                            {
                                if (EventSystem.current.currentSelectedGameObject == null)
                                {
                                    Selection = bb;
                                    position = hit.point;
                                }
                            }
                        }
                    }
                }
                if (failed)
                {
                    WindowRoot.gameObject.SetActive(false);
                }

                #endregion
                #region Has a Selection
                if (Selection != null)
                {
                    WindowRoot.gameObject.SetActive(true);
                    CloseButton.gameObject.SetActive(true);
                    WindowRoot.position = cam.WorldToScreenPoint(position);
                    UpdateTexts(Selection);
                    if (Game_Manager.State == GameState.Setup || Selection.Capsule_Placed)
                    {
                        RemoteWindow.SetActive(true);
                        Remote_Placed_Text.text = Selection.Capsule_Placed ? "REMOTE PLACED" : "";

                        if (Game_Manager.State == GameState.Setup)
                        {
                            AddRemoteButton.SetActive(!Selection.Capsule_Placed);
                            RemoveRemoteButton.SetActive(Selection.Capsule_Placed);
                            DetonateRemoteButton.SetActive(false);
                        }
                        else
                        {
                            AddRemoteButton.SetActive(false);
                            RemoveRemoteButton.SetActive(false);
                            DetonateRemoteButton.SetActive(true);
                        }
                    }
                    else
                    {
                        RemoteWindow.SetActive(false);
                    }
                }
                #endregion
                #region Regardless of Selection Status
                if (Game_Manager.State == GameState.Setup && Remotes_Placed >= Owner.C_Data.Remote_Capsule_Ammo_Initial && Owner.C_Data.Remote_Capsule_Ammo_Initial > 0)
                {
                    ConfirmRemoteWindow.SetActive(true);
                }
                else
                {
                    ConfirmRemoteWindow.SetActive(false);
                }
                #endregion
            }
        }
        else
        {
            WindowRoot.gameObject.SetActive(false);
            RemoteWindow.SetActive(false);
            ConfirmRemoteWindow.SetActive(false);
        }
	}
}