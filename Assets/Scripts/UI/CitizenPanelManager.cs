﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CitizenPanelManager : MonoBehaviour
{
    public static bool Locked = false;
    public Camera cam;
    LayerMask TargetLayer;

    public GameObject SelectionMarker_World;
    public GameObject IsInfectedParticle;
    public GameObject SelectionMarker_UI;

    public GameObject Panel;
    public Text NameText;
    public Text PersonalityText;
    public Text TypeText;
    public Text GenderText;
    public Text HealthText;
    public Text CurrentPanicStateText;
    public Text CurrentActionText;
    public Text BuildingTargetTypeText;
    public Text InfectedText;
    public Text AwareOfInfectionText;
    public Text DescriptionText;

    public Actor_Data Selection;

	void Start()
    {
	    if (!cam)
        {
            cam = GetComponent<Camera>();
        }
        TargetLayer = (1 << 11 | 1 << 14 | 1 << 17);
	}

    void UpdateTexts(Actor_Data Selection)
    {
        NameText.text = Selection.Forename + " " + Selection.Surname;
        GenderText.text = Selection.gender.ToString();
        TypeText.text = Selection.ActorType.ToString();
        CurrentActionText.text = "Action: " + Selection.CurrentAction.ToString();

        #region Type-Specific Texts
        switch (Selection.ActorType)
        {
            case Actor_Type.Citizen:
                UpdateTexts(Selection.GetComponent<NPC_Citizen>());
            break;
            case Actor_Type.Police:
                UpdateTexts(Selection.GetComponent<NPC_Police>());
            break;
            case Actor_Type.Zombie:
                UpdateTexts(Selection.GetComponent<NPC_Zombie>());
            break;
            case Actor_Type.Player:
                UpdateTexts(Selection.optional_Player_Marker);
            break;
        }
        #endregion
    }

    void UpdateTexts(NPC_Citizen Citizen)
    {
        HealthText.text = "Health: " + Citizen.Health.ToString("F2");
        CurrentPanicStateText.text = "Mood: " + Citizen.panic_State.ToString();
        DescriptionText.text = NPC_Descriptions.Citizen_Desc;
        if (Citizen.Target_Building)
        {
            BuildingTargetTypeText.text = "Target: " + Citizen.Target_Building.Building_Type;
        }
        else
        {
            BuildingTargetTypeText.text = "Target: None";
        }
        if (Citizen.Infected)
        {
            InfectedText.text = "**INFECTED**";
        }
        else
        {
            InfectedText.text = "";
        }
        if (Citizen.Aware_Of_Infection)
        {
            AwareOfInfectionText.text = "*AWARE OF INFECTION*";
        }
        else
        {
            AwareOfInfectionText.text = "";
        }
    }
    void UpdateTexts(NPC_Zombie Zombie)
    {
        HealthText.text = "Health: " + Zombie.Health.ToString("F2");
        CurrentPanicStateText.text = "Mood: ...Brains...";
        InfectedText.text = "";
        AwareOfInfectionText.text = "";
        DescriptionText.text = NPC_Descriptions.Zombie_Desc;

        if (Zombie.Target)
        {
            if (Zombie.Target.Containing_Building != null)
            {
                BuildingTargetTypeText.text = "Target: " + Zombie.Target.Containing_Building.Building_Type.ToString();
            }
            else
            {
                BuildingTargetTypeText.text = "Target: " + Zombie.Target.ActorType.ToString();
            }
        }
        else
        {
            if (Zombie.BuildingTarget != null)
            {
                BuildingTargetTypeText.text = Zombie.BuildingTarget.Building_Type.ToString();
            }
            else
            {
                BuildingTargetTypeText.text = "Target: None";
            }
        }
    }
    void UpdateTexts(NPC_Police Police)
    {
        HealthText.text = "Health: " + Police.Health.ToString("F2");
        CurrentPanicStateText.text = Police.Return_Mood_Description();
        InfectedText.text = "";
        AwareOfInfectionText.text = "";
        DescriptionText.text = NPC_Descriptions.Police_Desc;

        if (Police.Target)
        {
            BuildingTargetTypeText.text = "Target: " + Police.Target.ActorType.ToString();
        }
        else
        {
            BuildingTargetTypeText.text = "Target: None"; // + Police.Target.ActorType.ToString();
        }
    }
    void UpdateTexts(Avatar_Movement_Control Player)
    {
        NameText.text = "J. Doe";
        HealthText.text = "Health: " + Player.Health.ToString("F2");
        CurrentPanicStateText.text = "Mood: ???";
        InfectedText.text = "";
        AwareOfInfectionText.text = "";
        DescriptionText.text = NPC_Descriptions.Player_Desc;
        BuildingTargetTypeText.text = "";
    }
	
	void Update()
    {
        if (!Locked && Game_Manager.State != GameState.Paused)
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (EventSystem.current.currentSelectedGameObject == null)
                {
                    RaycastHit hit;
                    if (Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit, 2000f, TargetLayer.value))
                    {
                        Actor_Data AData = hit.collider.GetComponent<Actor_Data>();
                        if (AData)
                        {
                            Selection = AData;
                        }
                    }
                    else
                    {
                        Selection = null;
                    }
                }
            }
        }
        if (Selection != null)
        {
            Panel.SetActive(true);
            UpdateTexts(Selection);
            #region World Marker Updating
            if (Selection.Containing_Building == null)
            {
                SelectionMarker_World.SetActive(true);
                SelectionMarker_World.transform.position = Selection.transform.position;
            }
            else
            {
                SelectionMarker_World.SetActive(false);
            }
            if (Selection.ActorType == Actor_Type.Citizen)
            {
                if (Selection.GetComponent<NPC_Citizen>().Infected)
                {
                    IsInfectedParticle.SetActive(true);
                }
                else
                {
                    IsInfectedParticle.SetActive(false);
                }
            }
            else
            {
                IsInfectedParticle.SetActive(false);
            }
            #endregion
        }
        else
        {
            Panel.SetActive(false);
            SelectionMarker_World.SetActive(false);
        }
	}
}

public class NPC_Descriptions
{
    public static string DescriptionForType(Actor_Type Type)
    {
        switch (Type)
        {
            case Actor_Type.Citizen:
                return Citizen_Desc;
            case Actor_Type.Player:
                return Player_Desc;
            case Actor_Type.Police:
                return Police_Desc;
        }
        return "No Description Found";
    }
    public static string Citizen_Desc = "Citizen - A hapless, wandering, peasant. They retreat from danger (most of the time), and sometimes hide indoors.";
    public static string Police_Desc = "Police - Hand of justice, protector of the people, slayer of zombies and bio-terrorists. Carry guns.";
    public static string Zombie_Desc = "Zombie - A zombie heeds only its appetite. It eats any non-zombie flesh that crosses its path, and will hunt in packs.";
    public static string Player_Desc = "Player - Is it a baleful emnity for life, or an amoral obligation to the progress of science that drives this sick soul?";
}