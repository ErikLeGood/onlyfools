﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoZombieController : MonoBehaviour 
{
    Animator animator;
    public Renderer FaceRenderer;
    public Renderer ClothesRenderer;
    Color defaultColor;

	void Start ()
    {
        animator = GetComponentInChildren<Animator>();
        ClothesRenderer.materials[1].SetTexture("_MainTex", Face_Catalogue.GetRandomZombieOutfit());
        FaceRenderer.material.SetTexture("_MainTex", Face_Catalogue.Get_Random_Zombie_Face().Calm);
        defaultColor = ClothesRenderer.materials[0].GetColor("_Color");
	}
	
	void Update ()
    {
        animator.speed = Avatar_Movement_Control.All_Controllers[0].C_Data.Speed * 0.2f;
        ClothesRenderer.materials[0].SetColor("_Color", Color.Lerp(defaultColor * 0.2f, defaultColor, 1f / Avatar_Movement_Control.All_Controllers[0].C_Data.Toughness));
        ClothesRenderer.materials[0].SetColor("_EmissionColor", Color.Lerp(defaultColor * 0.2f, defaultColor, 1f / Avatar_Movement_Control.All_Controllers[0].C_Data.Toughness));
	}
}