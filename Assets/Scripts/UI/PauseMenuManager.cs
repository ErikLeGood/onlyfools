﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PauseMenuManager : MonoBehaviour 
{
    public GameObject ConfirmWindow;
    public static bool Confirm_Mode = false;

	void Start()
    {
	    
	}
	
	void Update()
    {
        if (!gameObject.activeInHierarchy)
        {
            Confirm_Mode = false;
        }
        ConfirmWindow.SetActive(Confirm_Mode);
	}

    public void ResumeButtonClick()
    {
        Game_Manager.UnPauseGame();
    }

    public void AbandonButtonClick()
    {
        Confirm_Mode = true;
    }

    public void CloseConfirmWindow()
    {
        Confirm_Mode = false;
    }
    public void ConfirmAbandon()
    {
        Game_Manager.Players[0].Abandon();
        Game_Manager.UnPauseGame();
    }
}