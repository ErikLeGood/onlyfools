﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DesignTutorial : MonoBehaviour 
{
    public int Index = 0;
    public List<string> Texts = new List<string>();
    public Text text;
    public GameObject BackButton;
    public GameObject NextButton;

	void Start()
    {
		
	}
	
	void Update()
    {
		if (text)
        {
            text.text = Texts[Index];
        }
        BackButton.SetActive(Index > 0);
        NextButton.SetActive(Index < Texts.Count - 1);
	}

    public void Next()
    {
        if (Index < Texts.Count - 1)
            Index++;
    }
    public void Previous()
    {
        if (Index > 0)
            Index--;
    }
    public void Hide()
    {
        gameObject.SetActive(false);
    }
}