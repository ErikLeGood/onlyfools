﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DemoMenuManager : MonoBehaviour
{
    public GameObject MainRoot;
    public GameObject CreditsRoot;
    public GameObject TutorialSubMenu;
    public GameObject GetInvolvedRoot;
    public GameObject ConfirmQuitRoot;

    public RandomTextOnStart RandomQuitMessageGenerator;
    
	void Start ()
    {
        MainRoot.SetActive(true);
        CreditsRoot.SetActive(false);
        TutorialSubMenu.SetActive(false);
        ConfirmQuitRoot.SetActive(false);

	}
	void Update ()
    {
		
	}
    public void TutorialButton()
    {
        TutorialSubMenu.SetActive(true);
    }
    public void DemoLevelButton()
    {
        LoadScreenManager.LEVEL_TARGET = "Level_1";
        SceneManager.LoadScene("LoadingScene", LoadSceneMode.Single);
    }
    public void CreditsButton()
    {
        TutorialSubMenu.SetActive(false);
        MainRoot.SetActive(false);
        CreditsRoot.SetActive(true);
    }
    public void ContributeButton()
    {
        TutorialSubMenu.SetActive(false);
    }
    public void CloseCreditsButton()
    {
        CreditsRoot.SetActive(false);
        MainRoot.SetActive(true);
    }
    public void ExitButton()
    {
        MainRoot.SetActive(false);
        TutorialSubMenu.SetActive(false);
        RandomQuitMessageGenerator.NewRandomText();
        ConfirmQuitRoot.SetActive(true);
    }
    public void ConfirmQuitYes()
    {
        Application.Quit();
    }
    public void ConfirmQuitNo()
    {
        ConfirmQuitRoot.SetActive(false);
        MainRoot.SetActive(true);
    }
}