﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;

public class TitleFadeIn : MonoBehaviour 
{
    public Text Title1;
    public Text Title2;

    public ColorCorrectionCurves ccc;
    float baseSatch;

    float timer = 0f;
    public float Duration = 5f;
    int stage = 0;

	// Use this for initialization
	void Start () 
    {
        baseSatch = ccc.saturation;
        ccc.saturation = 0f;
	}
	
	// Update is called once per frame
	void Update () 
    {
		if (stage == 0)
        {
            Title1.gameObject.SetActive(true);
            Title2.gameObject.SetActive(false);
            timer += Time.deltaTime;
            if (timer >= Duration * 0.5f)
            {
                timer = 0;
                Title2.gameObject.SetActive(true);
                stage++;
            }
        }
        else if (stage == 1)
        {
            if (ccc.saturation < baseSatch)
            {
                ccc.saturation += Time.deltaTime * 0.1f;
            }
            else
            {
                Title1.gameObject.SetActive(false);
                Title2.gameObject.SetActive(false);
            }
        }
	}
}
