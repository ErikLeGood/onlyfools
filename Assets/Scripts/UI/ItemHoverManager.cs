﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ItemHoverManager : MonoBehaviour 
{
    public GameObject ItemToolTip;
    public Text Title;
    public Text Description;
    public LayerMask mask;
    public Camera cam;

	void Start () 
    {
	
	}
	
	void Update()
    {
        bool Visible = false;
        RaycastHit hit;
        if (Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out hit, 2000f, mask.value))
        {
            Pickup_Component pickup = hit.collider.GetComponent<Pickup_Component>();
            if (pickup)
            {
                ItemToolTip.transform.position = Input.mousePosition;
                Visible = true;
                switch (pickup.Type)
                {
                    case PickUp_Type.Equipment:
                        Title.text = Item_Catalogue.Instance.Equipment[pickup.Index].Name;
                        Description.text = Item_Catalogue.Instance.Equipment[pickup.Index].Description;
                        break;
                    case PickUp_Type.PowerUp:
                        Title.text = Item_Catalogue.Instance.Power_Ups[pickup.Index].Name;
                        Description.text = Item_Catalogue.Instance.Power_Ups[pickup.Index].Description;
                        break;
                    case PickUp_Type.Unlock:
                        Title.text = Item_Catalogue.Instance.Unlocks[pickup.Index].Name;
                        Description.text = Item_Catalogue.Instance.Unlocks[pickup.Index].Description;
                        break;
                }
            }
        }

        ItemToolTip.SetActive(Visible);
	}
}