﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Health_Panel_Script : MonoBehaviour 
{
    public Avatar_Movement_Control player;
    public Text display;
    public bool Hurt = false;
    float timer = 0f;
    public float AnimationSpeed = 2f;


    public void GetHit()
    {
        timer = 0f;
        display.transform.localScale = Vector3.one * 1.25f;
        display.color = Color.red;
        Hurt = true;
    }

	void Start()
    {
        Avatar_Movement_Control.GotHurt_Event += GetHit;
	}
	
	void Update()
    {
        display.text = "Health: " + player.Health + "/" + player.Health_MAX;
        if (Hurt)
        {
            display.color = Color.Lerp(Color.red, Color.black, Time.deltaTime * AnimationSpeed);
            display.transform.localScale = Vector3.Lerp(Vector3.one * 1.25f, Vector3.one, Time.deltaTime * AnimationSpeed);
            if (display.color == Color.black && display.transform.localScale == Vector3.one)
            {
                Hurt = false;
            }
        }
	}
}