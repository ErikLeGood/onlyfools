﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CitizenThoughtTextManager : MonoBehaviour
{
    public Text text;
    public GameObject Root;
    float timer = 0f;
    public float MaxDuration = 4f;

    public void ShowText(NPC_Panic_State state)
    {
        text.text = CitizenSpeechPalette.GetRandomTextFor(state);
        timer = 0f;
        Root.SetActive(true);
    }

	void Start ()
    {
		
	}
	
	void Update ()
    {
		if (Root.activeInHierarchy)
        {
            timer += Time.deltaTime;
            if (timer > MaxDuration)
            {
                Root.SetActive(false);
            }
        }
	}
}