﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CitizenSpeechPalette
{
    public static Dictionary<NPC_Panic_State, List<string>> TextsByPanicState;

    //public static List<Font> PossibleFonts = new List<Font>();

    static CitizenSpeechPalette()
    {
        Configure();
    }

    public static string GetRandomTextFor(NPC_Panic_State state)
    {
        int max = TextsByPanicState[state].Count;
        return TextsByPanicState[state][Random.Range(0, max)];
    }

    static void Configure()
    {
        TextsByPanicState = new Dictionary<NPC_Panic_State, List<string>>();
        TextsByPanicState.Add(NPC_Panic_State.Calm, new List<string>());
        TextsByPanicState.Add(NPC_Panic_State.Angry, new List<string>());
        TextsByPanicState.Add(NPC_Panic_State.Fleeing, new List<string>());
        TextsByPanicState.Add(NPC_Panic_State.Hiding, new List<string>());
        TextsByPanicState.Add(NPC_Panic_State.Hysterical, new List<string>());

        #region Configure Calm
        TextsByPanicState[NPC_Panic_State.Calm].Add("(∪ ◡ ∪)");
        TextsByPanicState[NPC_Panic_State.Calm].Add("(✿◠‿◠)");
        TextsByPanicState[NPC_Panic_State.Calm].Add("^‿^");
        TextsByPanicState[NPC_Panic_State.Calm].Add("(︶▽︶)");
        TextsByPanicState[NPC_Panic_State.Calm].Add("( ͡° ͜ʖ ͡°)");
        #endregion
        #region Configure Angry
        TextsByPanicState[NPC_Panic_State.Angry].Add("(ง ͠° ͟ل͜ ͡°)ง");
        #endregion
        #region Configure Fleeing
        TextsByPanicState[NPC_Panic_State.Fleeing].Add("╚(•⌂•)╝");
        #endregion
        #region Configure Hiding
        TextsByPanicState[NPC_Panic_State.Hiding].Add("( ⚆ _ ⚆ )");
        #endregion
        #region Configure Hysterical
        TextsByPanicState[NPC_Panic_State.Hysterical].Add("(¤﹏¤)");
        #endregion
    }
}