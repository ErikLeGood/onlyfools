﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenURLOnClick : MonoBehaviour 
{
    public string Site_URL = "www.google.com";
	public void OnClick()
    {
        Application.OpenURL(Site_URL);
    }
}
