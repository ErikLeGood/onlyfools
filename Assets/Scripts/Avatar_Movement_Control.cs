﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public enum Avatar_Control_Mode
{
    _NONE, Move, Launch, Ranged_Taunt, Taunt
}
public delegate void EventHandler_Taunt(Actor_Data source);
public delegate void EventHandler_GotHurt();

public class NavMeshAgent_Helpers
{
    public static void EqualizeLayerCosts(UnityEngine.AI.NavMeshAgent agent)
    {
        agent.SetAreaCost(0, 1);
        agent.SetAreaCost(4, 1);
        agent.SetAreaCost(5, 1);
    }
    public static void RevertLayerCosts(UnityEngine.AI.NavMeshAgent agent)
    {
        agent.SetAreaCost(0, 5);
        agent.SetAreaCost(4, 25);
        agent.SetAreaCost(5, 100);
    }
}
public class RigidBody_Helpers
{
    public static Vector3 CalculateBestThrowSpeed(Vector3 origin, Vector3 target, float timeToTarget)
    {
        // calculate vectors
        Vector3 toTarget = target - origin;
        Vector3 toTargetXZ = toTarget;
        toTargetXZ.y = 0;

        // calculate xz and y
        float y = toTarget.y;
        float xz = toTargetXZ.magnitude;

        // calculate starting speeds for xz and y. Physics forumulase deltaX = v0 * t + 1/2 * a * t * t
        // where a is "-gravity" but only on the y plane, and a is 0 in xz plane.
        // so xz = v0xz * t => v0xz = xz / t
        // and y = v0y * t - 1/2 * gravity * t * t => v0y * t = y + 1/2 * gravity * t * t => v0y = y / t + 1/2 * gravity * t
        float t = timeToTarget;
        float v0y = y / t + 0.5f * Physics.gravity.magnitude * t;
        float v0xz = xz / t;

        // create result vector for calculated starting speeds
        Vector3 result = toTargetXZ.normalized;        // get direction of xz but with magnitude 1
        result *= v0xz;                                // set magnitude of xz to v0xz (starting speed in xz plane)
        result.y = v0y;                                // set y to v0y (starting speed of y plane)

        return result;
    }
}

public class Avatar_Movement_Control : MonoBehaviour
{
    public string OuterMenuName = "MainMenu";
    public static event EventHandler_Taunt Taunt_Event;
    public static event EventHandler_GotHurt GotHurt_Event;

    public bool Invulnerable = false;
    public bool Invisible = false;
    public float Speed = 8f;
    public float Speed_Modifier = 0f;
    public float Health = 10;
    public float Health_MAX = 10;
    public float Health_BASE_MAX = 10;
    public LayerMask MoveRayLayerMask;
    Camera cam;
    public Actor_Data actorControl;
    public Info_Panel_Script info_Script;
    public Avatar_Control_Mode Control_Mode = Avatar_Control_Mode.Move;
    Avatar_Control_Mode stored_Mode = Avatar_Control_Mode._NONE;
    public Contagion_Data C_Data;
    public Inventory inventory_Pointer;
    static Contagion_Data C_Data_HOLDOVER = null;
    public static List<Avatar_Movement_Control> All_Controllers = new List<Avatar_Movement_Control>();

    public Text ThrowInstructions;
    public Text OutOfRangeText;

    public float Launch_Range = 15f;
    public GameObject Capsule_Prefab;
    public GameObject NoiseParticle_Prefab;
    public Transform Launch_Target_Marker;
    public Transform Taunt_Target_Marker;
    public Transform Launch_Spawn_Marker;
    LayerMask mask;

    public Text Ammo_Counter_Text;

    float rangeTauntCooldownTimer = 0f;
    bool CanUseTaunt = true;
    float RangeTauntCooldown = 5f;
    public Transform CooldownRect;
    Vector3 Cooldown_Rect_InitPos;

    Vector3? Ranged_Dest = null;

    public Restart_Menu_Manager restartManager;
    float fail_timer = 0f;
    float fail_allowance = 10f;

    public ParticleSystem Part_System_Noise;

    public void Configure_Ammo_Counts()
    {
        C_Data.Capsule_Ammo_Remaining = C_Data.Capsule_Ammo_Initial;
        C_Data.Remote_Capsule_Ammo_Remaining = C_Data.Remote_Capsule_Ammo_Initial;
    }

    bool Abandoned = false;
    public void Abandon()
    {
        Abandoned = true;
    }

    void Victory_And_Failure_Check()
    {
        #region Failure Checks

        #region Abandoned Condition
        if (Abandoned)
        {
            restartManager.Show(Game_End_Reason.Mission_Abandoned);
            return;
        }
        #endregion

        //Out of Ammo check comes first, as this is the primary requisite for failure in the tree.
        if (C_Data.Capsule_Ammo_Remaining == 0 && C_Data.Remote_Capsule_Ammo_Remaining <= 0)
        {
            #region All Shots Missed Condition
            if (Game_Manager.Citizens_Infected == 0 && Game_Manager.Citizen_Ever_Infected == 0 && Game_Manager.Zombies == 0)
            {
                fail_timer += Time.deltaTime;
                if (fail_timer >= fail_allowance)
                {
                    fail_timer = 0f;
                    restartManager.Show(Game_End_Reason.All_Shots_Missed);
                    return;
                }
            }
            #endregion
            #region All Zombies Died Condition
            else if (Game_Manager.Citizens_Infected == 0 && Game_Manager.Zombies == 0 && Game_Manager.Zombies_Ever > 0) 
            {
                fail_timer += Time.deltaTime;
                if (fail_timer >= fail_allowance)
                {
                    fail_timer = 0f;
                    restartManager.Show(Game_End_Reason.Zombies_Died);
                    return;
                }
            }
            #endregion
            #region Contagion Got Cured Condition
            else if (Game_Manager.Citizens_Infected == 0 && Game_Manager.Citizen_Ever_Infected > 0 && Game_Manager.Zombies == 0 && Game_Manager.Zombies_Ever == 0)
            {
                fail_timer += Time.deltaTime;
                if (fail_timer >= fail_allowance)
                {
                    fail_timer = 0f;
                    restartManager.Show(Game_End_Reason.Got_Cured);
                    return;
                }
            }
            #endregion
        }

        if (Health <= 0)
        {
            Health = 0;
            restartManager.Show(Game_End_Reason.Player_Died);
            return;
        }
        #endregion

        if (Game_Manager.All_Police.Count == 0 && Game_Manager.Citizens_Healthy == 0 && Game_Manager.Citizens_Infected == 0)
        {
            Main_Menu_Manager.RegisterWin(this);
            restartManager.Show(Game_End_Reason.Success);
            return;
        }
    }

    public Tutorial_Click_Through Capsule_Tutorial;
    public Tutorial_Click_Through Shout_Tutorial;
    public Tutorial_Click_Through Noise_Tutorial;

    bool CapsuleTutDone, ShoutTutDone, NoiseTutDone;
    public static bool MoveLocked = false;
    public static bool PowersLocked = false;

    public void Launch_Mode_Button()
    {
        if (Game_Manager.Instance.Tutorial_Mode)
        {
            if (Capsule_Tutorial)
            {
                if (!CapsuleTutDone && !PowersLocked)
                {
                    CapsuleTutDone = true;
                    Capsule_Tutorial.gameObject.SetActive(true);
                    Capsule_Tutorial.index = 0;
                    Camera_Control.MoveLocked = true;
                    Avatar_Movement_Control.PowersLocked = true;
                    Avatar_Movement_Control.MoveLocked = true;
                    CitizenPanelManager.Locked = true;
                    return;
                }
            }
        }
        Control_Mode = Avatar_Control_Mode.Launch;
        //actorControl.CurrentAction = Actor_Action.Idle;
        //actorControl.Stop();
    }

    public void Move_Mode_Button()
    {
        Control_Mode = Avatar_Control_Mode.Move;
    }

    public void Make_Noise_Button()
    {
        if (Game_Manager.Instance.Tutorial_Mode)
        {
            if (Shout_Tutorial)
            {
                if (!ShoutTutDone && !PowersLocked)
                {
                    ShoutTutDone = true;
                    Shout_Tutorial.gameObject.SetActive(true);
                    Shout_Tutorial.index = 0;
                    Camera_Control.MoveLocked = true;
                    Avatar_Movement_Control.PowersLocked = true;
                    Avatar_Movement_Control.MoveLocked = true;
                    CitizenPanelManager.Locked = true;
                    return;
                }
            }
        }

        if (Game_Manager.Zombies > 0)
            Taunt_Event.Invoke(this.actorControl);

        Part_System_Noise.Emit(8);
        
    }

    public void Throw_Noise_Button()
    {
        
        if (Game_Manager.Instance.Tutorial_Mode)
        {
            if (Noise_Tutorial)
            {
                if (!NoiseTutDone && !PowersLocked)
                {
                    NoiseTutDone = true;
                    Noise_Tutorial.gameObject.SetActive(true);
                    Noise_Tutorial.index = 0;
                    Camera_Control.MoveLocked = true;
                    Avatar_Movement_Control.PowersLocked = true;
                    Avatar_Movement_Control.MoveLocked = true;
                    CitizenPanelManager.Locked = true;
                    return;
                }
            }
        }
        Control_Mode = Avatar_Control_Mode.Ranged_Taunt;
    }

    public void Restart_Button()
    {
        C_Data.Capsule_Ammo_Remaining = C_Data.Capsule_Ammo_Initial;
        C_Data.Remote_Capsule_Ammo_Remaining = C_Data.Remote_Capsule_Ammo_Initial;
        C_Data_HOLDOVER = C_Data;

        Game_Manager.Restart_Level();
    }

    public void TakeDamage(float Amount)
    {
        Health -= Amount;
        if (Health < 0)
            Health = 0;

        GotHurt_Event.Invoke();
    }

    public void ExitLevel()
    {
        //Application.LoadLevel("MainMenu");
        SceneManager.LoadScene(OuterMenuName);
    }

    void Awake()
    {
        if (!All_Controllers.Contains(this))
        {
            All_Controllers.Add(this);
        }

        if (C_Data_HOLDOVER != null)
            C_Data = C_Data_HOLDOVER;
        Cooldown_Rect_InitPos = CooldownRect.position;
    }

	void Start()
    {
        if (Main_Menu_Manager.Saved_Game != null)
        {
            C_Data = Main_Menu_Manager.Saved_Game.Stats;
            for (int i = 0; i < Main_Menu_Manager.Saved_Game.Equipped_Items.Count; i++)
            {
                inventory_Pointer.Pick_Up_Item(PickUp_Type.Equipment, Main_Menu_Manager.Saved_Game.Equipped_Items[i]);
            }
        }

        cam = GetComponent<Camera>();
        mask = ~(1 << 10 | 1 << 11 | 1 << 12 | 1 << 17 | 1 << 18);

        Health = Health_MAX;
	}
	
	void Update()
    {
        Ammo_Counter_Text.text = C_Data.Capsule_Ammo_Remaining.ToString();
        if (Game_Manager.State == GameState.Preview)
            actorControl.gameObject.SetActive(false);
        else if (Game_Manager.State == GameState.Playing)
        {
            actorControl.gameObject.SetActive(true);
            Victory_And_Failure_Check();

            #region Taunt Cooldown/Animation
            if (!CanUseTaunt)
            {
                rangeTauntCooldownTimer += Time.deltaTime;
                CooldownRect.localPosition -= Vector3.up * 16f * Time.deltaTime;
                if (rangeTauntCooldownTimer >= RangeTauntCooldown)
                {
                    rangeTauntCooldownTimer = 0f;
                    CanUseTaunt = true;
                    CooldownRect.gameObject.SetActive(false);
                }
            }
            #endregion

            //if (Input.GetKeyDown(KeyCode.Alpha1))
            //    Move_Mode_Button();
            if (Input.GetKeyDown(KeyCode.Alpha1) && C_Data.Capsule_Ammo_Remaining > 0)
                Launch_Mode_Button();
            if (Input.GetKeyDown(KeyCode.Alpha2))
                Make_Noise_Button();
            if (Input.GetKeyDown(KeyCode.Alpha3))
                Throw_Noise_Button();

            if (!MoveLocked)
            {
                if (Input.GetMouseButtonDown(1))
                {
                    if (Control_Mode != Avatar_Control_Mode.Move)
                    {
                        Control_Mode = Avatar_Control_Mode.Move;
                    }
                    else
                    {
                        Ranged_Dest = null;
                        stored_Mode = Avatar_Control_Mode._NONE;
                        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
                        RaycastHit rayHit;
                        if (Physics.Raycast(ray, out rayHit, 1000f, MoveRayLayerMask))
                        {
                            if (rayHit.collider.GetComponentInParent<Building_Behaviour>())
                            {
                                //actorControl.StartMoving(rayHit.collider.GetComponentInParent<Building_Behaviour>().DoorPoint.position, Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift));
                                actorControl.StartMoving(rayHit.collider.GetComponentInParent<Building_Behaviour>().DoorPoint.position, true, Speed + Speed_Modifier);
                            }
                            else if (rayHit.collider.GetComponent<Pickup_Component>())
                            {
                                actorControl.StartMoving(rayHit.collider.transform.position, true, Speed + Speed_Modifier);
                            }
                            else
                            {
                                //actorControl.StartMoving(rayHit.point, Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift));
                                actorControl.StartMoving(rayHit.point, true, Speed + Speed_Modifier);
                            }
                        }
                    }
                }
            }
            if (Control_Mode == Avatar_Control_Mode.Launch)
            {
                NPC_Police.ShowRanges = true;
            }
            else
            {
                NPC_Police.ShowRanges = false;
            }

            #region Queued Throws
            if (Ranged_Dest != null)
            {
                if (Vector3.Distance(actorControl.transform.position, Ranged_Dest.Value) <= Launch_Range)
                {
                    actorControl.Stop();
                    switch (stored_Mode)
                    {
                        case Avatar_Control_Mode.Launch:
                            if (C_Data.Capsule_Ammo_Remaining > 0)
                            {
                                GameObject cap = (GameObject)GameObject.Instantiate(Capsule_Prefab, Launch_Spawn_Marker.position, Launch_Spawn_Marker.rotation);
                                cap.GetComponent<Capsule_Script>().Owning_Player = this;
                                cap.GetComponent<Rigidbody>().AddForce(RigidBody_Helpers.CalculateBestThrowSpeed(Launch_Spawn_Marker.position, Ranged_Dest.Value, 1.25f), ForceMode.VelocityChange);
                                C_Data.Capsule_Ammo_Remaining--;
                                if (C_Data.Capsule_Ammo_Remaining == 0)
                                {
                                    Move_Mode_Button();
                                }
                            }
                            break;
                        case Avatar_Control_Mode.Ranged_Taunt:
                            if (CanUseTaunt)
                            {
                                GameObject part = (GameObject)GameObject.Instantiate(NoiseParticle_Prefab, Launch_Spawn_Marker.position, Quaternion.identity);
                                part.GetComponent<Rigidbody>().AddForce(RigidBody_Helpers.CalculateBestThrowSpeed(Launch_Spawn_Marker.position, Ranged_Dest.Value, 1.25f), ForceMode.VelocityChange);
                                CanUseTaunt = false;
                                CooldownRect.localPosition = Cooldown_Rect_InitPos;
                                CooldownRect.gameObject.SetActive(true);
                            }
                            break;
                    }
                    Ranged_Dest = null;
                    stored_Mode = Avatar_Control_Mode._NONE;
                }
            }
            #endregion

            if (Control_Mode == Avatar_Control_Mode.Ranged_Taunt || Control_Mode == Avatar_Control_Mode.Launch)
            {
                ThrowInstructions.gameObject.SetActive(true);
            }
            else
            {
                ThrowInstructions.gameObject.SetActive(false);
                OutOfRangeText.gameObject.SetActive(false);
            }

            switch (Control_Mode)
            {
                #region Movement Mode
                case Avatar_Control_Mode.Move:
                    Taunt_Target_Marker.gameObject.SetActive(false);
                    Launch_Target_Marker.gameObject.SetActive(false);
                    break;
                #endregion

                #region Capsule Launch Mode
                case Avatar_Control_Mode.Launch:
                    if (!PowersLocked)
                    {
                        //if (Input.GetMouseButtonDown(0))
                        //{
                        //    Control_Mode = Avatar_Control_Mode.Move;
                        //}
                        Ray ray2 = cam.ScreenPointToRay(Input.mousePosition);
                        RaycastHit rayHit2;
                        if (Physics.Raycast(ray2, out rayHit2, 1000f, mask.value))
                        {
                            Taunt_Target_Marker.gameObject.SetActive(false);
                            Launch_Target_Marker.gameObject.SetActive(true);
                            Launch_Target_Marker.position = rayHit2.point;
                            actorControl.transform.LookAt(rayHit2.point);
                            Launch_Target_Marker.rotation = Quaternion.Euler(Launch_Target_Marker.rotation.eulerAngles.x, cam.transform.parent.rotation.eulerAngles.y, Launch_Target_Marker.rotation.eulerAngles.z);
                            actorControl.transform.rotation = Quaternion.Euler(0f, actorControl.transform.rotation.eulerAngles.y, 0f);
                            if (Vector3.Distance(rayHit2.point, actorControl.transform.position) <= Launch_Range)
                            {
                                Launch_Target_Marker.GetComponentInChildren<Light>().color = new Color(0, 5, 0, 1f);
                                OutOfRangeText.gameObject.SetActive(false);
                                if (C_Data.Capsule_Ammo_Remaining > 0)
                                {
                                    if (Input.GetMouseButtonDown(0))
                                    {
                                        GameObject cap = (GameObject)GameObject.Instantiate(Capsule_Prefab, Launch_Spawn_Marker.position, Launch_Spawn_Marker.rotation);
                                        cap.GetComponent<Capsule_Script>().Owning_Player = this;
                                        cap.GetComponent<Rigidbody>().AddForce(RigidBody_Helpers.CalculateBestThrowSpeed(Launch_Spawn_Marker.position, rayHit2.point, 1.25f), ForceMode.VelocityChange);
                                        C_Data.Capsule_Ammo_Remaining--;
                                        if (C_Data.Capsule_Ammo_Remaining == 0)
                                        {
                                            Move_Mode_Button();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                Launch_Target_Marker.GetComponentInChildren<Light>().color = new Color(5, 0, 0, 1f);
                                OutOfRangeText.gameObject.SetActive(true);
                                //if (Input.GetMouseButtonDown(0))
                                //{
                                //    actorControl.StartMoving(rayHit2.point, true, Speed + Speed_Modifier);
                                //    Ranged_Dest = rayHit2.point;
                                //    stored_Mode = Control_Mode;
                                //}
                            }
                        }
                    }
                    break;
                #endregion

                #region Ranged Taunt Mode
                case Avatar_Control_Mode.Ranged_Taunt:
                    if (!PowersLocked)
                    {
                        if (Input.GetMouseButtonDown(0))
                        {
                            Control_Mode = Avatar_Control_Mode.Move;
                        }
                        Ray ray3 = cam.ScreenPointToRay(Input.mousePosition);
                        RaycastHit rayHit3;
                        if (Physics.Raycast(ray3, out rayHit3, 1000f, mask.value))
                        {
                            Launch_Target_Marker.gameObject.SetActive(false);
                            Taunt_Target_Marker.gameObject.SetActive(true);
                            Taunt_Target_Marker.position = rayHit3.point;
                            Taunt_Target_Marker.rotation = Quaternion.Euler(Taunt_Target_Marker.rotation.eulerAngles.x, cam.transform.parent.rotation.eulerAngles.y, Taunt_Target_Marker.rotation.eulerAngles.z);
                            actorControl.transform.LookAt(rayHit3.point);
                            actorControl.transform.rotation = Quaternion.Euler(0f, actorControl.transform.rotation.eulerAngles.y, 0f);
                            if (CanUseTaunt && Vector3.Distance(rayHit3.point, actorControl.transform.position) <= Launch_Range)
                            {
                                OutOfRangeText.gameObject.SetActive(false);
                                Taunt_Target_Marker.GetComponentInChildren<Light>().color = new Color(0, 5, 0, 1f);
                                if (Input.GetMouseButtonDown(0))
                                {
                                    GameObject part = (GameObject)GameObject.Instantiate(NoiseParticle_Prefab, Launch_Spawn_Marker.position, Quaternion.identity);
                                    part.GetComponent<Rigidbody>().AddForce(RigidBody_Helpers.CalculateBestThrowSpeed(Launch_Spawn_Marker.position, rayHit3.point, 1.25f), ForceMode.VelocityChange);
                                    CanUseTaunt = false;
                                    CooldownRect.localPosition = Cooldown_Rect_InitPos;
                                    CooldownRect.gameObject.SetActive(true);
                                    //Ranged_Taunt_Event.Invoke(rayHit3.point);
                                }
                            }
                            else
                            {
                                Taunt_Target_Marker.GetComponentInChildren<Light>().color = new Color(5, 0, 0, 1f);
                                OutOfRangeText.gameObject.SetActive(true);
                                //if (Input.GetMouseButtonDown(0))
                                //{
                                //    actorControl.StartMoving(rayHit3.point, true, Speed + Speed_Modifier);
                                //    Ranged_Dest = rayHit3.point;
                                //    stored_Mode = Control_Mode;
                                //}
                            }
                        }
                    }
                    break;
                #endregion
            }
        }
    }
}

[System.Serializable]
public class Contagion_Data
{
    //Points
    public int Points_Initial = 25;
    public int Points_Remaining = 25;
    [HideInInspector]
    public int Points_Earned = 0;

    //Player/Capsule Attributes
    public int Capsule_Ammo_Initial = 1;
    public int Capsule_Ammo_Remaining = 1;
    public int Remote_Capsule_Ammo_Initial = 0;
    public int Remote_Capsule_Ammo_Remaining = 0;

    public bool Stealth_Capsule = false;

    //Contagion Attributes
    public float Infectivity = 0;
    public float Lethality = 0.01f;
    public float Subtlety = 0;
    public float Range = 2;

    //Contagion Extras
    public bool Mutagenic_Mode = false;

    //Zombie Attributes
    public float Speed = 1f;
    public float Toughness = 1;
    public float Damage = 1;

    //Zombie Extras

    //Private / Fixed Stuff
    const int Concentration = 1;
}