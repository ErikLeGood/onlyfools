﻿using UnityEngine;
using System.IO;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class SaveData 
{
    public int Bosses_Defeated;
    public List<int> Complete_Level_Indices;
    public Contagion_Data Stats = new Contagion_Data();
    public List<int> Equipped_Items = new List<int>();
    public int Zombies_Spawned = 0;

    public bool Tutorial_Completed = false;

    public SaveData()
    {
        Equipped_Items = new List<int>();
        Complete_Level_Indices = new List<int>();
    }
}