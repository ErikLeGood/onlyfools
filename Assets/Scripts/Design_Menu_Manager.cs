﻿using UnityEngine;
using System;
using UnityEngine.UI;
using System.Collections;

public enum StatRating : int
{
    Terrible = 0, Low = 1, Medium = 2, High = 3, Exceptional = 4, Max = 5
}
public class Design_Menu_Manager : MonoBehaviour
{
    public Button[] Buttons;
    public bool View_Only_Mode = false;

    public Avatar_Movement_Control playerPointer;
    public Text PointsText;

    public Text Capsule_Counter;

    public Text Remote_Capsule_Counter;
    public Text Lethality_Counter;
    public Text Subtlety_Counter;
    public Text Contagiousness_Counter;
    public Text Damage_Counter;
    public Text Speed_Counter;
    public Text Toughness_Counter;

    public GameObject ConfirmWindow;
    public GameObject ReturnToGameButton;
    public GameObject ReturnToPreviewButton;

    public Text ZombieStats_HP;
    public Text ZombieStats_Speed;
    public Text ZombieStats_Toughness;
    public Text ZombieStats_Damage;

    public void BeginButton_Click()
    {
        if (playerPointer.C_Data.Points_Remaining > 0)
        {
            ConfirmWindow.SetActive(true);
        }
        else
        {
            StartGame();
        }
    }
    public void ConfirmForm_YES()
    {
        StartGame();
    }
    public void ConfirmForm_NO()
    {
        ConfirmWindow.SetActive(false);
    }

    void StartGame()
    {
        playerPointer.C_Data.Capsule_Ammo_Remaining = playerPointer.C_Data.Capsule_Ammo_Initial;
        Game_Manager.Begin_Game(playerPointer.C_Data.Remote_Capsule_Ammo_Initial > 0);
    }

    public void ResetButton()
    {
        playerPointer.C_Data.Points_Remaining = playerPointer.C_Data.Points_Initial;
        playerPointer.C_Data.Capsule_Ammo_Initial = CapsuleMinimum;
        playerPointer.C_Data.Remote_Capsule_Ammo_Initial = RemoteCapsuleMinimum;

        DamageRating = 0;
        ContagiousnessRating = 0;
        LethalityRating = 1;
        SpeedRating = 0;
        SubtletyRating = 0;
        ToughnessRating = 0;

        playerPointer.C_Data.Damage = CalculateDamage();
        playerPointer.C_Data.Infectivity = CalculateContagiousness();
        playerPointer.C_Data.Lethality = CalculateLethality();
        playerPointer.C_Data.Speed = CalculateSpeed();
        playerPointer.C_Data.Subtlety = CalculateSubtlety();
        playerPointer.C_Data.Toughness = CalculateToughness();

        playerPointer.C_Data.Mutagenic_Mode = false;
    }

    public void PreviewButton()
    {
        Game_Manager.PreviewLevel();
    }

    public void Refresh()
    {
        playerPointer.C_Data.Damage = CalculateDamage();
        playerPointer.C_Data.Infectivity = CalculateContagiousness();
        playerPointer.C_Data.Lethality = CalculateLethality();
        playerPointer.C_Data.Speed = CalculateSpeed();
        playerPointer.C_Data.Subtlety = CalculateSubtlety();
        playerPointer.C_Data.Toughness = CalculateToughness();
    }

    public void Awake()
    {
        Buttons = GetComponentsInChildren<Button>();
    }

    void HideButtons()
    {
        for (int i = 0; i < Buttons.Length; i++)
        {
            Buttons[i].gameObject.SetActive(false);
        }
    }

    void ShowButtons()
    {
        for (int i = 0; i < Buttons.Length; i++)
        {
            Buttons[i].gameObject.SetActive(true);
        }
    }

    public void ReviewModeButton()
    {
        if (!Avatar_Movement_Control.PowersLocked)
        {
            Game_Manager.Instance.Show_UI = false;
            if (Game_Manager.State == GameState.Playing)
            {
                View_Only_Mode = true;
                HideButtons();
            }
            Game_Manager.Show_Design_HUD();
        }
    }

    public void ReturnToGame()
    {
        View_Only_Mode = false;
        ShowButtons();
        Game_Manager.Show_Active_HUD();
        Game_Manager.Instance.Show_UI = true;
    }

	void Start()
    {
        ConfirmWindow.SetActive(false);
        ResetButton();
	}
	
	void Update()
    {
        ReturnToGameButton.SetActive(View_Only_Mode);
        ReturnToPreviewButton.SetActive(!View_Only_Mode);
        if(View_Only_Mode)
        {
            ConfirmWindow.SetActive(false);
        }
        PointsText.text = playerPointer.C_Data.Points_Remaining.ToString() + " / " + playerPointer.C_Data.Points_Initial.ToString();
        
        Capsule_Counter.text = playerPointer.C_Data.Capsule_Ammo_Initial.ToString();
        Remote_Capsule_Counter.text = playerPointer.C_Data.Remote_Capsule_Ammo_Initial.ToString();
        Lethality_Counter.text = LethalityRating + "/5";
        Subtlety_Counter.text = SubtletyRating + "/5";
        Contagiousness_Counter.text = ContagiousnessRating + "/5";
        Damage_Counter.text = DamageRating + "/5";
        Speed_Counter.text = SpeedRating + "/5";
        Toughness_Counter.text = ToughnessRating + "/5";

        string dmgText = "(";
        if (DamageRating == 0)
            dmgText += "Gentle";
        else if (DamageRating == 1)
            dmgText += "Weak";
        else if (DamageRating == 2)
            dmgText += "Normal";
        else if (DamageRating == 3)
            dmgText += "Strong";
        else if (DamageRating == 4)
            dmgText += "Painful";
        else if (DamageRating == 5)
            dmgText += "Crippling";

        dmgText += ")";
        ZombieStats_Damage.text = "Damage (Per Second): " + playerPointer.C_Data.Damage.ToString() + " " + dmgText;
        string speedtext = "(";
        if (SpeedRating == 0)
            speedtext += "Barely-Mobile";
        else if (SpeedRating == 1)
            speedtext += "Slow";
        else if (SpeedRating == 2)
            speedtext += "Normal";
        else if (SpeedRating == 3)
            speedtext += "Quick";
        else if (SpeedRating == 4)
            speedtext += "Fast";
        else if (SpeedRating == 5)
            speedtext += "Frenzied";

        speedtext += ")";
        ZombieStats_Speed.text = "Speed: " + playerPointer.C_Data.Speed.ToString() + " " + speedtext;
        
        //ZombieStats_HP.text = "HP: " + (playerPointer.C_Data.Toughness + 5).ToString() + " (Shots to kill: " + Mathf.CeilToInt(((playerPointer.C_Data.Toughness + 5) / (NPC_Police.Damage / playerPointer.C_Data.Toughness))).ToString() + ")";
        string tufText = "(";

        if (ToughnessRating == 0)
            tufText += "FLimsy";
        else if (ToughnessRating == 1)
            tufText += "Weak";
        else if (ToughnessRating == 2)
            tufText += "Normal";
        else if (ToughnessRating == 3)
            tufText += "Tough";
        else if (ToughnessRating == 4)
            tufText += "Durable";
        else if (ToughnessRating == 5)
            tufText += "Undying";

        tufText += ")";

        ZombieStats_Toughness.text = "Shots (to kill): " + Mathf.CeilToInt(((playerPointer.C_Data.Toughness + 5) / (NPC_Police.Damage / playerPointer.C_Data.Toughness))).ToString() + " " + tufText;
	}

    //Buttons - PURCHASE COSTS, MINIMUM LEVELS AND INCREMENTS ARE DEFINED HERE
    #region Costs, Minimums and Amounts
    int CapsuleCost = 10;
    int CapsuleMinimum = 1;

    int RemoteCapsuleCost = 20;
    int RemoteCapsuleMinimum = 0;

    int LethalityRating = 1;
    int SubtletyRating = 0;
    int ContagiousnessRating = 0;
    int DamageRating = 0;
    int SpeedRating = 0;
    int ToughnessRating = 0;

    //int LethalityCost = 1;
    //int LethalityAmount = 1;
    ////float LethalityAmount = 0.1f;
    //int LethalityMinimum = 1;

    //int SubtletyCost = 1;
    //float SubtletyAmount = 0.5f;
    //float SubtletyMinimum = 0f;
    
    //int ContagiousnessCost = 1;
    //float ContagiousnessAmount = 0.1f;
    //float ContagiousnessMinimum = 0.1f;

    //int DamageCost = 1;
    //float DamageAmount = 0.5f;
    //float DamageMinimum = 1f;

    //int SpeedCost = 1;
    //float SpeedAmount = 0.5f;
    //float SpeedMinimum = 1f;

    //int ToughnessCost = 1;
    //float ToughnessAmount = 0.1f;
    //float ToughnessMinimum = 1f;
    #endregion

    #region Button Functions

    public void Capsule_Count_Button_More()
    {
        if (playerPointer.C_Data.Points_Remaining >= CapsuleCost)
        {
            playerPointer.C_Data.Points_Remaining -= CapsuleCost;

            playerPointer.C_Data.Capsule_Ammo_Initial++;

            playerPointer.C_Data.Capsule_Ammo_Remaining = playerPointer.C_Data.Capsule_Ammo_Initial;
        }
    }
    public void Capsule_Count_Button_Less()
    {
        if (playerPointer.C_Data.Capsule_Ammo_Initial > CapsuleMinimum)
        {
            if (playerPointer.C_Data.Points_Remaining <= playerPointer.C_Data.Points_Initial - CapsuleCost)
            {
                playerPointer.C_Data.Points_Remaining += CapsuleCost;

                playerPointer.C_Data.Capsule_Ammo_Initial--;
                playerPointer.C_Data.Capsule_Ammo_Remaining = playerPointer.C_Data.Capsule_Ammo_Initial;
            }
        }
    }

    public void Remote_Capsules_Button_More()
    {
        if (playerPointer.C_Data.Points_Remaining >= RemoteCapsuleCost)
        {
            playerPointer.C_Data.Points_Remaining -= RemoteCapsuleCost;

            playerPointer.C_Data.Remote_Capsule_Ammo_Initial++;
            playerPointer.C_Data.Remote_Capsule_Ammo_Remaining = playerPointer.C_Data.Remote_Capsule_Ammo_Initial;
        }
    }
    public void Remote_Capsules_Button_Less()
    {
        if (playerPointer.C_Data.Remote_Capsule_Ammo_Initial > RemoteCapsuleMinimum)
        {
            if (playerPointer.C_Data.Points_Remaining <= playerPointer.C_Data.Points_Initial - RemoteCapsuleCost)
            {
                playerPointer.C_Data.Points_Remaining += RemoteCapsuleCost;

                playerPointer.C_Data.Remote_Capsule_Ammo_Initial--;
                playerPointer.C_Data.Remote_Capsule_Ammo_Remaining = playerPointer.C_Data.Remote_Capsule_Ammo_Initial;
            }
        }
    }

    public void Lethality_Button_More()
    {
        if (playerPointer.C_Data.Points_Remaining >= 1)
        {
            if (LethalityRating < 5)
            {
                playerPointer.C_Data.Points_Remaining--;

                LethalityRating++;
                playerPointer.C_Data.Lethality = CalculateLethality();
            }
        }
    }
    public void Lethality_Button_Less()
    {
        if (LethalityRating > 1)
        {
            if (playerPointer.C_Data.Points_Remaining <= playerPointer.C_Data.Points_Initial - 1)
            {
                playerPointer.C_Data.Points_Remaining++;
                LethalityRating--;
                playerPointer.C_Data.Lethality = CalculateLethality();
            }
        }
        
    }
    float CalculateLethality()
    {
        return ((LethalityRating + 1f) / 5f);
    }

    public void Subtlety_Button_More()
    {
        if (playerPointer.C_Data.Points_Remaining >= 1)
        {
            if (SubtletyRating < 5)
            {
                playerPointer.C_Data.Points_Remaining--;
                SubtletyRating++;
                playerPointer.C_Data.Subtlety = CalculateSubtlety();
            }
        }
    }
    public void Subtlety_Button_Less()
    {
        if (SubtletyRating > 0)
        {
            if (playerPointer.C_Data.Points_Remaining <= playerPointer.C_Data.Points_Initial - 1)
            {
                playerPointer.C_Data.Points_Remaining++;

                SubtletyRating--;
                playerPointer.C_Data.Subtlety = CalculateSubtlety();
            }
        }
    }
    float CalculateSubtlety()
    {
        return SubtletyRating * 2f;
    }

    public void Contagiousness_Button_More()
    {
        if (playerPointer.C_Data.Points_Remaining >= 1)
        {
            if (ContagiousnessRating < 5)
            {
                playerPointer.C_Data.Points_Remaining--;
                ContagiousnessRating++;
                playerPointer.C_Data.Infectivity = CalculateContagiousness();
            }
        }
    }
    public void Contagiousness_Button_Less()
    {
        if (ContagiousnessRating > 0)
        {
            if (playerPointer.C_Data.Points_Remaining <= playerPointer.C_Data.Points_Initial - 1)
            {
                playerPointer.C_Data.Points_Remaining++;
                ContagiousnessRating--;
                playerPointer.C_Data.Infectivity = CalculateContagiousness();
            }
        }
    }
    float CalculateContagiousness()
    {
        return ContagiousnessRating * 0.2f;
    }

    public void Damage_Button_More()
    {
        if (playerPointer.C_Data.Points_Remaining >= 1)
        {
            if (DamageRating < 5)
            {
                playerPointer.C_Data.Points_Remaining--;
                DamageRating++;
                playerPointer.C_Data.Damage = CalculateDamage();
            }
        }
    }
    public void Damage_Button_Less()
    {
        if (DamageRating > 0)
        {
            if (playerPointer.C_Data.Points_Remaining <= playerPointer.C_Data.Points_Initial - 1)
            {
                playerPointer.C_Data.Points_Remaining++;
                DamageRating--;
                playerPointer.C_Data.Damage = CalculateDamage();
            }
        }
    }
    float CalculateDamage()
    {
        return 0.5f + (DamageRating * 0.75f);
    }

    public void Speed_Button_More()
    {
        if (playerPointer.C_Data.Points_Remaining >= 1)
        {
            if (SpeedRating < 5)
            {
                playerPointer.C_Data.Points_Remaining--;
                SpeedRating++;
                playerPointer.C_Data.Speed = CalculateSpeed();
            }
        }
    }
    public void Speed_Button_Less()
    {
        if (SpeedRating > 0)
        {
            if (playerPointer.C_Data.Points_Remaining <= playerPointer.C_Data.Points_Initial - 1)
            {
                playerPointer.C_Data.Points_Remaining++;
                SpeedRating--;
                playerPointer.C_Data.Speed = CalculateSpeed();
            }
        }
    }
    float CalculateSpeed()
    {
        return 1f + (SpeedRating);
    }

    public void Toughness_Button_More()
    {
        if (playerPointer.C_Data.Points_Remaining >= 1)
        {
            if (ToughnessRating < 5)
            {
                playerPointer.C_Data.Points_Remaining--;
                ToughnessRating++;
                playerPointer.C_Data.Toughness = CalculateToughness();
            }
        }
    }
    public void Toughness_Button_Less()
    {
        if (ToughnessRating > 0)
        {
            if (playerPointer.C_Data.Points_Remaining <= playerPointer.C_Data.Points_Initial - 1)
            {
                playerPointer.C_Data.Points_Remaining++;

                ToughnessRating--;
                playerPointer.C_Data.Toughness = CalculateToughness();
            }
        }
    }
    float CalculateToughness()
    {
        return 1f + (0.5f * ToughnessRating);
    }

    #endregion
}