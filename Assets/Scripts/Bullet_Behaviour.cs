﻿using UnityEngine;
using System.Collections;

public class Bullet_Behaviour : MonoBehaviour
{
    public float Speed_Life = 0.75f;

	void Start()
    {
        GameObject.Destroy(this.gameObject, Speed_Life + 0.1f);
	}
	
	void Update()
    {
	
	}

    public static void Spawn(Bullet_Behaviour prefab, Vector3 position, Quaternion rotation, Vector3 destination)
    {
        GameObject bullet = (GameObject)GameObject.Instantiate(prefab.gameObject, position, rotation);
        bullet.GetComponent<Rigidbody>().AddForce(RigidBody_Helpers.CalculateBestThrowSpeed(bullet.transform.position, destination, prefab.Speed_Life));
    }

}