﻿using UnityEngine;
using System.Collections;

public class Pickup_Component : MonoBehaviour
{
    public PickUp_Type Type = PickUp_Type.PowerUp;
    public int Index = 0;

	void Start()
    {
	    //Configure Visuals Here.
	}
	
	void Update()
    {
	
	}

    void OnTriggerStay(Collider other)
    {
        Actor_Data aDat = other.GetComponent<Actor_Data>();
        if (aDat)
        {
            if (aDat.optional_Player_Marker)
            {
                aDat.optional_Player_Marker.C_Data.Points_Earned++;
                GameObject.Destroy(gameObject);
                //if (aDat.optional_Player_Marker.inventory_Pointer.Pick_Up_Item(Type, Index))
                //{
                //    gameObject.SetActive(false);
                //}
            }
        }
    }
    //void OnTriggerExit(Collider other)
    //{
    //    Actor_Data aDat = other.GetComponent<Actor_Data>();
    //    if (aDat)
    //    {
    //        if (aDat.optional_Player_Marker)
    //        {
    //            //aDat.optional_Player_Marker.inventory_Pointer.CancelDeleteButton();
    //        }
    //    }
    //}
}