﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Inventory : MonoBehaviour
{
    public Texture BlankIcon;
    public int Max_Equipment = 2;

    public List<Image> EquipmentHudIcons = new List<Image>();

    public List<int> Equipped_Items = new List<int>();
    public List<int> Local_Acquired_Unlocks = new List<int>();
    public List<int> Active_Powerups = new List<int>();

    List<Pick_Up> Active_Equipment = new List<Pick_Up>();
    Avatar_Movement_Control controller;
    bool TutorialDone = false;
    public Tutorial_Click_Through Tutorial;

	void Start()
    {
        controller = GetComponent<Avatar_Movement_Control>();
        ApplyPassiveEquipmentEffects();
	}

    bool DeleteMode = false;
    public GameObject DeletionWindow;
    public GameObject ConfirmDeleteForm;
    public List<Image> DeletionButtons = new List<Image>();
    int deletion_target_index = -1;

    public void ConfirmDeleteYes()
    {
        DeletionWindow.SetActive(false);
        ConfirmDeleteForm.SetActive(false);
        Equipped_Items.RemoveAt(deletion_target_index);
        Active_Equipment.RemoveAt(deletion_target_index);
        EquipmentHudIcons[deletion_target_index].sprite = Sprite.Create((Texture2D)BlankIcon, EquipmentHudIcons[deletion_target_index].sprite.rect, new Vector2(0.5f, 0.5f));
    }
    public void ConfirmDeleteNo()
    {
        ConfirmDeleteForm.SetActive(false);
    }

    public void DeleteItemButton(int input)
    {
        deletion_target_index = input;
        ConfirmDeleteForm.SetActive(true);
    }
	
    void EnterDeleteMode()
    {
        DeletionWindow.SetActive(true);
        DeleteMode = true;
        for (int i = 0; i < Active_Equipment.Count; i++)
        {
            DeletionButtons[i].sprite = Sprite.Create((Texture2D)EquipmentHudIcons[i].mainTexture, EquipmentHudIcons[i].sprite.rect, new Vector2(0.5f, 0.5f));
        }
    }

    void Hotkey_Controls()
    {
        if (Input.GetKeyDown(KeyCode.E))
            EquipmentButtonUsed(0);
        if (Input.GetKeyDown(KeyCode.R))
            EquipmentButtonUsed(1);
    }
    
	void Update()
    {
        Hotkey_Controls();
        Update_Active_Powerups();
        Update_Active_Equipment();

        if (DeleteMode)
        {
            ItemDeletionModeUpdate();
        }
        else if (DeletionWindow.activeInHierarchy)
        {
            DeletionWindow.SetActive(false);
            ConfirmDeleteForm.SetActive(false);
        }
	}

    void ItemDeletionModeUpdate()
    {

    }
    public void CancelDeleteButton()
    {
        DeleteMode = false;
        DeletionWindow.SetActive(false);
    }

    private List<int> powerupRemovalTargets = new List<int>();

    void Update_Active_Powerups()
    {
        powerupRemovalTargets.Clear();
        for (int i = 0; i < Active_Powerups.Count; i++)
        {
            Item_Catalogue.Instance.Power_Ups[Active_Powerups[i]].timer += Time.deltaTime;
            if (Item_Catalogue.Instance.Power_Ups[Active_Powerups[i]].timer >= Item_Catalogue.Instance.Power_Ups[Active_Powerups[i]].Duration)
            {
                Item_Catalogue.Instance.Power_Ups[Active_Powerups[i]].timer = 0f;
                powerupRemovalTargets.Add(Active_Powerups[i]);
                switch (Item_Catalogue.Instance.Power_Ups[Active_Powerups[i]].PowerUp_SubType)
                {
                    case PowerUp_Type.SpeedBoost:
                        controller.Speed_Modifier -= Item_Catalogue.Instance.Power_Ups[Active_Powerups[i]].Magnitude;
                        if (controller.Speed_Modifier < 0f)
                            controller.Speed_Modifier = 0f;
                        break;
                    case PowerUp_Type.Invulnerability:
                        controller.Invulnerable = false;
                        break;
                }
            }
        }
        for (int i = 0; i < powerupRemovalTargets.Count; i++)
        {
            Active_Powerups.Remove(powerupRemovalTargets[i]);
        }
    }
    void Update_Active_Equipment()
    {
        for (int i = 0; i < Active_Equipment.Count; i++)
        {
            #region Cooldown
            if (!Active_Equipment[i].Ready_To_Use)
            {
                Active_Equipment[i].cooldown_Timer += Time.deltaTime;
                if (Active_Equipment[i].cooldown_Timer >= Active_Equipment[i].Cooldown)
                {
                    Active_Equipment[i].cooldown_Timer = 0f;
                    Active_Equipment[i].Ready_To_Use = true;
                }
            }
            #endregion
        }
    }

    void Add_Equipment(int item)
    {
        Equipped_Items.Add(item);
        Active_Equipment.Add(Item_Catalogue.Instance.Equipment[item]);
        ApplyPassiveEquipmentEffects();

        //Visual Stuff
        for (int i = 0; i < EquipmentHudIcons.Count; i++)
        {
            if (EquipmentHudIcons[i].mainTexture.name == "Blank")
            {
                EquipmentHudIcons[i].sprite = Sprite.Create((Texture2D)Active_Equipment[Active_Equipment.Count - 1].Icon, EquipmentHudIcons[Active_Equipment.Count - 1].sprite.rect, new Vector2(0.5f, 0.5f));
                break;
            }
        }
    }
    void Remove_Equipment(int index_Target)
    {
        Equipped_Items.RemoveAt(index_Target);
        Active_Equipment.RemoveAt(index_Target);
    }
    
    bool Pick_Up_Equipment(int item)
    {
        if (Equipped_Items.Count < Max_Equipment)
        {
            if (Equipped_Items.Contains(item))
            {
                if (Item_Catalogue.Instance.Equipment[item].Stackable)
                {
                    Add_Equipment(item);
                    return true;
                }
                else
                {
                    //Can't-carry-any-more-of-those feedback goes here.
                }
            }
            else
            {
                Add_Equipment(item);
                return true;
            }
        }
        else 
        {
            EnterDeleteMode();
            //Select-Item-To-Remove functionality goes here.
        }
        return false;
    }
    //This function is where powerups are programmed. partly.
    bool Pick_Up_Powerups(int item)
    {
        if (Active_Powerups.Contains(item))
        {
            Item_Catalogue.Instance.Power_Ups[item].timer = 0f;
            return true;
        }
        else
        {
            Active_Powerups.Add(item);
            switch (Item_Catalogue.Instance.Power_Ups[item].PowerUp_SubType)
            {
                case PowerUp_Type.SpeedBoost:
                    controller.Speed_Modifier += Item_Catalogue.Instance.Power_Ups[item].Magnitude;
                    return true;
                case PowerUp_Type.Health:
                    controller.Health += Item_Catalogue.Instance.Power_Ups[item].Magnitude;
                    if (controller.Health > controller.Health_MAX)
                        controller.Health = controller.Health_MAX;
                    return true;
                case PowerUp_Type.Score:
                    Game_Manager.Score++;
                    controller.C_Data.Points_Earned += Item_Catalogue.Instance.Power_Ups[item].Magnitude;
                    //controller.C_Data.Points_Initial += Item_Catalogue.Instance.Power_Ups[item].Magnitude;
                    //controller.C_Data.Points_Remaining += Item_Catalogue.Instance.Power_Ups[item].Magnitude;
                    return true;
                case PowerUp_Type.Invulnerability:
                    controller.Invulnerable = true;
                    return true;
            }
        }
        return false;
    }
    bool Pick_Up_Unlocks(int item)
    {
        //Requires -Significant- work to the game structure.
        return false;
    }

    //Called by Pickup_Component.cs
    public bool Pick_Up_Item(PickUp_Type Type, int item)
    {
        if (Game_Manager.Instance.Tutorial_Mode)
        {
            if (!TutorialDone)
            {
                TutorialDone = true;
                Tutorial.gameObject.SetActive(true);
            }
        }
        switch (Type)
        {
            case PickUp_Type.Equipment:
                return Pick_Up_Equipment(item);
                //break;
            case PickUp_Type.PowerUp:
                return Pick_Up_Powerups(item);
                //break;
            case PickUp_Type.Unlock:
                return Pick_Up_Unlocks(item);
                //break;
        }
        return false;
    }

    //These 2 functions are where the actual functionality of equipment items should be added.
    public void EquipmentButtonUsed(int index)
    {
        if (index < Active_Equipment.Count)
        {
            if (Active_Equipment[index] != null)
            {
                if (Active_Equipment[index].ActivatedAbility)
                {
                    if (Active_Equipment[index].Ready_To_Use)
                    {
                        Active_Equipment[index].Ready_To_Use = false;
                        switch (Active_Equipment[index].Equipment_SubType)
                        {
                            case Equipment_Type.Sprint:
                                break;
                            case Equipment_Type.Stealth:
                                break;
                        }
                    }
                }
            }
        }
    }

    public void ApplyPassiveEquipmentEffects()
    {
        controller.Health_MAX = controller.Health_BASE_MAX;

        for (int i = 0; i < Active_Equipment.Count; i++)
        {
            if (!Active_Equipment[i].ActivatedAbility)
            {
                switch (Active_Equipment[i].Equipment_SubType)
                {
                    case Equipment_Type.Health_Increase:
                        controller.Health_MAX += Active_Equipment[i].Magnitude;
                        break;
                }
            }
        }
    }
}