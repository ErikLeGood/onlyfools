﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Item_Catalogue : MonoBehaviour
{
    public List<Pick_Up> Power_Ups = new List<Pick_Up>();
    public List<Pick_Up> Equipment = new List<Pick_Up>();
    public List<Pick_Up> Unlocks = new List<Pick_Up>();

    public static Item_Catalogue Instance;

    public GameObject World_Item_Prefab;

    public void SpawnRandomItem(Vector3 position)
    {
        Pickup_Component comp = ((GameObject)GameObject.Instantiate(World_Item_Prefab, position, Quaternion.identity)).GetComponent<Pickup_Component>();
        switch (Random.Range(0,3))
        {
            case 0:
                comp.Type = PickUp_Type.Equipment;
                comp.Index = Random.Range(0, Equipment.Count);
                break;
            case 1:
                comp.Type = PickUp_Type.PowerUp;
                comp.Index = Random.Range(0, Power_Ups.Count);
                break;
            case 2:
                comp.Type = PickUp_Type.Unlock;
                comp.Index = Random.Range(0, Unlocks.Count);
                break;
            default:
                Debug.LogError("Item_Catalogue Indexing Exception.");
                break;
        }
    }
    public void SpawnRandomItemOfType(Vector3 position, PickUp_Type Type)
    {
        Pickup_Component comp = ((GameObject)GameObject.Instantiate(World_Item_Prefab, position, Quaternion.identity)).GetComponent<Pickup_Component>();
        switch (Type)
        {
            case PickUp_Type.Equipment:
                comp.Type = PickUp_Type.Equipment;
                comp.Index = Random.Range(0, Equipment.Count);
                break;
            case PickUp_Type.PowerUp:
                comp.Type = PickUp_Type.PowerUp;
                comp.Index = Random.Range(0, Power_Ups.Count);
                break;
            case PickUp_Type.Unlock:
                comp.Type = PickUp_Type.Unlock;
                comp.Index = Random.Range(0, Unlocks.Count);
                break;
            default:
                Debug.LogError("Item_Catalogue Indexing Exception.");
                break;
        }
    }
    public void SpawnRandomItemOfTypeXOrY(Vector3 position, PickUp_Type TypeX, PickUp_Type TypeY)
    {
        Pickup_Component comp = ((GameObject)GameObject.Instantiate(World_Item_Prefab, position, Quaternion.identity)).GetComponent<Pickup_Component>();
        switch (Random.Range(0, 2))
        {
            case 0:
                SpawnRandomItemOfType(position, TypeX);
                break;
            case 1:
                SpawnRandomItemOfType(position, TypeY);
                break;
        }
    }

    void Awake()
    {
        Instance = this;
    }
}