﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public enum PickUp_Type : int
{
    PowerUp = 0,
    Equipment = 1,
    Unlock = 2
}

[System.Serializable]
public enum PowerUp_Type : int
{
    _NONE = -1,
    Health = 0,
    SpeedBoost = 1,
    Invulnerability = 2,
    Score = 3
}

[System.Serializable]
public enum Equipment_Type : int
{
    _NONE = -1,
    Sprint = 0,
    Health_Regen = 1,
    Stealth = 2,
    Health_Increase = 3
}

[System.Serializable]
public enum Unlock_Type : int
{
    _NONE = -1, AlphaZombie = 0, PlagueSpreaders = 1, Firestarters = 2
}

[System.Serializable]
public class Pick_Up
{
    public string Name = "Pick_Up";
    public string Description = "Item Description Here.";
    public PickUp_Type Type;
    public PowerUp_Type PowerUp_SubType = PowerUp_Type._NONE;
    public Equipment_Type Equipment_SubType = Equipment_Type._NONE;
    public Unlock_Type Unlock_SubType = Unlock_Type._NONE;

    public Texture Icon;

    public bool ActivatedAbility = false;
    [HideInInspector]
    public bool IsActivated = false;

    public int Magnitude = 1;
    public float Duration = 0f;
    [HideInInspector]
    public float timer = 0f;

    public float Cooldown = 0f;
    [HideInInspector]
    public float cooldown_Timer = 0f;
    [HideInInspector]
    public bool Ready_To_Use = false;

    public bool Stackable = false;
}