﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public enum BuildingType : int
{
    House = 0, 
    PlayerHouse = 1, 
    Clinic = 2, 
    PoliceStation = 3, 
    Powerplant = 4, 
    Shop = 5, 
    Butcher = 6,
    Cinema = 7,
    Bank = 8
}

public class Building_Behaviour : MonoBehaviour 
{
    public Transform DoorPoint;
    public Transform OptionalExitPoint;
    public Transform ZombieAttackMarker;
    public GameObject ContagionCloudPrefab;
    public GameObject[] EnableWhenDestroyed;
    Avatar_Movement_Control infection;

    public Renderer renderer;
    public Texture destroyedTexture;
    public int MaterialIndex = 0;

    bool HasCapsule = false;
    public bool Capsule_Placed
    {
        get { return HasCapsule; }
    }

    public bool Assign_Capsule(Avatar_Movement_Control infector)
    {
        if (!HasCapsule)
        {
            if (infector.C_Data.Remote_Capsule_Ammo_Remaining > 0)
            {
                HasCapsule = true;
                infection = infector;
                infector.C_Data.Remote_Capsule_Ammo_Remaining--;
                return true;
            }
        }
        return false;
    }

    public void Remove_Capsule(Avatar_Movement_Control infector)
    {
        if (HasCapsule)
        {
            if (infector.C_Data.Remote_Capsule_Ammo_Remaining < infector.C_Data.Remote_Capsule_Ammo_Initial)
            {
                HasCapsule = false;
                infection = null;
                infector.C_Data.Remote_Capsule_Ammo_Remaining++;
            }
        }
    }

    public void Detonate_Remote()
    {
        GameObject.Instantiate(ContagionCloudPrefab, transform.position, Quaternion.identity);
        Infect_All_Inhabitants(infection);
        infection.C_Data.Remote_Capsule_Ammo_Remaining--;
        HasCapsule = false;
    }
    public void Infect_All_Inhabitants(Avatar_Movement_Control infection)
    {
        if (Building_Type != BuildingType.Clinic)
        {
            for (int i = 0; i < Population; i++)
            {
                switch (Contained_Actors[i].ActorType)
                {
                    case Actor_Type.Citizen:
                        Contained_Actors[i].GetComponent<NPC_Citizen>().Infect(infection, false);
                        break;
                    case Actor_Type.Police:
                        break;
                }
            }
        }
    }

    public BuildingType Building_Type = BuildingType.House;
    public int Capacity = 5;
    public int Attraction = 1;
    public float Health = 15f;
    public bool Locked = false;
    bool Destroyed = false;

    public List<Actor_Data> Contained_Actors = new List<Actor_Data>();

    [HideInInspector]
    public int Population
    {
        get
        {
            return Contained_Actors.Count;
        }
    }

    public float Boot_Time_Remaining
    {
        get
        {
            return boot_Duration - boot_Timer;
        }
    }
    float boot_Timer = 0f;
    float boot_BaseTime = 1f;
    float boot_Duration = 0f;

	void Start()
    {
	
	}

    public float Lock_Time_Remaining
    {
        get { return locklength - locktimer; }
    }
    float locktimer = 0f;
    public float locklength = 60f;

    public bool HasSpace
    {
        get
        {
            return Population < Capacity;
        }
    }

	void FixedUpdate()
    {
        if (Health > 0f)
        {
            #region Auto-Unlock
            if (Locked)
            {
                locktimer += Time.deltaTime;
                if (locktimer >= locklength)
                {
                    locktimer = 0f;
                    Locked = false;
                }
            }
            #endregion
            if (Population > 0)
            {
                NPC_Citizen npc_C;
                for (int i = 0; i < Population; i++)
                {
                    if (Contained_Actors[i].Containing_Building != this)
                        Contained_Actors[i].Containing_Building = this;

                    npc_C = null;
                    npc_C = Contained_Actors[i].GetComponent<NPC_Citizen>();
                    if (npc_C)
                    {
                        if (npc_C.Infected)
                        {
                            npc_C.Health_Tick();
                            if (npc_C.Health <= 0)
                                Eject_Actor(Contained_Actors[i]);
                            if (npc_C.Is_Dead_Check(true))
                            {
                                Game_Manager.Spawn_Zombie(DoorPoint.position, npc_C.InfectionPointer);
                                for (int j = 0; j < Population; j++)
                                {
                                    if (i != j)
                                    {
                                        if (!Contained_Actors[j].GetComponent<NPC_Citizen>().Infected)
                                        {
                                            Contained_Actors[j].GetComponent<NPC_Citizen>().Infect(npc_C.InfectionPointer, false);
                                            Stats.Infections_Hygiene++;
                                        }
                                    }
                                }
                                Health = 0;
                            }
                        }
                    }
                }
                if (boot_Duration == 0f)
                {
                    float bootDurationModifier = 0f;
                    if (Building_Type == BuildingType.Cinema)
                    {
                        bootDurationModifier += 60f;
                    }
                    boot_Duration = boot_BaseTime + Random.Range(0f, 5f) + bootDurationModifier;
                    boot_Timer += Time.deltaTime;
                }
                else
                {
                    boot_Timer += Time.deltaTime;
                    
                    if (boot_Timer >= boot_Duration + (Weather.CurrentWeather == WeatherType.Storm ? 15f : 0f))
                    {
                        boot_Duration = 0f;
                        boot_Timer = 0f;
                        for (int i = 0; i < Contained_Actors.Count; i++)
                        {
                            
                            NPC_Citizen npc = Contained_Actors[i].GetComponent<NPC_Citizen>();
                            if (npc)
                            {
                                #region NPC is Calm, and not in a hospital
                                if (npc.panic_State == NPC_Panic_State.Calm && Building_Type != BuildingType.Clinic)
                                {
                                    for (int j = 0; j < Contained_Actors.Count; j++)
                                    {
                                        if (j == i)
                                            continue;

                                        NPC_Citizen OtherNPC = Contained_Actors[j].GetComponent<NPC_Citizen>();
                                        if (OtherNPC)
                                        {
                                            if (OtherNPC.InfectionPointer != null)
                                            {
                                                if (!npc.Infected)
                                                {
                                                    float roll = Random.Range(0f, 10f);
                                                    if (roll < Contained_Actors[j].GetComponent<NPC_Citizen>().InfectionPointer.C_Data.Infectivity)
                                                    {
                                                        npc.Infect(Contained_Actors[j].GetComponent<NPC_Citizen>().InfectionPointer, false);
                                                        Stats.Infections_Hygiene++;
                                                    }
                                                }
                                            }   
                                        }
                                    }

                                    if (Building_Type != BuildingType.Cinema)
                                    {
                                        Eject_Actor(Contained_Actors[i]);
                                    }
                                    else
                                    {
                                        Force_Eject_All(false);
                                        Locked = true;
                                    }
                                    break;
                                }

                                #endregion
                                #region NPC is Calm, In a hospital, infected and aware of infection
                                else if (npc.panic_State == NPC_Panic_State.Calm && Building_Type == BuildingType.Clinic)
                                {
                                    //npc.Cure();
                                    Eject_Actor(Contained_Actors[i]);
                                    break;
                                }
                                #endregion
                                #region NPC wasn't chased inside by a zombie
                                else
                                {
                                    if (!npc.Chased_By_Zombie)
                                    {
                                        npc.panic_State = NPC_Panic_State.Calm;
                                        continue;
                                    }
                                }
                                #endregion
                            }
                            //else
                            //{
                            //    NPC_Police popo = Contained_Actors[i].GetComponent<NPC_Police>();
                            //    if (popo)
                            //    {

                            //    }
                            //}
                        }
                    }
                }
            }
            else
            {
                boot_Duration = 0f;
                boot_Timer = 0f;
            }
        }
        else
        {
            Force_Eject_All();
            Destroy_Self();
        }
	}

    void Destroy_Self()
    {
        if (!Destroyed)
        {
            //if (Building_Type == BuildingType.Bank || Random.Range(0, 50) == 0)
            //{
            //    Item_Catalogue.Instance.SpawnRandomItemOfTypeXOrY(DoorPoint.position, PickUp_Type.Equipment, PickUp_Type.PowerUp);
            //}
            Game_Manager.All_Buildings.Remove(this);
            Game_Manager.Buildings_By_Type[Building_Type].Remove(this);
            for (int i = 0; i < EnableWhenDestroyed.Length; i++)
            {
                EnableWhenDestroyed[i].SetActive(true);
            }

            //Placeholder Visuals
            if (destroyedTexture)
            {
                renderer.materials[MaterialIndex].SetTexture("_MainTex", destroyedTexture);
            }
            Renderer[] rends = GetComponentsInChildren<Renderer>();
            for (int i = 0; i < rends.Length; i++)
            {
                for (int j = 0; j < rends[i].materials.Length; j++)
                {
                    rends[i].materials[j].SetColor("_Color", rends[i].materials[j].color * 0.5f);
                }
            }
            Destroyed = true;
        }
    }

    public bool Take_In_Actor(Actor_Data actor)
    {
        if (Population < Capacity)
        {
            if (!Locked)
            {
                actor.Containing_Building = this;
                Contained_Actors.Add(actor);
                actor.transform.position = transform.position;
                actor.gameObject.SetActive(false);

                if (Building_Type == BuildingType.Clinic)
                {
                    if (actor.GetComponent<NPC_Citizen>().Infected && actor.GetComponent<NPC_Citizen>().Aware_Of_Infection)
                    {
                        actor.GetComponent<NPC_Citizen>().Cure();
                    }
                }

                return true;
            }
        }
        return false;
    }
    public void Eject_Actor(Actor_Data actor, bool was_Forced = false)
    {
        if (OptionalExitPoint)
        {
            actor.transform.position = OptionalExitPoint.position;
        }
        else
        {
            actor.transform.position = DoorPoint.position;
        }
        actor.CurrentAction = Actor_Action.Idle;
        actor.Containing_Building = null;
        NPC_Citizen npc = actor.GetComponent<NPC_Citizen>();
        if (npc)
        {
            npc.Clear_Building_Target();
            if (was_Forced)
            {
                npc.Roll_Panic(gameObject);
            }
        }

        actor.gameObject.SetActive(true);
        Contained_Actors.Remove(actor);
    }
    public void Force_Eject_All(bool Damaged = true)
    {
        while (Population > 0)
        {
            Eject_Actor(Contained_Actors[Contained_Actors.Count - 1], Damaged);
        }
    }
}

public class BuildingDescriptions
{
    public static string DescriptionForType(BuildingType Type)
    {
        switch (Type)
        {
            case BuildingType.Bank:
                return BankDesc;
            case BuildingType.Butcher:
                return ButcherDesc;
            case BuildingType.Cinema:
                return CinemaDesc;
            case BuildingType.Clinic:
                return HospitalDesc;
            case BuildingType.House:
                return HouseDesc;
            case BuildingType.PoliceStation:
                return PoliceStationDesc;
            case BuildingType.Powerplant:
                return PowerStationDesc;
            case BuildingType.Shop:
                return ShopDesc;
        }
        return "No Description Found";
    }
    public static string HouseDesc = "The house: A place for citizens to take brief refuge from the adversities of life, like weather, fatigue and zombies.";
    public static string HospitalDesc = "The Clinic: Infected citizens will go here to be cured... assuming they realize they are sick.";
    public static string PoliceStationDesc = "The Police Station: Spawns Police, and improves the co-ordination of police officers through the miracle of radio.";
    public static string ShopDesc = "The Shop: Citizens frequently visit these shops to swap paper for other, equally pointless things.";
    public static string BankDesc = "The Bank: Banks are, by necessity, secure, making them good for storing valuables such as jewelry, gold and, occasionally, loved ones.";
    public static string ButcherDesc = "The Butcher: The selection of masterfully eviscerated livestock herein is irresistable to zombies.";
    public static string CinemaDesc = "The Cinema: Citizens periodically travel here in groups, seeking escape from the dreariness of life.";

    //Not yet implemented (Some may never be)
    public static string RadioDesc = "The Radio Station: Broadcasts health warnings, increasing the chances of citizens realising they are infected.";
    public static string PowerStationDesc = "The Power Station: Electricity is the lifeforce of the modern world. Without it, all buildings slow down.";
    public static string WaterPumpDesc = "The Pumping Station: Destroying this building will infect the water supply, causing several random outbreaks.";
    public static string ReligiousSiteDec = "The Sacred Place: Some Citizens head here for spiritual guidance.";
    public static string MilitaryHQDesc = "The HQ: The base of operations for the outbreak containment unit. Destroy this, and kill its inhabitants.";
    public static string GunStoreDesc = "The Gun Store: Many peoples' first port of call in a zombie apocalypse, it provides Citizens with firearms.";
    
}