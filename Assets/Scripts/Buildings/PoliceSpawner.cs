﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Building_Behaviour))]
public class PoliceSpawner : MonoBehaviour 
{
    Building_Behaviour owner;
    public int PoliceTotal = 10;
    public int MaxActivePolice = 5;
    public float MaxSpawnInterval;
    float interval;
    public float timer = 0f;

	void Awake()
    {
        owner = GetComponent<Building_Behaviour>();
	}
	
	void Update()
    {
        if (owner.Health > 0)
        {
            if (Game_Manager.All_Police.Count < PoliceTotal)
            {
                NPC_Police p = Game_Manager.Spawn_Police_Officer(owner.DoorPoint.position);
                owner.Take_In_Actor(p.ActorControl);
            }
            if (PoliceTotal - owner.Population < MaxActivePolice)
            {
                timer += Time.deltaTime;
                if (timer >= MaxSpawnInterval)
                {
                    timer = 0f;
                    owner.Eject_Actor(owner.Contained_Actors[0]);
                }
            }
            else
            {
                timer = 0f;
            }
        }
	}
}