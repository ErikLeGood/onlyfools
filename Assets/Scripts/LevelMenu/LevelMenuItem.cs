﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public enum Difficulty
{
    Easy, Medium, Hard, Boss
}

public class LevelMenuItem : MonoBehaviour
{
    public int Index = -1;
    public string Name = "Town Sector";
    public string Description = "This section has stuff in it, and things.";
    public bool Completed = false;
    public bool Boss_Stage = false;
    public Difficulty difficulty = Difficulty.Easy;
    public int CitizenPopulation = 123;
    public GameObject ParticleSystem;
    public Light Spotlight;
    float Light_BaseIntensity = 1f;
    float Light_FadeSpeed = 5f;
    float Light_IntensityTarget = 1f;
    public string Scene_File_Name = "";

	void Start()
    {
        Light_BaseIntensity = Spotlight.intensity;
        ResetIntensity();
	}

    public void ResetIntensity()
    {
        Spotlight.intensity = Light_BaseIntensity;
        Light_IntensityTarget = Spotlight.intensity;
    }

    public void DefaultIntensityTarget()
    {
        Light_IntensityTarget = Light_BaseIntensity;
    }

    public void SetIntensityTarget(float Target)
    {
        Light_IntensityTarget = Target;
    }
	
	void FixedUpdate()
    {
        ParticleSystem.SetActive(Completed);
        if (Spotlight.intensity < Light_IntensityTarget)
        {
            Spotlight.intensity += Light_FadeSpeed * Time.deltaTime;
        }
        else if (Spotlight.intensity > Light_IntensityTarget)
        {
            Spotlight.intensity -= Light_FadeSpeed * Time.deltaTime;
        }
	}
}