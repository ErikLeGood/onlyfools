﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class LoadScreenManager : MonoBehaviour
{
    public List<string> Tips = new List<string>();
    public static string LEVEL_TARGET = "";
	void Start()
    {
        //Application.LoadLevelAsync(LEVEL_TARGET);
        SceneManager.LoadSceneAsync(LEVEL_TARGET);
	}
}
