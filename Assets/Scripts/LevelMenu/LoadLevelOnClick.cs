﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadLevelOnClick : MonoBehaviour 
{
    public string LevelName;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void LoadOnClick()
    {
        LoadScreenManager.LEVEL_TARGET = LevelName;
        SceneManager.LoadScene("LoadingScene", LoadSceneMode.Single);
    }
}