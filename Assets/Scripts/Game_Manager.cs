﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Enum = System.Enum;
using UnityEngine.UI;

[System.Serializable]
public enum GameState
{
    Playing, Paused, Creation, Setup, Menu, Failure, Success, Preview
}

public class Game_Manager : MonoBehaviour
{
    public bool Tutorial_Mode = false;
    public static SaveData SavedGame;
    public Text citizentext;
    public GameObject Pause_Menu;
    public static GameState State;

    public static List<Building_Behaviour> All_Buildings;
    public static Dictionary<BuildingType, List<Building_Behaviour>> Buildings_By_Type;
    public static Dictionary<string, List<Actor_Data>> Actors_By_Surname;
    public static List<Avatar_Movement_Control> Players = new List<Avatar_Movement_Control>();
    public static List<NPC_Citizen> All_Citizens = new List<NPC_Citizen>();
    public static List<NPC_Zombie> All_Zombies = new List<NPC_Zombie>();
    public static List<NPC_Citizen> Dead_Citizens = new List<NPC_Citizen>();
    public static List<NPC_Police> All_Police = new List<NPC_Police>();
    //public List<Info_Panel_Script> Selectors = new List<Info_Panel_Script>();

    public GameObject Zombie_Prefab;
    public GameObject Blood_Prefab;
    public GameObject Citizen_Prefab;
    public GameObject Police_Prefab;
    public GameObject Sample_Prefab;
    public static Game_Manager Instance;

    public static int Score = 0;
    public int Population = 50;
    public static int Citizens_Infected = 0;
    public static int Citizen_Ever_Infected = 0;
    public static int Citizens_Healthy = 0;
    public static int Zombies = 0;
    public static int Zombies_Ever = 0;

    #region Record Keeping Functions
    public static void Record_Infection()
    {
        Citizens_Healthy--;
        Citizens_Infected++;
        Citizen_Ever_Infected++;
    }
    public static void Record_Curing()
    {
        Citizens_Healthy++;
        Citizens_Infected--;
        Stats.Infections_Cured++;
    }
    public static void Record_Death(bool Healthy)
    {
        if (Healthy)
        {
            Citizens_Healthy--;
        }
        else
        {
            Citizens_Infected--;
        }
    }
    public static void Record_Zombie_Spawn()
    {
        Zombies++;
        Zombies_Ever++;
    }
    public static void Record_Zombie_Death()
    {
        Zombies--;
    }
    #endregion

    #region Building Retrieval
    public static Building_Behaviour GetRandomBuilding()
    {
        return All_Buildings[Random.Range(0, All_Buildings.Count)];
    }
    public static Building_Behaviour GetRandomBuildingOfType(BuildingType type)
    {
        if (Buildings_By_Type[type].Count > 0)
        {
            return Buildings_By_Type[type][Random.Range(0, Buildings_By_Type[type].Count)];
        }
        else
        {
            return null;
        }
    }
    public static Building_Behaviour GetRandomBuildingExceptOfTypes(params BuildingType[] TypeMasks)
    {
        List<int> indices = new List<int>();
        for (int i = 0; i < All_Buildings.Count; i++)
        {
            bool add = true;
            for (int j = 0; j < TypeMasks.Length; j++)
            {
                if (All_Buildings[i].Building_Type == TypeMasks[j])
                {
                    add = false;
                    break;
                }
            }
            if (add)
            {
                indices.Add(i);
            }
        }
        return All_Buildings[indices[Random.Range(0, indices.Count)]];
    }

    public static Building_Behaviour ClosestBuildingTo(Vector3 WorldPosition)
    {
        Building_Behaviour output = null;
        float dist = 10000000;
        for (int i = 0; i < All_Buildings.Count; i++)
        {
            if (All_Buildings[i].Population < All_Buildings[i].Capacity)
            {
                if (Vector3.Distance(WorldPosition, All_Buildings[i].DoorPoint.position) < dist)
                {
                    output = All_Buildings[i];
                    dist = Vector3.Distance(WorldPosition, All_Buildings[i].DoorPoint.position);
                }
            }
        }
        return output;
    }

    public static Building_Behaviour ClosestBuildingOfTypeTo(BuildingType type, Vector3 WorldPosition)
    {
        Building_Behaviour output = null;
        float dist = 10000000;
        for (int i = 0; i < Buildings_By_Type[type].Count; i++)
        {
            if (Buildings_By_Type[type][i].Population < Buildings_By_Type[type][i].Capacity)
            {
                if (Vector3.Distance(WorldPosition, Buildings_By_Type[type][i].DoorPoint.position) < dist)
                {
                    output = Buildings_By_Type[type][i];
                    dist = Vector3.Distance(WorldPosition, Buildings_By_Type[type][i].DoorPoint.position);
                }
            }
        }
        return output;
    }
    #endregion

    void Reassign_Citizen_Nav_Priorities()
    {
        for (int i = 0; i < All_Citizens.Count; i++)
        {
            int prio = Mathf.CeilToInt(i / 4f);
            if (prio >= 99)
            {
                prio = 98;
            }
            All_Citizens[i].GetComponent<UnityEngine.AI.NavMeshAgent>().avoidancePriority = prio;
        }
    }

    public Transform ZombieRoot;
    public Transform CitizenRoot;
    public Transform PoliceRoot;

    public GameObject Design_Stage_UI;
    public GameObject Playing_Stage_UI;
    public GameObject Restart_Stage_UI;
    public GameObject Preview_Stage_UI;

    #region Game Flow / UI Functions
    public static void Begin_Game(bool SetupStage)
    {
        Stats.RESET();
        if (Game_Manager.Instance.Tutorial_Mode)
        {
            TutorialAutoCam.Begin();
        }
        if (SetupStage)
        {
            State = GameState.Setup;
            Show_Active_HUD();
        }
        else
        {
            State = GameState.Playing;
            Show_Active_HUD();
        }
    }
    public static void PreviewLevel()
    {
        Show_Preview_HUD();
        State = GameState.Preview;
    }

    public static void Restart_Level()
    {
        //PLACEHOLDER
        //Application.LoadLevel(0);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public static void Register_Victory(Avatar_Movement_Control winner)
    {
        Main_Menu_Manager.RegisterWin(winner);
        winner.restartManager.Victory = true;
    }

    public static void Show_Preview_HUD()
    {
        Instance.Playing_Stage_UI.SetActive(false);
        Instance.Design_Stage_UI.SetActive(false);
        Instance.Restart_Stage_UI.SetActive(false);
        Instance.Preview_Stage_UI.SetActive(true);
    }

    public static void Show_Active_HUD()
    {
        Instance.Show_UI = true;
        Instance.Playing_Stage_UI.SetActive(true);
        Instance.Design_Stage_UI.SetActive(false);
        Instance.Restart_Stage_UI.SetActive(false);
        Instance.Preview_Stage_UI.SetActive(false);
    }
    public static void Show_Design_HUD()
    {
        Instance.Playing_Stage_UI.SetActive(false);
        Instance.Design_Stage_UI.SetActive(true);
        Instance.Restart_Stage_UI.SetActive(false);
        Instance.Preview_Stage_UI.SetActive(false);
    }
    public static void Show_Restart_UI()
    {
        Instance.Show_UI = false;
        Instance.Design_Stage_UI.SetActive(false);
        Instance.Preview_Stage_UI.SetActive(false);
        Instance.Restart_Stage_UI.SetActive(true);
    }
    #endregion

    void Awake()
    {
        Instance = this;
        All_Buildings = FindObjectsOfType<Building_Behaviour>().ToList();
        Buildings_By_Type = new Dictionary<BuildingType, List<Building_Behaviour>>();
        Actors_By_Surname = new Dictionary<string, List<Actor_Data>>();
        int tot = Enum.GetNames(typeof(BuildingType)).Length;
        for (int i = 0; i < tot; i++)
        {
            Buildings_By_Type.Add((BuildingType)i, new List<Building_Behaviour>());
        }
        for (int i = 0; i < All_Buildings.Count; i++)
        {
            Buildings_By_Type[All_Buildings[i].Building_Type].Add(All_Buildings[i]);
        }
        Players = FindObjectsOfType<Avatar_Movement_Control>().ToList();
        All_Zombies = FindObjectsOfType<NPC_Zombie>().ToList();
        All_Citizens = FindObjectsOfType<NPC_Citizen>().ToList();
        All_Police = FindObjectsOfType<NPC_Police>().ToList();
        for (int i = 0; i < All_Police.Count; i++)
        {
            NPC_Police.sawZombie_Event += All_Police[i].Got_Radio_Call;
        }

        if (Main_Menu_Manager.Levels_Static != null)
        {
            if (Main_Menu_Manager.SelectedLevel < Main_Menu_Manager.Levels_Static.Count && Main_Menu_Manager.SelectedLevel >= 0)
            {
                Population = Main_Menu_Manager.Levels_Static[Main_Menu_Manager.SelectedLevel].CitizenPopulation;
                Debug.Log(Population);
            }
        }

        SpawnUpToPopulation(Population);

        Citizens_Healthy = All_Citizens.Count;
        Citizens_Infected = 0;
        Citizen_Ever_Infected = 0;
        Zombies_Ever = 0;
        Zombies = 0;

        Application.targetFrameRate = 60;

        //Testing
        if (!Tutorial_Mode)
        {
            State = GameState.Preview;
        }
        else
        {
            Show_UI = false;
            Design_Stage_UI.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.F4))
        {

        }
    }

    void Start()
    {

    }

    public bool Show_UI = false;

    public static void PauseGame()
    {
        Instance.Pause();
    }
    public static void UnPauseGame()
    {
        Instance.UnPause();
    }
    public void Pause()
    {
        State = GameState.Paused;
        Time.timeScale = 0f;
        Show_UI = false;
        Pause_Menu.SetActive(true);
    }
    public void UnPause()
    {
        State = GameState.Playing;
        Time.timeScale = 1f;
        Show_UI = true;
        Pause_Menu.SetActive(false);
    }

    void Update()
    {
        citizentext.text = "Healthy: " + Citizens_Healthy.ToString() + " / Infected: " + Citizens_Infected.ToString() + " / Zombies: " + Zombies.ToString();

        if (Input.GetKeyDown(KeyCode.F1))
        {
            Show_UI = !Show_UI;
        }
        Playing_Stage_UI.SetActive(Show_UI && State == GameState.Playing);

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (State == GameState.Playing)
            {
                Pause();
            }
            else if (State == GameState.Paused)
            {
                UnPause();
            }
        }
        if (Input.GetKeyDown(KeyCode.F4))
        {
            GoHome();
        }
    }

    #region AI Cheats
    public void GoHome()
    {
        for (int i = 0; i < All_Citizens.Count; i++)
        {
            if (All_Citizens[i].ActorControl.Containing_Building != null)
            {
                All_Citizens[i].ActorControl.Containing_Building.Eject_Actor(All_Citizens[i].ActorControl);
            }
            All_Citizens[i].GoHome(true);
        }
    }
    #endregion

    #region Object Spawning
    public static void Spawn_Zombie(Vector3 position, NPC_Zombie progenitor, Actor_Data Victim = null)
    {
        Spawn_Zombie(position, progenitor.owner, progenitor.Generation + 1, Victim);
    }

    public static void Spawn_Zombie(Vector3 position, Avatar_Movement_Control progenitor, int GenerationOverride = 1, Actor_Data Victim = null)
    {
        GameObject newZombie = (GameObject)GameObject.Instantiate(Instance.Zombie_Prefab, position, Quaternion.identity);
        NPC_Zombie zScript = newZombie.GetComponent<NPC_Zombie>();
        zScript.owner = progenitor;
        if (Victim != null)
        {
            zScript.ActorControl.Forename = Victim.Forename;
            zScript.ActorControl.Surname = Victim.Surname;
            zScript.ActorControl.gender = Victim.gender;
            AddActorWithSurname(zScript.ActorControl);
        }
        
        zScript.Generation = GenerationOverride;
        newZombie.transform.parent = Instance.ZombieRoot;
        Record_Zombie_Spawn();
        All_Zombies.Add(zScript);
        NPC_Police.Fired_A_Shot_Event += zScript.Gunshot_Event;
        Avatar_Movement_Control.Taunt_Event += zScript.TauntedByPlayer;
    }

    public static void SpawnBlood(Vector3 pos)
    {
        GameObject blud = (GameObject)GameObject.Instantiate(Instance.Blood_Prefab, pos, Quaternion.identity);
        float size = Random.Range(0.5f, 1.8f);
        blud.transform.localScale = new Vector3(size, 1f, size);
        blud.transform.rotation = Quaternion.Euler(0f, Random.Range(0, 360f), 0f);
        Renderer[] rends = blud.GetComponentsInChildren<Renderer>();
        for (int i = 0; i < rends.Length; i++)
        {
            rends[i].material.mainTextureScale = new Vector2(Random.Range(0.01f, 0.1f), Random.Range(0.01f, 0.1f));
        }
    }
    public static void SpawnSample(Vector3 pos)
    {
        GameObject s = (GameObject)GameObject.Instantiate(Instance.Sample_Prefab, pos, Quaternion.identity);
    }

    public static void SpawnUpToPopulation(int population)
    {
        while (All_Citizens.Count < population)
        {
            Spawn_Randomised_Civilian(GetRandomBuildingOfType(BuildingType.House));
        }
    }

    public static void Spawn_Randomised_Civilian(Building_Behaviour building)
    {
        NPC_Citizen citizen = ((GameObject)GameObject.Instantiate(Instance.Citizen_Prefab, building.DoorPoint.position, Quaternion.identity)).GetComponent<NPC_Citizen>();
        citizen.Home = building;
        AddActorWithSurname(citizen.ActorControl);
        citizen.transform.parent = Instance.CitizenRoot;
        if (!All_Citizens.Contains(citizen))
        {
            All_Citizens.Add(citizen);
        }
        if (Random.Range(0,7) != 0)
            building.Take_In_Actor(citizen.ActorControl);
    }

    public static NPC_Police Spawn_Police_Officer(Vector3 position)
    {
        NPC_Police popo = ((GameObject)GameObject.Instantiate(Instance.Police_Prefab, position, Quaternion.identity)).GetComponent<NPC_Police>();
        All_Police.Add(popo);
        popo.transform.parent = Instance.PoliceRoot;
        return popo;
    }
    
    #endregion

    #region Surname Dictionary Functions
    public static void AddActorWithSurname(Actor_Data Actor)
    {
        if (!Actors_By_Surname.ContainsKey(Actor.Surname))
        {
            Actors_By_Surname.Add(Actor.Surname, new List<Actor_Data>());//Add Surname to Dictionary and create List
            Actors_By_Surname[Actor.Surname].Add(Actor);//Add Actor Instance to List.
        }
        else
        {
            Actors_By_Surname[Actor.Surname].Add(Actor);//Add Actor Instance to List.
        }
    }
    public static void RemoveActorWithSurname(Actor_Data Actor)
    {
        if (Actors_By_Surname.ContainsKey(Actor.Surname))
        {
            Actors_By_Surname[Actor.Surname].Add(Actor);//Add Actor Instance to List.
        }
    }
    #endregion

    #region Death Functions
    public static void Kill_Citizen(NPC_Citizen citizen, bool OverrideZombie = false)
    {
        //Dead_Citizens.Add(citizen);
        All_Citizens.Remove(citizen);
        RemoveActorWithSurname(citizen.ActorControl);
        NPC_Police.Fired_A_Shot_Event -= citizen.Heard_Gunshot_Event;
        SpawnBlood(citizen.transform.position);
        Capsule_Script.Remove_Subscriber(citizen);
        if (citizen.transform.childCount > 0)
        {
            for (int i = 0; i < citizen.transform.childCount; i++)
            {
                if (citizen.transform.GetChild(i).tag == "Marker")
                    citizen.transform.GetChild(i).parent = null;
            }
        }
        Record_Death(!citizen.Infected);
        if (citizen.Infected && !OverrideZombie)
        {
            Spawn_Zombie(citizen.transform.position, citizen.InfectionPointer, 1, citizen.ActorControl);
            SpawnSample(citizen.transform.position);
        }
        for (int i = 0; i < citizen.transform.childCount; i++)
        {
            if (citizen.transform.GetChild(i).gameObject.tag == "Marker")
            {
                citizen.transform.GetChild(i).parent = null;
                break;
            }
        }
        GameObject.Destroy(citizen.gameObject);
        //citizen.gameObject.SetActive(false);
    }

    public static void Kill_Zombie(NPC_Zombie zombie)
    {
        SpawnBlood(zombie.transform.position);
        Record_Zombie_Death();
        All_Zombies.Remove(zombie);
        RemoveActorWithSurname(zombie.ActorControl);
        Avatar_Movement_Control.Taunt_Event -= zombie.TauntedByPlayer;
        NoiseMaker_Script.Ranged_Taunt_Event -= zombie.TauntedBySound;
        for (int i = 0; i < zombie.transform.childCount; i++)
        {
            if (zombie.transform.GetChild(i).gameObject.tag == "Marker")
            {
                zombie.transform.GetChild(i).parent = null;
                break;
            }
        }
        GameObject.Destroy(zombie.gameObject);
    }

    public static void Kill_Police(NPC_Police police)
    {
        NPC_Police.sawZombie_Event -= police.Got_Radio_Call;

        if (police.Infected)
            Spawn_Zombie(police.transform.position, police.Infection_Pointer);

        Capsule_Script.Remove_Subscriber(police);
        SpawnBlood(police.transform.position);
        All_Police.Remove(police);
        for (int i = 0; i < police.transform.childCount; i++)
        {
            if (police.transform.GetChild(i).gameObject.tag == "Marker")
            {
                police.transform.GetChild(i).parent = null;
                break;
            }
        }
        GameObject.Destroy(police.gameObject);
    }
    #endregion
}