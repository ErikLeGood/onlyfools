﻿using UnityEngine;
using UnityStandardAssets.ImageEffects;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public enum Game_End_Reason : int
{
    Success = 0, Got_Cured = 1, Player_Died = 2, Zombies_Died = 3, All_Shots_Missed = 4, Mission_Abandoned = 5
}

public class Restart_Menu_Manager : MonoBehaviour
{
    List<string> Explanations = new List<string>();
    public Text Context_Title;
    public Text Explanation_Text;

    public GameObject RetryButton;
    public GameObject ExitButton;

    public bool Victory = false;

    public Text Infections_Capsule_Text;
    public Text Infections_Bite_Text;
    public Text Infections_Hygiene_Text;
    public Text Infections_Cured_Text;

    public Text Z_Deaths_By_Police_Text;
    public Text Z_Deaths_By_Civs_Text;
    public Text Z_Deaths_By_Other_Text;

    void Awake()
    {
        Victory = false;
        Explanations.Add("You successfully wiped out the town, becoming the only remaining human! Good for you, murderer.");
        Explanations.Add("You were defeated by medical science! Your contagion was spotted and cured before it had a chance to claim even one victim. Sad times... kinda.");
        Explanations.Add("You were killed. Whether by the gun or the zombie, sooner or later all mortals meet their end.");
        Explanations.Add("Your Zombies were destroyed! The heroic deeds of the brave have beaten back the shambling horde and reclaimed the town. Yay! ...Oh wait, that means you lose... \n...Awkward!");
        Explanations.Add("Your Contagion didn't infect a single person. Either you need a reminder of the controls, or you were overcome with mercy. Either way, you failed. Big time.");
        Explanations.Add("You chose to Abandon the Mission... \n \n...The mission was Abandoned.");
        gameObject.SetActive(false);
    }

	void Start()
    {
	
	}
	
	void Update()
    {
	
	}
    public void Show(Game_End_Reason reason)
    {
        switch ((int)reason)
        {
            case 0:
                Context_Title.text = "You Won!";
                break;
            case 1:
                Context_Title.text = "Contagion Eradicated.";
                break;
            case 2:
                Context_Title.text = "Player dead.";
                break;
            case 3:
                Context_Title.text = "Zombies Eradicated.";
                break;
            case 4:
                Context_Title.text = "Failed to Infect. (Missed)";
                break;
            case 5:
                Context_Title.text = "Abandoned.";
                break;
        }
        Explanation_Text.text = Explanations[(int)reason];
        RetryButton.SetActive(reason != Game_End_Reason.Success);
        Game_Manager.Show_Restart_UI();
        Camera.main.GetComponent<Grayscale>().enabled = true;
        Update_Stats();
    }

    void Update_Stats()
    {
        Infections_Capsule_Text.text = "Capsule: " + Stats.Infections_Capsule.ToString();
        Infections_Bite_Text.text = "Zombie: " + Stats.Infections_Biting.ToString();
        Infections_Hygiene_Text.text = "Inter-Personal: " + Stats.Infections_Hygiene.ToString();
        Infections_Cured_Text.text = "Cures Applied: " + Stats.Infections_Cured.ToString();

        Z_Deaths_By_Police_Text.text = "By Police: " + Stats.Zombies_Killed_By_Cops.ToString();
        Z_Deaths_By_Civs_Text.text = "By Citizens: " + Stats.Zombies_Killed_By_Civs.ToString();
        Z_Deaths_By_Other_Text.text = "By Other: " + Stats.Zombies_Killed_By_Other.ToString();
    }
}