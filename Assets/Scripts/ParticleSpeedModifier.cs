﻿using UnityEngine;
using System.Collections;

public class ParticleSpeedModifier : MonoBehaviour
{
    public float Speed = 1f;

	void Start ()
    {
        gameObject.GetComponent<ParticleSystem>().playbackSpeed = Speed;
	}
	
	void Update ()
    {
	
	}
}