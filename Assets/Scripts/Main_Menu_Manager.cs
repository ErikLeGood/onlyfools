﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.IO;
using System.Xml.Serialization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

[System.Serializable]
public enum Menu_State
{
    Main, Level_Select
}

[System.Serializable]
public class CamPositions
{
    public Transform MainPosition;
    public Transform LevelSelectPosition;
    public Transform[] Level_Positions;
}

[System.Serializable]
public class ToolTipTextHelper
{
    public Text Title;
    public Text Complete;
    public Text Population;
    public Text Difficulty;
    public Text Description;
    public Button PlayLevelButton;

    public void Update(LevelMenuItem level)
    {
        Title.text = level.Name;
        Complete.text = level.Completed ? "Completed" : "Not Completed";
        Population.text = "Population: " + level.CitizenPopulation.ToString();
        Difficulty.text = "Difficulty: " + level.difficulty.ToString();
        Description.text = level.Description;
        PlayLevelButton.gameObject.SetActive(!level.Completed);
    }
}

public class Main_Menu_Manager : MonoBehaviour
{
    public Menu_State MenuState = Menu_State.Main;
    public List<LevelMenuItem> Levels = new List<LevelMenuItem>();
    public static int SelectedLevel = -1;
    public static List<LevelMenuItem> Levels_Static = new List<LevelMenuItem>();
    public CameraTruck camControl;
    Camera cam;
    public CamPositions camPositions = new CamPositions();
    public GameObject MainMenuItems;
    public GameObject LevelSelectItems;
    public GameObject LevelSelectToolTip;
    public Text PopupTitleText;
    public Text PopupCompleteText;
    public Text PopupDescText;

    public GameObject ZoomedSelectionToolTip;
    public ToolTipTextHelper ZoomedToopTipHelper;

    public GameObject ConfirmNewGameWindow;
    public GameObject ContinueButton;

    bool zoomed = false;

    public static SaveData Saved_Game;
    public static string Path = "";
    public static string PathToSave;

    public Light SceneMainLight;
    float Light_BaseIntensity = 1f;
    float Light_FadeSpeed = 1.5f;
    float Light_IntensityTarget = 1f;

    void Configure_From_Save()
    {
        for (int i = 0; i < Levels.Count; i++)
        {
            if (Saved_Game.Complete_Level_Indices.Contains(i))
            {
                Levels[i].Completed = true;
            }
        }
    }

    void Awake()
    {
        PathToSave = Application.dataPath + "/Saves" + "/Current.Xml";
        Path = Application.dataPath + "/Saves";
        if (!Directory.Exists(Path))
            Directory.CreateDirectory(Path);

        Path += "/Current.Xml";
        if (File.Exists(Path))
        {
            Saved_Game = Load(Path);
            ContinueButton.SetActive(true);
            Configure_From_Save();
        }
        else
        {
            ContinueButton.SetActive(false);
        }
        Levels_Static = Levels;
    }

    public SaveData Load(string path)
    {
        SaveData output;
        XmlSerializer xs = new XmlSerializer(typeof(SaveData));
        FileStream file = File.OpenRead(path);
        output = (SaveData)xs.Deserialize(file);
        file.Close();
        return output;
    }
    public static bool Save(SaveData save)
    {
        XmlSerializer xs = new XmlSerializer(typeof(SaveData));
        try
        {
            FileStream file = File.Create(PathToSave);
            xs.Serialize(file, save);
            file.Close();
            return true;
        }
        catch
        {

        }
        return false;
    }
    public static void DeleteSaveFile()
    {
        if (File.Exists(PathToSave))
            File.Delete(PathToSave);
    }

    public void LoadLevel()
    {
        LoadScreenManager.LEVEL_TARGET = Levels[SelectedLevel].Scene_File_Name;
        //Application.LoadLevel("LoadingScene");
        SceneManager.LoadScene("LoadingScene");
    }

    public static void RegisterWin(Avatar_Movement_Control winner)
    {
        if (Saved_Game == null)
            Saved_Game = new SaveData();

        //Saved_Game.Equipped_Items.Clear();
        //Saved_Game.Equipped_Items.AddRange(winner.inventory_Pointer.Equipped_Items);
        if (!Saved_Game.Complete_Level_Indices.Contains(SelectedLevel))
            Saved_Game.Complete_Level_Indices.Add(SelectedLevel);
        Saved_Game.Stats = winner.C_Data;
        Saved_Game.Zombies_Spawned += Game_Manager.All_Zombies.Count;

        Save(Saved_Game);
    }

    public void Quit_Button()
    {
        Application.Quit();
    }

	void Start()
    {
        cam = camControl.GetComponent<Camera>();
        Light_BaseIntensity = SceneMainLight.intensity;
        ResetIntensity();
	}

    public void PlayButtonClicked(bool NewGame)
    {
        if (Saved_Game == null || !NewGame)
        {
            MenuState = Menu_State.Level_Select;
            camControl.SetTarget(camPositions.LevelSelectPosition);
            MainMenuItems.SetActive(false);
            LevelSelectItems.SetActive(true);
        }
        else
        {
            ConfirmNewGameWindow.SetActive(true);
        }
    }
    public void ConfirmNewGame_YES()
    {
        DeleteSaveFile();
        for (int i = 0; i < Levels.Count; i++)
            Levels[i].Completed = false;

        Saved_Game = new SaveData();
        MenuState = Menu_State.Level_Select;
        camControl.SetTarget(camPositions.LevelSelectPosition);
        MainMenuItems.SetActive(false);
        LevelSelectItems.SetActive(true);
        ConfirmNewGameWindow.SetActive(false);
    }
    public void ConfirmNewGame_NO()
    {
        ConfirmNewGameWindow.SetActive(false);
    }

    public void BackButtonClicked()
    {
        if (SelectedLevel == -1)
        {
            camControl.SetTarget(camPositions.MainPosition);
            MainMenuItems.SetActive(true);
            LevelSelectItems.SetActive(false);
            MenuState = Menu_State.Main;
        }
        else
        {
            camControl.SetTarget(camPositions.LevelSelectPosition);
            SelectedLevel = -1;
        }
        zoomed = false;
    }

    void UpdatePopUp(LevelMenuItem lvl)
    {
        PopupTitleText.text = lvl.Name;
        PopupCompleteText.text = lvl.Completed ? "Completed" : "Not Completed";
        PopupDescText.text = lvl.Description;
    }

	void Update()
    {
	    switch (MenuState)
        {
            case Menu_State.Main:
                break;
            case Menu_State.Level_Select:
                RaycastHit rayHit;
                LevelSelectToolTip.SetActive(false);
                #region Unzoomed
                if (!zoomed)
                {
                    DefaultIntensityTarget();
                    for (int i = 0; i < Levels.Count; i++)
                    {
                        Levels[i].DefaultIntensityTarget();
                    }
                    ZoomedSelectionToolTip.SetActive(false);
                    if (EventSystem.current.currentSelectedGameObject == null)
                    {
                        if (Physics.Raycast(cam.ScreenPointToRay(Input.mousePosition), out rayHit, 1 << 9))
                        {
                            LevelMenuItem lvl = rayHit.collider.GetComponent<LevelMenuItem>();
                            if (lvl)
                            {
                                LevelSelectToolTip.transform.position = Input.mousePosition;
                                LevelSelectToolTip.SetActive(true);
                                UpdatePopUp(lvl);
                                if (Input.GetMouseButtonDown(0))
                                {
                                    //Debug.Log(rayHit.collider.gameObject);
                                    camControl.SetTarget(camPositions.Level_Positions[lvl.Index]);
                                    SelectedLevel = lvl.Index;
                                    zoomed = true;
                                }
                            }
                        }
                    }
                }
                #endregion
                #region Zoomed
                else
                {
                    if (!ZoomedSelectionToolTip.activeInHierarchy)
                        ZoomedSelectionToolTip.SetActive(true);

                    Light_IntensityTarget = 0.1f;
                    for (int i = 0; i < Levels.Count; i++)
                    {
                        if (i != SelectedLevel)
                            Levels[i].SetIntensityTarget(0.1f);
                        else
                            Levels[i].SetIntensityTarget(8f);
                    }
                    ZoomedSelectionToolTip.transform.position = cam.WorldToScreenPoint(Levels[SelectedLevel].transform.position);
                    ZoomedToopTipHelper.Update(Levels[SelectedLevel]);
                }
                #endregion
                break;
        }
	}

    public void ResetIntensity()
    {
        SceneMainLight.intensity = Light_BaseIntensity;
        Light_IntensityTarget = SceneMainLight.intensity;
    }
    public void SetIntensityTarget(float Target)
    {
        Light_IntensityTarget = Target;
    }
    public void DefaultIntensityTarget()
    {
        Light_IntensityTarget = Light_BaseIntensity;
    }
    void FixedUpdate()
    {
        if (SceneMainLight.intensity < Light_IntensityTarget)
        {
            SceneMainLight.intensity += Light_FadeSpeed * Time.deltaTime;
        }
        else if (SceneMainLight.intensity > Light_IntensityTarget)
        {
            SceneMainLight.intensity -= Light_FadeSpeed * Time.deltaTime;
        }
    }
}