﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Setup_Menu_Manager : MonoBehaviour
{
    public BuildingPopupManager SelectionManager;
    public Avatar_Movement_Control playerPointer;
    int remoteCapsules = 0;
    public Button Begin_Game_Button;
    public Button Add_Building_Button;
    public Button Remove_Building_Button;
    public GameObject RemoteMarker_Prefab;
    //Camera cam;
    //LayerMask mask;

	void Start()
    {
        remoteCapsules = playerPointer.C_Data.Remote_Capsule_Ammo_Initial;
        //cam = playerPointer.GetComponent<Camera>();
        //mask = (1 << 10 | 1 << 1);
	}
	
	void Update()
    {
        #region UI Update
        if (remoteCapsules > 0)
        {
            Begin_Game_Button.gameObject.SetActive(false);
        }
        else
        {
            Begin_Game_Button.gameObject.SetActive(true);
        }

        if (SelectionManager.Selection != null)
        {
            if (!SelectionManager.Selection.Capsule_Placed)
            {
                Add_Building_Button.gameObject.SetActive(true);
                Remove_Building_Button.gameObject.SetActive(false);
            }
            else
            {
                Add_Building_Button.gameObject.SetActive(false);
                Remove_Building_Button.gameObject.SetActive(true);
            }
        }
        else
        {
            Add_Building_Button.gameObject.SetActive(false);
            Remove_Building_Button.gameObject.SetActive(false);
        }
        #endregion
    }

    public void AddRemoteCapButton()
    {
        if (!SelectionManager.Selection.Capsule_Placed)
            SelectionManager.Selection.Assign_Capsule(playerPointer);
    }
    public void RemoveRemoteCapButton()
    {

    }

    public void Begin_Button()
    {
        Game_Manager.Begin_Game(false);
    }
}