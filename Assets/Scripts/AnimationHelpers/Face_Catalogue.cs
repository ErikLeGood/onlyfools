﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Face_Catalogue : MonoBehaviour
{
    public List<Face> Male_Faces = new List<Face>();
    public List<Face> Female_Faces = new List<Face>();
    public List<Face> Zombie_Faces = new List<Face>();
    public static Face_Catalogue instance;

    public List<Texture> Male_Outfits = new List<Texture>();
    public List<Texture> Female_Outfits = new List<Texture>();
    public List<Texture> Zombie_Outfits = new List<Texture>();

    public static Texture GetRandomCitizenOutfit(Gender gender)
    {
        Texture output = new Texture();
        switch (gender)
        {
            case Gender.Female:
                output = instance.Female_Outfits[Random.Range(0, instance.Female_Outfits.Count)];
                break;
            case Gender.Male:
                output = instance.Male_Outfits[Random.Range(0, instance.Male_Outfits.Count)];
                break;
        }
        return output;
    }
    public static Texture GetRandomZombieOutfit()
    {
        return instance.Zombie_Outfits[Random.Range(0, instance.Zombie_Outfits.Count)];
    }

    public static Face Get_Random_Face(Gender gender)
    {
        switch (gender)
        {
            case Gender.Female:
                return instance.Female_Faces[Random.Range(0, instance.Female_Faces.Count)];
            case Gender.Male:
                return instance.Male_Faces[Random.Range(0, instance.Male_Faces.Count)];
        }
        return null;
    }
    public static Face Get_Random_Zombie_Face()
    {
        return instance.Zombie_Faces[Random.Range(0, instance.Zombie_Faces.Count)];
    }

    void Awake()
    {
        instance = this;
    }

	void Start()
    {
	
	}
	
	void Update()
    {
	
	}
}