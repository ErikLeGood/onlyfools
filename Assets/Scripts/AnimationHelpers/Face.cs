﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Face
{
    public Gender Gender;
    public Texture Calm;
    public Texture Panicked;

}