﻿using UnityEngine;
using System.Collections;

public class Particle_Auto_Destroy : MonoBehaviour
{
    ParticleSystem system;
    bool PlayingLastFrame = false;

	void Start ()
    {
        system = GetComponent<ParticleSystem>();
	}
	
	void Update ()
    {
        if (!system.isPlaying && PlayingLastFrame)
        {
            GameObject.Destroy(gameObject, 1.5f);
        }

        PlayingLastFrame = system.isPlaying;
	}
}