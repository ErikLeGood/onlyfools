﻿using UnityEngine;
using System.Collections;

public class Fence_Script : MonoBehaviour 
{
    public UnityEngine.AI.NavMeshObstacle Obstacle;
    public Rigidbody rigidbod;
    public BoxCollider triggerzone;

	void Start()
    {
        if (!rigidbod)
            rigidbod = GetComponent<Rigidbody>();
        if (!Obstacle)
            Obstacle = GetComponent<UnityEngine.AI.NavMeshObstacle>();
        if (!triggerzone)
            triggerzone = GetComponent<BoxCollider>();
	}

    public bool Fire = false;
    private bool fired = false;

    Vector3 force_Direction = Vector3.zero;
	
	void Update()
    {
	    if (Fire && !fired)
        {
            fired = true;
            rigidbod.constraints = RigidbodyConstraints.None;
            Obstacle.carving = false;
            Obstacle.enabled = false;
            if (force_Direction == Vector3.zero)
                force_Direction = Vector3.right;

            rigidbod.AddForce(force_Direction, ForceMode.Impulse);
        }
        if (rigidbod)
        {
            if (rigidbod.IsSleeping())
            {
                rigidbod.gameObject.GetComponent<Collider>().enabled = false;
                GameObject.Destroy(rigidbod);
            }
        }
	}

    void OnTriggerEnter(Collider c)
    {
        if (!fired)
        {
            if (c.GetComponent<Actor_Data>())
            {
                Actor_Data aData = c.GetComponent<Actor_Data>();
                if (aData.ActorType == Actor_Type.Zombie || aData.Running)
                {
                    Fire = true;
                    force_Direction = (transform.position - c.transform.position).normalized;
                    Vector3 dest = aData.navAgent.destination;
                    //aData.StartMoving(dest, true);
                }
            }
        }
    }
}