﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public delegate void EventHandler_WeatherChanged(WeatherType newWeather);

[System.Serializable]
public enum WeatherType : int
{
    Fog = 0,
    Rain = 1, 
    Storm = 2,
    Clear = 3,
    Snow = 4,
    Blizzard = 5
}
public class Weather : MonoBehaviour
{
    public static WeatherType CurrentWeather = WeatherType.Clear;

    public static event EventHandler_WeatherChanged WeatherChangeEvent;

    public bool WeatherEnabled = true;

    public Light MainLight;
    public float ClearLightLevel = 0.4f;
    public float DimLightLevel = 0.1f;
    float LightLevelTarget;

    public float FogOffLevel = 0f;
    public float FogOnLevel = 1f;
    float FogIntensityTarget = 0f;

    public float WeatherChangeMaxInterval = 120f;
    public float WeatherChangeMinInterval = 30f;
    public float WeatherChangeInterval = 30f;
    float timer = 0f;

    public DigitalRuby.RainMaker.RainScript Rain;
    public float RainIntensityTarget = 0f;

    public Lightning lightning;

    public void NewRandomWeatherInterval()
    {
        WeatherChangeInterval = Random.Range(WeatherChangeMinInterval, WeatherChangeMaxInterval);
    }
    public void NewRandomWeather()
    {
        int d = Random.Range(0, 4);
        if ((WeatherType)d != CurrentWeather)
        {
            WeatherChangeEvent.Invoke((WeatherType)d);
        }
        CurrentWeather = (WeatherType)d;
        Debug.Log(CurrentWeather.ToString());

        InitializeWeather();
    }
    public void NewRandomWeatherNotFog()
    {
        int d = Random.Range(1, 4);
        if ((WeatherType)d != CurrentWeather)
        {
            WeatherChangeEvent.Invoke((WeatherType)d);
        }
        CurrentWeather = (WeatherType)d;
        Debug.Log(CurrentWeather.ToString());
        InitializeWeather();
    }
    void InitializeWeather()
    {
        if (CurrentWeather == WeatherType.Rain)
        {
            RainIntensityTarget = 0.5f;
            LightLevelTarget = DimLightLevel;
            lightning.gameObject.SetActive(false);
            FogIntensityTarget = 500f;
            Rain.EnableWind = true;
        }
        else if (CurrentWeather == WeatherType.Storm)
        {
            RainIntensityTarget = 1f;
            LightLevelTarget = DimLightLevel;
            lightning.gameObject.SetActive(true);
            FogIntensityTarget = 500f;
            Rain.EnableWind = true;
        }
        else if (CurrentWeather == WeatherType.Clear)
        {
            RainIntensityTarget = 0f;
            LightLevelTarget = ClearLightLevel;
            lightning.gameObject.SetActive(false);
            FogIntensityTarget = 500f;
            Rain.EnableWind = true;
        }
        else if (CurrentWeather == WeatherType.Fog)
        {
            lightning.gameObject.SetActive(false);
            RainIntensityTarget = 0f;
            LightLevelTarget = ClearLightLevel;
            FogIntensityTarget = 50;
            Rain.EnableWind = false;
        }
    }

	void Start()
    {
        FogIntensityTarget = 500f;
        NewRandomWeatherNotFog();
        NewRandomWeatherInterval();
	}
	
	void Update()
    {
        if (WeatherEnabled)
        {
            timer += Time.deltaTime;
            if (timer >= WeatherChangeInterval || Input.GetKeyDown(KeyCode.F3))
            {
                NewRandomWeatherInterval();
                NewRandomWeather();
                timer = 0f;
            }
        }
        else
        {
            CurrentWeather = WeatherType.Clear;
            RainIntensityTarget = 0f;
            LightLevelTarget = ClearLightLevel;
            lightning.gameObject.SetActive(false);
            FogIntensityTarget = 1000f;
            Rain.EnableWind = true;
        }

        #region Fog Updating
        if (CurrentWeather == WeatherType.Fog)
        {
            float fogTarg = Vector3.Distance(CamController2.instance.cam.transform.position, CamController2.instance.CamOrbitOrigin.position) * 2;
            if (fogTarg > 200f)
                fogTarg = 200f;
            RenderSettings.fogEndDistance = Mathf.Lerp(RenderSettings.fogEndDistance, fogTarg, 2f * Time.deltaTime);
        }
        else if (CurrentWeather == WeatherType.Rain)
        {
            if (RenderSettings.fogEndDistance <= 450f)
            {
                RenderSettings.fogEndDistance += 25f * Time.deltaTime;
            }
            if (RenderSettings.fogEndDistance >= 450f)
            {
                RenderSettings.fogEndDistance -= 25f * Time.deltaTime;
            }
        }
        else if (CurrentWeather == WeatherType.Storm)
        {
            if (RenderSettings.fogEndDistance <= 300f)
            {
                RenderSettings.fogEndDistance += 25f * Time.deltaTime;
            }
            else if (RenderSettings.fogEndDistance >= 300f)
            {
                RenderSettings.fogEndDistance -= 25f * Time.deltaTime;
            }
        }
        else if (CurrentWeather == WeatherType.Clear)
        {
            if (RenderSettings.fogEndDistance <= 1500f)
            {
                RenderSettings.fogEndDistance += 25f * Time.deltaTime;
            }
            //else
            //{
            //    RenderSettings.fog = false;
            //}
        }
        #endregion
        
        if (Rain.RainIntensity != RainIntensityTarget)
        {
            Rain.RainIntensity = Mathf.Lerp(Rain.RainIntensity, RainIntensityTarget, 0.75f * Time.deltaTime);
        }
        if (MainLight.intensity != LightLevelTarget)
        {
            MainLight.intensity = Mathf.Lerp(MainLight.intensity, LightLevelTarget, 0.75f * Time.deltaTime);
        }
	}
}