﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LightningPalette
{
    public List<AudioClip> SFX = new List<AudioClip>();
    public List<Texture> Textures = new List<Texture>();
}

public class Lightning : MonoBehaviour
{
    public AudioSource audiosource;

    public Light light;

    public Renderer Renderer;

    float timer = 0f;
    float flashTimer = 0f;
    public float MaxFlashInterval = 0.5f;
    float flashInterval = 0.1f;
    public int MaxFlashes = 5;
    int flashes = 0;
    public float MaxCooldownInterval = 25f;
    float CooldownInterval = 15f;

    public bool Flashing = false;
    bool sound = false;

    public LightningPalette palette;

	void Start()
    {
		
	}
	
	void Update()
    {
		if (Flashing)
        {
            if (!sound)
            {
                sound = true;
                if (palette.SFX.Count > 0)
                    audiosource.PlayOneShot(palette.SFX[Random.Range(0, palette.SFX.Count)]);
            }
            flashTimer += Time.deltaTime;
            if (flashTimer >= flashInterval)
            {
                flashTimer = 0f;
                flashInterval = Random.Range(0f, MaxFlashInterval);
                light.gameObject.SetActive(!light.gameObject.activeInHierarchy);
                if (light.gameObject.activeInHierarchy == false)
                    flashes++;

                if (flashes >= MaxFlashes)
                {
                    Flashing = false;
                    CooldownInterval = Random.Range(1f, MaxCooldownInterval);
                }
            }
        }
        else
        {
            timer += Time.deltaTime;
            if (timer >= CooldownInterval)
            {
                sound = false;
                flashTimer = 0f;
                flashes = 0;
                Flashing = true;
                timer = 0f;
            }
        }
	}
}
