﻿using UnityEngine;
using System.Collections;

public class Stats
{
    public static int Infections_Capsule = 0;
    public static int Infections_Biting = 0;
    public static int Infections_Hygiene = 0;
    public static int Infections_Cured = 0;

    public static int Zombies_Killed_By_Civs = 0;
    public static int Zombies_Killed_By_Cops = 0;
    public static int Zombies_Killed_By_Other = 0;

    public static void RESET()
    {
        Infections_Biting = 0;
        Infections_Capsule = 0;
        Infections_Hygiene = 0;
        Infections_Cured = 0;
        Zombies_Killed_By_Civs = 0;
        Zombies_Killed_By_Cops = 0;
        Zombies_Killed_By_Other = 0;
    }
}